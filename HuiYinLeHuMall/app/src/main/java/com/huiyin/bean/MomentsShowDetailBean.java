package com.huiyin.bean;

import android.content.Context;

import com.google.gson.Gson;
import com.huiyin.utils.LogUtil;
import com.huiyin.utils.StringUtils;

public class MomentsShowDetailBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8161563623621076536L;

	private Map map;

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public class Map {
		/** 收藏数 **/
		private int collectCount;
		/** 点赞数 **/
		private int likeCount;
		/** 是否已经点赞 0 未点赞 1已点赞 **/
		private int isLike;
		/** 是否已经收藏 0 未收藏 1已收藏 **/
		private int isCollect;
		/** 是否已经打赏 0 未打赏 1已打赏 **/
		private int isReward;
		/** 分享地址 **/
		private String shareUrl;
		/** 打赏的红包总金额 **/
		private String bonus;
		/** 秀好物商品信息 **/
		private GoodsMap goodsMap;
		/** 详情基本信息 **/
		private showMap showMap;

		public int getCollectCount() {
			return collectCount;
		}

		public void setCollectCount(int collectCount) {
			this.collectCount = collectCount;
		}

		public int getLikeCount() {
			return likeCount;
		}

		public void setLikeCount(int likeCount) {
			this.likeCount = likeCount;
		}

		public int getIsLike() {
			return isLike;
		}

		public void setIsLike(int isLike) {
			this.isLike = isLike;
		}

		public int getIsCollect() {
			return isCollect;
		}

		public void setIsCollect(int isCollect) {
			this.isCollect = isCollect;
		}

		public int getIsReward() {
			return isReward;
		}

		public void setIsReward(int isReward) {
			this.isReward = isReward;
		}

		public GoodsMap getGoodsMap() {
			return goodsMap;
		}

		public void setGoodsMap(GoodsMap goodsMap) {
			this.goodsMap = goodsMap;
		}

		public showMap getShowMap() {
			return showMap;
		}

		public void setShowMap(showMap showMap) {
			this.showMap = showMap;
		}

		public String getBonus() {
			return bonus;
		}

		public void setBonus(String bonus) {
			this.bonus = bonus;
		}

		public String getShareUrl() {
			return shareUrl;
		}

		public void setShareUrl(String shareUrl) {
			this.shareUrl = shareUrl;
		}

	}

	/**
	 * 推荐商品Map
	 * 
	 * @author zhyao
	 * 
	 */
	public class GoodsMap {
		private String PRICE;

		private String ID;

		private String GOODS_NAME;

		private String IMG;

		public String getPRICE() {
			return PRICE;
		}

		public void setPRICE(String pRICE) {
			PRICE = pRICE;
		}

		public String getID() {
			return ID;
		}

		public void setID(String iD) {
			ID = iD;
		}

		public String getGOODS_NAME() {
			return GOODS_NAME;
		}

		public void setGOODS_NAME(String gOODS_NAME) {
			GOODS_NAME = gOODS_NAME;
		}

		public String getIMG() {
			return IMG;
		}

		public void setIMG(String iMG) {
			IMG = iMG;
		}

	}

	/**
	 * 详情Map
	 * 
	 * @author zhyao
	 * 
	 */
	public class showMap {
		private String SHOW_FILE;
		private String VIDEO_IMG;
		private String SHOW_GOODS_IDS;
		private String FACE_IMAGE_PATH;
		private String USER_ID;
		private String CREATE_TIME;
		private String SHOW_IMG;
		private String CIRCLE_IMG;
		private String PHONE;
		private String SPOTLIGHT_CIRCLE_ID;
		private String CIRCLE_NAME;
		private String CONTENT;
		private String USER_NAME;
		private String ID;
		private int TYPE;
		private String TITLE;

		public String getSHOW_FILE() {
			return SHOW_FILE;
		}

		public void setSHOW_FILE(String sHOW_FILE) {
			SHOW_FILE = sHOW_FILE;
		}

		public String getVIDEO_IMG() {
			return VIDEO_IMG;
		}

		public void setVIDEO_IMG(String vIDEO_IMG) {
			VIDEO_IMG = vIDEO_IMG;
		}

		public String getSHOW_GOODS_IDS() {
			return SHOW_GOODS_IDS;
		}

		public void setSHOW_GOODS_IDS(String sHOW_GOODS_IDS) {
			SHOW_GOODS_IDS = sHOW_GOODS_IDS;
		}

		public String getFACE_IMAGE_PATH() {
			return FACE_IMAGE_PATH;
		}

		public void setFACE_IMAGE_PATH(String fACE_IMAGE_PATH) {
			FACE_IMAGE_PATH = fACE_IMAGE_PATH;
		}

		public String getUSER_ID() {
			return USER_ID;
		}

		public void setUSER_ID(String uSER_ID) {
			USER_ID = uSER_ID;
		}

		public String getCREATE_TIME() {
			return StringUtils.friendly_time(CREATE_TIME);
		}

		public void setCREATE_TIME(String cREATE_TIME) {
			CREATE_TIME = cREATE_TIME;
		}

		public String getSHOW_IMG() {
			return SHOW_IMG;
		}

		public void setSHOW_IMG(String sHOW_IMG) {
			SHOW_IMG = sHOW_IMG;
		}

		public String getCIRCLE_IMG() {
			return CIRCLE_IMG;
		}

		public void setCIRCLE_IMG(String cIRCLE_IMG) {
			CIRCLE_IMG = cIRCLE_IMG;
		}

		public String getPHONE() {
			return PHONE;
		}

		public void setPHONE(String pHONE) {
			PHONE = pHONE;
		}

		public String getSPOTLIGHT_CIRCLE_ID() {
			return SPOTLIGHT_CIRCLE_ID;
		}

		public void setSPOTLIGHT_CIRCLE_ID(String sPOTLIGHT_CIRCLE_ID) {
			SPOTLIGHT_CIRCLE_ID = sPOTLIGHT_CIRCLE_ID;
		}

		public String getCIRCLE_NAME() {
			return CIRCLE_NAME;
		}

		public void setCIRCLE_NAME(String cIRCLE_NAME) {
			CIRCLE_NAME = cIRCLE_NAME;
		}

		public String getCONTENT() {
			return CONTENT;
		}

		public void setCONTENT(String cONTENT) {
			CONTENT = cONTENT;
		}

		public String getUSER_NAME() {
			return USER_NAME;
		}

		public void setUSER_NAME(String uSER_NAME) {
			USER_NAME = uSER_NAME;
		}

		public String getID() {
			return ID;
		}

		public void setID(String iD) {
			ID = iD;
		}

		public int getTYPE() {
			return TYPE;
		}

		public void setTYPE(int tYPE) {
			TYPE = tYPE;
		}

		public String getTITLE() {
			return TITLE;
		}

		public void setTITLE(String tITLE) {
			TITLE = tITLE;
		}

	}

	/**
	 * json解析
	 * 
	 * @param json
	 * @param context
	 * @return
	 */
	public static MomentsShowDetailBean explainJson(String json, Context context) {
		Gson gson = new Gson();
		try {
			MomentsShowDetailBean bean = gson.fromJson(json,
					MomentsShowDetailBean.class);
			return bean;
		} catch (Exception e) {
			LogUtil.d("dataExplainJson", e.toString());
			return null;
		}
	}
}
