package com.huiyin.bean;

import com.huiyin.utils.StringUtils;

public class MomentsShowItem {


	/** 秀歌,秀视频等文件路径 **/
	private String SHOW_FILE;

	/**  秀好物的商品id **/
	private String SHOW_GOODS_IDS;

	/**  发布秀场的用户头像 **/
	private String FACE_IMAGE_PATH;

	/**  发布秀场的用户ID **/
	private String USER_ID;

	/**  发布秀场的时间 **/
	private String CREATE_TIME;

	/** 秀图,秀好物等图片路径 **/
	private String SHOW_IMG;

	/** 发布者的手机号 **/
	private String PHONE;

	/** 秀场所属圈子ID  **/
	private int SPOTLIGHT_CIRCLE_ID;

	/** 圈子名称 **/
	private String CIRCLE_NAME;
	
	/** 点赞数 **/
	private int LIKENUM;
	
	/** 发布的内容 **/
	private String CONTENT;
	
	/** 发布 用户名称 **/
	private String USER_NAME;
	
	/**  秀场ID **/
	private String ID;
	
	/** 当前 list index **/
	private int NUM;
	
	/** 评论数 **/
	private int APPRAISENUM;
	
	/** 秀场类型   1.秀图 2.秀歌 3秀视频 4.秀好物 **/
	private int TYPE;
	
	/** 秀场标题 **/
	private String TITLE;
	
	/**视频略缩图**/
	private String VIDEO_IMG;

	public String getSHOW_FILE() {
		return SHOW_FILE;
	}

	public void setSHOW_FILE(String sHOW_FILE) {
		SHOW_FILE = sHOW_FILE;
	}

	public String getSHOW_GOODS_IDS() {
		return SHOW_GOODS_IDS;
	}

	public void setSHOW_GOODS_IDS(String sHOW_GOODS_IDS) {
		SHOW_GOODS_IDS = sHOW_GOODS_IDS;
	}

	public String getFACE_IMAGE_PATH() {
		return FACE_IMAGE_PATH;
	}

	public void setFACE_IMAGE_PATH(String fACE_IMAGE_PATH) {
		FACE_IMAGE_PATH = fACE_IMAGE_PATH;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getCREATE_TIME() {
		
		return StringUtils.friendly_time(CREATE_TIME);
	}

	public void setCREATE_TIME(String cREATE_TIME) {
		CREATE_TIME = cREATE_TIME;
	}

	public String getSHOW_IMG() {
		return SHOW_IMG;
	}

	public void setSHOW_IMG(String sHOW_IMG) {
		SHOW_IMG = sHOW_IMG;
	}

	public String getPHONE() {
		return PHONE;
	}

	public void setPHONE(String pHONE) {
		PHONE = pHONE;
	}

	public int getSPOTLIGHT_CIRCLE_ID() {
		return SPOTLIGHT_CIRCLE_ID;
	}

	public void setSPOTLIGHT_CIRCLE_ID(int sPOTLIGHT_CIRCLE_ID) {
		SPOTLIGHT_CIRCLE_ID = sPOTLIGHT_CIRCLE_ID;
	}

	public String getCIRCLE_NAME() {
		return CIRCLE_NAME;
	}

	public void setCIRCLE_NAME(String cIRCLE_NAME) {
		CIRCLE_NAME = cIRCLE_NAME;
	}

	public int getLIKENUM() {
		return LIKENUM;
	}

	public void setLIKENUM(int lIKENUM) {
		LIKENUM = lIKENUM;
	}

	public String getCONTENT() {
		return CONTENT;
	}

	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}

	public String getUSER_NAME() {
		return USER_NAME;
	}

	public void setUSER_NAME(String uSER_NAME) {
		USER_NAME = uSER_NAME;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public int getNUM() {
		return NUM;
	}

	public void setNUM(int nUM) {
		NUM = nUM;
	}

	public int getAPPRAISENUM() {
		return APPRAISENUM;
	}

	public void setAPPRAISENUM(int aPPRAISENUM) {
		APPRAISENUM = aPPRAISENUM;
	}

	public int getTYPE() {
		return TYPE;
	}

	public void setTYPE(int tYPE) {
		TYPE = tYPE;
	}

	public String getTITLE() {
		return TITLE;
	}

	public void setTITLE(String tITLE) {
		TITLE = tITLE;
	}

	public String getVIDEO_IMG() {
		return VIDEO_IMG;
	}

	public void setVIDEO_IMG(String vIDEO_IMG) {
		VIDEO_IMG = vIDEO_IMG;
	}

}
