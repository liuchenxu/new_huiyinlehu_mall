package com.huiyin.bean;

import java.util.List;

import com.huiyin.api.URLs;
import com.huiyin.constants.Constants;

public class ZhuanquBean {
	
	private String title;

	private String layout;
	
	private List<ZhuanquGoodbean> listGoodbeans;

	private List<ZhuanquGallery> listGalleries;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ZhuanquGoodbean> getListGoodbeans() {
		return listGoodbeans;
	}

	public void setListGoodbeans(List<ZhuanquGoodbean> listGoodbeans) {
		this.listGoodbeans = listGoodbeans;
	}

	public List<ZhuanquGallery> getListGalleries() {
		return listGalleries;
	}

	public void setListGalleries(List<ZhuanquGallery> listGalleries) {
		this.listGalleries = listGalleries;
	}

	

	@Override
	public String toString() {
		return "ZhuanquBean [title=" + title + ", layout=" + layout
				+ ", listGoodbeans=" + listGoodbeans + ", listGalleries="
				+ listGalleries + "]";
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	/**
	 * 获取分享的图片
	 * @return
	 */
	public String getShareImageUrl() {
		try{
			return listGalleries.get(0).getImageUrl();
		}catch(Exception e){
			return null;
		}
	}

	/**
	 * 获取跳转的url
	 * @return
	 */
	public String getShareTargetUrl(String id, int type) {
		switch (type) {
		case Constants.ZhuangQu_Home_Class:
			
			//分类聚合
			return URLs.queryPrefecture+"?id="+id;

		case Constants.ZhuangQu_Home_Promote:
			
			//活动专题
			return URLs.prefecture+"?id=" + id + "&flag=2";
			
		case Constants.ZhuangQu_Home_Banner:
			
			//首页导航-活动
			return URLs.prefecture+"?id=" + id + "&flag=1";
			
		default:
			
			//活动专题
			return URLs.prefecture+"?id=" + id + "&flag=2";
			
		}
	}

}
