package com.huiyin.bean;

import java.util.ArrayList;

import android.content.Context;

import com.google.gson.Gson;
import com.huiyin.utils.LogUtil;
import com.huiyin.utils.StringUtils;

/**
 * 秀场评论列表
 * @author zhyao
 *
 */
public class MomentsAppraiseListBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5099229419914197917L;

	private int pageIndex;

	private int totalPageNum;

	private int totalNum;

	private ArrayList<AppraiseItem> appraiseList;

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getTotalPageNum() {
		return totalPageNum;
	}

	public void setTotalPageNum(int totalPageNum) {
		this.totalPageNum = totalPageNum;
	}

	public int getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}

	public ArrayList<AppraiseItem> getAppraiseList() {
		return appraiseList;
	}

	public void setAppraiseList(ArrayList<AppraiseItem> appraiseList) {
		this.appraiseList = appraiseList;
	}

	public class AppraiseItem {

		/** 电话 **/
		private String PHONE;
		/** 头像地址 **/
		private String FACE_IMAGE_PATH;
		/** 回复ID **/
		private String REPLY_ID;
		/** 用户ID **/
		private String USER_ID;
		/** 创建时间 **/
		private String CREATE_TIME;
		/** 评论内容 **/
		private String CONTENT;
		/** 回复人电话 **/
		private String REPLY_PHONE;
		/** 用户名 **/
		private String USER_NAME;
		/** 当前index **/
		private int NUM;
		/** 回复人姓名 **/
		private String REPLY_NAME;

		public String getPHONE() {
			return PHONE;
		}

		public void setPHONE(String pHONE) {
			PHONE = pHONE;
		}

		public String getFACE_IMAGE_PATH() {
			return FACE_IMAGE_PATH;
		}

		public void setFACE_IMAGE_PATH(String fACE_IMAGE_PATH) {
			FACE_IMAGE_PATH = fACE_IMAGE_PATH;
		}

		public String getREPLY_ID() {
			return REPLY_ID;
		}

		public void setREPLY_ID(String rEPLY_ID) {
			REPLY_ID = rEPLY_ID;
		}

		public String getUSER_ID() {
			return USER_ID;
		}

		public void setUSER_ID(String uSER_ID) {
			USER_ID = uSER_ID;
		}

		public String getCREATE_TIME() {
			return StringUtils.friendly_time(CREATE_TIME);
		}

		public void setCREATE_TIME(String cREATE_TIME) {
			CREATE_TIME = cREATE_TIME;
		}

		public String getCONTENT() {
			return CONTENT;
		}

		public void setCONTENT(String cONTENT) {
			CONTENT = cONTENT;
		}

		public String getREPLY_PHONE() {
			return REPLY_PHONE;
		}

		public void setREPLY_PHONE(String rEPLY_PHONE) {
			REPLY_PHONE = rEPLY_PHONE;
		}

		public String getUSER_NAME() {
			return USER_NAME;
		}

		public void setUSER_NAME(String uSER_NAME) {
			USER_NAME = uSER_NAME;
		}

		public int getNUM() {
			return NUM;
		}

		public void setNUM(int nUM) {
			NUM = nUM;
		}

		public String getREPLY_NAME() {
			return REPLY_NAME;
		}

		public void setREPLY_NAME(String rEPLY_NAME) {
			REPLY_NAME = rEPLY_NAME;
		}

	}
	
	/**
	 * json解析
	 * @param json
	 * @param context
	 * @return
	 */
	public static MomentsAppraiseListBean explainJson(String json, Context context) {
		Gson gson = new Gson();
		try {
			MomentsAppraiseListBean bean = gson.fromJson(json, MomentsAppraiseListBean.class);
			return bean;
		} catch (Exception e) {
			LogUtil.d("dataExplainJson", e.toString());
			return null;
		}
	}
}
