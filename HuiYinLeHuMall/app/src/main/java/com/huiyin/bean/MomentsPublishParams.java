package com.huiyin.bean;

import java.io.Serializable;

/**
 * 秀场发布参数
 * @author zhyao
 *
 */
public class MomentsPublishParams implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8377274670542076162L;

	/**
	 * 秀场类型   1.秀图 2.秀歌 3秀视频 4.秀好物
	 */
	private String type;
	/**
	 * 标题
	 */
	private String title;
	/**
	 *  圈子ID
	 */
	private String spotlight_circle_id;
	/**
	 *  发布人ID      
	 */
	private String user_id;
	/**
	 *  内容
	 */
	private String content;
	/**
	 * 秀图 秀好物的图片路径  多个图片 "," 分隔
	 */
	private String show_img;
	/**
	 * 秀歌 秀视频的文件路径 多个文件 "," 分隔
	 */
	private String show_file;
	/**
	 * 视频略所图
	 */
	private String video_img;           
	/**
	 * 秀好物的商品ID,应该只有一个 如果多个还是"," 分隔
	 */
	private String show_goods_ids;
	/**
	 *  数据来源  1: web 2: pc 3:android, 4:ios 5:wp
	 */
	private String origin;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSpotlight_circle_id() {
		return spotlight_circle_id;
	}

	public void setSpotlight_circle_id(String spotlight_circle_id) {
		this.spotlight_circle_id = spotlight_circle_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getShow_img() {
		return show_img;
	}

	public void setShow_img(String show_img) {
		this.show_img = show_img;
	}

	public String getShow_file() {
		return show_file;
	}

	public void setShow_file(String show_file) {
		this.show_file = show_file;
	}

	public String getShow_goods_ids() {
		return show_goods_ids;
	}

	public void setShow_goods_ids(String show_goods_ids) {
		this.show_goods_ids = show_goods_ids;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getVideo_img() {
		return video_img;
	}

	public void setVideo_img(String video_img) {
		this.video_img = video_img;
	}
	
	
}
