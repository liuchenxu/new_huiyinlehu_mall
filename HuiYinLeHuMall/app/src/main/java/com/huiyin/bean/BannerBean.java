package com.huiyin.bean;

import java.io.Serializable;

import android.content.Context;

import com.google.gson.Gson;

public class BannerBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 528895292111130341L;

	public int SORT;

	/**
	 * 广告跳转ID
	 */
	public int BANNER_JUMP_ID;

	/**
	 * 目前只有BANNER_JUMP_FLAG=6时，存放视频播放地址
	 */
	public String BANNER_CONTENT;

	/**
	 * 广告图片
	 */
	public String BANNER_IMG;

	/**
	 * 活动id(BANNER_JUMP_FLAG为1或者5时有用)
	 */
	public int ID;

	public int BANNER_LAYOUT;

	/**
	 *  banner跳转标志 1活动介绍,2商品详细页,3专区,4快捷服务,5活动H5网页,6视频
	 */
	public int BANNER_JUMP_FLAG;

	public int STATUS;

	public int NUM;

	public String BANNER_NAME;

	public static BannerBean explainJson(String json, Context context) {

		Gson gson = new Gson();
		try {
			BannerBean bean = gson.fromJson(json, BannerBean.class);
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
	}

}
