package com.huiyin.bean;

import java.util.ArrayList;

import android.content.Context;

import com.google.gson.Gson;
import com.huiyin.utils.LogUtil;

/**
 * 秀好物商品列表bean
 * 
 * @author zhyao
 * 
 */
public class MomentsGoodsListBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1808085966118130995L;

	private int pageIndex;

	private int totalPageNum;

	/** 商品列表 **/
	private ArrayList<MomentsGoodsItem> goodsList;

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getTotalPageNum() {
		return totalPageNum;
	}

	public void setTotalPageNum(int totalPageNum) {
		this.totalPageNum = totalPageNum;
	}

	public ArrayList<MomentsGoodsItem> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(ArrayList<MomentsGoodsItem> goodsList) {
		this.goodsList = goodsList;
	}

	

	/**
	 * json解析
	 * @param json
	 * @param context
	 * @return
	 */
	public static MomentsGoodsListBean explainJson(String json, Context context) {
		Gson gson = new Gson();
		try {
			MomentsGoodsListBean bean = gson.fromJson(json, MomentsGoodsListBean.class);
			return bean;
		} catch (Exception e) {
			LogUtil.d("dataExplainJson", e.toString());
			return null;
		}
	}


}
