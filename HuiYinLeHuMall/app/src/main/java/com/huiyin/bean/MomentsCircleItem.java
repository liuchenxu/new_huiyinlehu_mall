package com.huiyin.bean;

import java.io.Serializable;

import com.huiyin.utils.StringUtils;

/**
 * 秀场首页圈子
 * @author zhyao
 *
 */
public class MomentsCircleItem implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2432864508176015523L;

	/**
	 * 圈子名
	 */
	private String NAME;

	/**
	 * 圈子ID
	 */
	private String ID;

	/**
	 * 圈子图片
	 */
	private String IMG;
	
	/**
	 * 今日数量(选择圈子列表字段)
	 */
	private String COUNT;
	
	/**
	 * 圈子创建时间(选择圈子列表字段)
	 */
	private String CREATE_TIME;
	
	/**
	 * 圈子描述(选择圈子列表字段)
	 */
	private String DESCRIPTION;

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getIMG() {
		return IMG;
	}

	public void setIMG(String iMG) {
		IMG = iMG;
	}

	public String getCOUNT() {
		return COUNT;
	}

	public void setCOUNT(String cOUNT) {
		COUNT = cOUNT;
	}

	public String getCREATE_TIME() {
		return StringUtils.friendly_time(CREATE_TIME);
	}

	public void setCREATE_TIME(String cREATE_TIME) {
		CREATE_TIME = cREATE_TIME;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}

}
