package com.huiyin.bean;

import java.io.Serializable;

import android.content.Context;

import com.google.gson.Gson;
import com.huiyin.utils.LogUtil;

/**
 * 我的推广
 * 
 * @author zhyao
 * 
 */
public class PromoterBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4636891559026238324L;

	private boolean success;

	private Promoter promoter;

	public class Promoter {
		private String id;
		private String name;
		private String phone;
		private String idCardNo;
		private String referralCode;
		private RebatesStatistics rebatesStatistics;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getIdCardNo() {
			return idCardNo;
		}

		public void setIdCardNo(String idCardNo) {
			this.idCardNo = idCardNo;
		}

		public String getReferralCode() {
			return referralCode;
		}

		public void setReferralCode(String referralCode) {
			this.referralCode = referralCode;
		}

		public RebatesStatistics getRebatesStatistics() {
			return rebatesStatistics;
		}

		public void setRebatesStatistics(RebatesStatistics rebatesStatistics) {
			this.rebatesStatistics = rebatesStatistics;
		}

	}

	public class RebatesStatistics {

		private String amount;
		private String currentMonthLastDate;
		private String currentMonthRegisterNum;
		private String orderNum;
		private String totalOrderAmount;
		private String totalRegisterNum;

		public String getAmount() {
			return amount;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}

		public String getCurrentMonthLastDate() {
			return currentMonthLastDate;
		}

		public void setCurrentMonthLastDate(String currentMonthLastDate) {
			this.currentMonthLastDate = currentMonthLastDate;
		}

		public String getCurrentMonthRegisterNum() {
			return currentMonthRegisterNum;
		}

		public void setCurrentMonthRegisterNum(String currentMonthRegisterNum) {
			this.currentMonthRegisterNum = currentMonthRegisterNum;
		}

		public String getOrderNum() {
			return orderNum;
		}

		public void setOrderNum(String orderNum) {
			this.orderNum = orderNum;
		}

		public String getTotalOrderAmount() {
			return totalOrderAmount;
		}

		public void setTotalOrderAmount(String totalOrderAmount) {
			this.totalOrderAmount = totalOrderAmount;
		}

		public String getTotalRegisterNum() {
			return totalRegisterNum;
		}

		public void setTotalRegisterNum(String totalRegisterNum) {
			this.totalRegisterNum = totalRegisterNum;
		}

	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Promoter getPromoter() {
		return promoter;
	}

	public void setPromoter(Promoter promoter) {
		this.promoter = promoter;
	}

	/**
	 * json解析
	 * 
	 * @param json
	 * @param context
	 * @return
	 */
	public static PromoterBean explainJson(String json, Context context) {
		Gson gson = new Gson();
		try {
			PromoterBean bean = gson.fromJson(json, PromoterBean.class);
			return bean;
		} catch (Exception e) {
			LogUtil.d("dataExplainJson", e.toString());
			return null;
		}
	}

}
