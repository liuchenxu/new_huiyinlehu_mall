package com.huiyin.common.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class LiveUtil {

	/**
	 * 登录
	 * 
	 * @param context
	 */
	public static void startLogin(Activity context) {
		Intent intent = new Intent();
		intent.setClassName("com.huiyin", "com.huiyin.ui.user.LoginActivity");
		intent.putExtra("Action", "GRLive");
		context.startActivityForResult(intent, EntityConstant.LoginCode);
	}

	/**
	 * 商品详情
	 * 
	 * @param context
	 * @param goodsId
	 * @param goodsNo
	 * @param storeId
	 */
	public static void startGoodDetail(Context context, String goodsId,
			String goodsNo, String storeId) {
		Intent intent = new Intent();
		intent.setClassName("com.huiyin", "com.huiyin.ui.classic.ProductsDetailActivity");
		intent.putExtra("goods_detail_id", goodsId);
		intent.putExtra("STORE_ID", storeId);
		intent.putExtra("GOODS_NO", goodsNo);
		context.startActivity(intent);
	}

	/**
	 * 分享
	 * 
	 * @param activity
	 * @param activity
	 *            上下文
	 * @param title
	 *            分享标题
	 * @param url
	 *            分享跳转地址
	 * @param imageUrl
	 *            分享显示的图片地址
	 * @param text
	 *            分享内容
	 */
	public static void startShare(Activity activity, String title, String url,
			String imageUrl, String text) {
		try {
			Class<?> shareUtils = Class.forName("com.huiyin.utils.ShareUtils");
			Method showShare = shareUtils.getMethod("showShare", Activity.class, String.class, String.class, String.class, String.class);
			showShare.invoke(null, activity, title, url, imageUrl, text);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 支付
	 * 
	 * @param context
	 * @param amounts
	 *            金额
	 * @param point
	 *            金币
	 */
	public static void startPay(Context context, double amounts, Integer point) {
		try {
			Class<?> grLiveUtil = Class.forName("com.huiyin.utils.GRLiveUtil");
			Method startPay = grLiveUtil.getMethod("startPay", Context.class, String.class, String.class);
			startPay.invoke(null, context, String.valueOf(amounts), String.valueOf(point));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

}
