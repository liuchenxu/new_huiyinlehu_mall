package com.huiyin.broadcast;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.huiyin.ui.MainActivity;
import com.huiyin.utils.AppManager;
import com.huiyin.utils.YsfUtil;
import com.qiyukf.nimlib.sdk.NimIntent;

public class YsfMessageReceiverActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		parseIntent();
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		
		parseIntent();
	}
	
	private void parseIntent() {
		
		//程序退出的状态下拉起主页面
		if(AppManager.getAppManager().getActivitySize() == 0) {
			//跳转到主界面
			Intent i = new Intent();
			i.setClass(this, MainActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			i.putExtra(MainActivity.From, "login");
			startActivity(i);
		}
		
        Intent intent = getIntent();
        if (intent.hasExtra(NimIntent.EXTRA_NOTIFY_CONTENT)) {
            YsfUtil.consultService(this, null, null, null);
            // 最好将intent清掉，以免从堆栈恢复时又打开客服窗口
            setIntent(new Intent());
        }
        finish();
    }
}
