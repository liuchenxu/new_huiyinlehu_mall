/**
 * 
 */
package com.huiyin.newpackage.utils;

import android.text.TextUtils;

import com.huiyin.AppContext;

/**
 * @author liuchenxu 用户信息处理工具类
 */
public class UserInfoUtils {

	/**
	 * 判断用户是否登录的工具类
	 * 
	 * @return true 已经登录 false 没有登录
	 */
	public static boolean IsUserLogin() {
		String userId = AppContext.userId;
		if (!TextUtils.isEmpty(userId) && null != AppContext.mUserInfo) {
			return true;
		}
		return false;
	}
}
