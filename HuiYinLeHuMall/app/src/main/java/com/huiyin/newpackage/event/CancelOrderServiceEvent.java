/**
 * 
 */
package com.huiyin.newpackage.event;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.huiyin.api.URLs;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.android.http.AsyncHttpResponseHandler;
import com.huiyin.newpackage.android.http.RequestParams;

import de.greenrobot.event.EventBus;

/**
 * 取消售后申请
 * 
 * @author liuchenxu
 * 
 */
public class CancelOrderServiceEvent extends Event {
	public static void CancelOrderServiceEvent(AsyncHttpClient client, String order_service_id) {
		RequestParams params = new RequestParams();
		params.put("order_service_id", order_service_id);
		client.post(URLs.cancel_apply_service_order, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				String str = new String(responseBody);
				Gson gson = new Gson();
				CancelOrderServiceEvent event = gson.fromJson(str, CancelOrderServiceEvent.class);
				EventBus.getDefault().post(event);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				CancelOrderServiceEvent event = new CancelOrderServiceEvent();
				CancelOrderServiceEvent.onFailure(event);
			}

		});
	}
}
