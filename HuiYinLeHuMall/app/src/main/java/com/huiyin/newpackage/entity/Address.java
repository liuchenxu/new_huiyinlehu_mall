/**
 * 
 */
package com.huiyin.newpackage.entity;

import java.io.Serializable;

/**
 * @author liuchenxu
 *	地址信息实体类
 */
public class Address implements Serializable {
	
	private String ADDRESS;

	private String AREANAME;

	private String CITYNAME;

	private long ID;

	private String PROVINCENAME;

	private long STORE_ID;

	public String getADDRESS() {
		return ADDRESS;
	}

	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}

	public String getAREANAME() {
		return AREANAME;
	}

	public void setAREANAME(String aREANAME) {
		AREANAME = aREANAME;
	}

	public String getCITYNAME() {
		return CITYNAME;
	}

	public void setCITYNAME(String cITYNAME) {
		CITYNAME = cITYNAME;
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public String getPROVINCENAME() {
		return PROVINCENAME;
	}

	public void setPROVINCENAME(String pROVINCENAME) {
		PROVINCENAME = pROVINCENAME;
	}

	public long getSTORE_ID() {
		return STORE_ID;
	}

	public void setSTORE_ID(long sTORE_ID) {
		STORE_ID = sTORE_ID;
	}
	
	
}
