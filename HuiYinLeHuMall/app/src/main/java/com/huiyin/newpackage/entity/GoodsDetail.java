package com.huiyin.newpackage.entity;

import java.io.Serializable;

public class GoodsDetail implements Serializable {
	
	private String AFTER_SALES_CODE;

//	private int AFTER_SALES_ID;

	private int AFTER_SALES_STATUS;

	private long GOODS_ID;

	private String GOODS_IMG;

	private String GOODS_NAME;

	private long GOODS_NO;

	private float GOODS_PRICE;

	private int OLD_QUANTITY;

	private int ORDER_DETAIL_ID;

	private int ORDER_ID;

	private float PRICE;

	private int QUANTITY;//商品购买数量

	private int STORE_ID;
	
	private String GOODS_NORMS;
	
	private int GOODS_QUANTITY;//退货数量
	
	private long ORDER_DETAILS_ID;
	
	private long ORDER_SERVICE_DETAIL_ID;

	private float REFUND_AMOUNT;

	private String REFUND_AMOUNT_EXPLAIN;

	private long SERVICE_ID;
	
    public String NUM_GOODS_NEED_RETURN;//退换货的数量

	public String getAFTER_SALES_CODE() {
		return AFTER_SALES_CODE;
	}

	public void setAFTER_SALES_CODE(String aFTER_SALES_CODE) {
		AFTER_SALES_CODE = aFTER_SALES_CODE;
	}

//	public int getAFTER_SALES_ID() {
//		return AFTER_SALES_ID;
//	}
//
//	public void setAFTER_SALES_ID(int aFTER_SALES_ID) {
//		AFTER_SALES_ID = aFTER_SALES_ID;
//	}

	public int getAFTER_SALES_STATUS() {
		return AFTER_SALES_STATUS;
	}

	public void setAFTER_SALES_STATUS(int aFTER_SALES_STATUS) {
		AFTER_SALES_STATUS = aFTER_SALES_STATUS;
	}

	public long getGOODS_ID() {
		return GOODS_ID;
	}

	public void setGOODS_ID(long gOODS_ID) {
		GOODS_ID = gOODS_ID;
	}

	public String getGOODS_IMG() {
		return GOODS_IMG;
	}

	public void setGOODS_IMG(String gOODS_IMG) {
		GOODS_IMG = gOODS_IMG;
	}

	public String getGOODS_NAME() {
		return GOODS_NAME;
	}

	public void setGOODS_NAME(String gOODS_NAME) {
		GOODS_NAME = gOODS_NAME;
	}

	public long getGOODS_NO() {
		return GOODS_NO;
	}

	public void setGOODS_NO(long gOODS_NO) {
		GOODS_NO = gOODS_NO;
	}

	public float getGOODS_PRICE() {
		return GOODS_PRICE;
	}

	public void setGOODS_PRICE(float gOODS_PRICE) {
		GOODS_PRICE = gOODS_PRICE;
	}

	public int getOLD_QUANTITY() {
		return OLD_QUANTITY;
	}

	public void setOLD_QUANTITY(int oLD_QUANTITY) {
		OLD_QUANTITY = oLD_QUANTITY;
	}

	public int getORDER_DETAIL_ID() {
		return ORDER_DETAIL_ID;
	}

	public void setORDER_DETAIL_ID(int oRDER_DETAIL_ID) {
		ORDER_DETAIL_ID = oRDER_DETAIL_ID;
	}

	public int getORDER_ID() {
		return ORDER_ID;
	}

	public void setORDER_ID(int oRDER_ID) {
		ORDER_ID = oRDER_ID;
	}

	public float getPRICE() {
		return PRICE;
	}

	public void setPRICE(float pRICE) {
		PRICE = pRICE;
	}

	public int getQUANTITY() {
		return QUANTITY;
	}

	public void setQUANTITY(int qUANTITY) {
		QUANTITY = qUANTITY;
	}

	public int getSTORE_ID() {
		return STORE_ID;
	}

	public void setSTORE_ID(int sTORE_ID) {
		STORE_ID = sTORE_ID;
	}
	
	public void setInitNumGoodsNeedReturn(){
		NUM_GOODS_NEED_RETURN=this.QUANTITY+"";
	}

	public String getGOODS_NORMS() {
		return GOODS_NORMS;
	}

	public void setGOODS_NORMS(String gOODS_NORMS) {
		GOODS_NORMS = gOODS_NORMS;
	}

	public int getGOODS_QUANTITY() {
		return GOODS_QUANTITY;
	}

	public void setGOODS_QUANTITY(int gOODS_QUANTITY) {
		GOODS_QUANTITY = gOODS_QUANTITY;
	}

	public long getORDER_DETAILS_ID() {
		return ORDER_DETAILS_ID;
	}

	public void setORDER_DETAILS_ID(long oRDER_DETAILS_ID) {
		ORDER_DETAILS_ID = oRDER_DETAILS_ID;
	}

	public long getORDER_SERVICE_DETAIL_ID() {
		return ORDER_SERVICE_DETAIL_ID;
	}

	public void setORDER_SERVICE_DETAIL_ID(long oRDER_SERVICE_DETAIL_ID) {
		ORDER_SERVICE_DETAIL_ID = oRDER_SERVICE_DETAIL_ID;
	}

	public float getREFUND_AMOUNT() {
		return REFUND_AMOUNT;
	}

	public void setREFUND_AMOUNT(float rEFUND_AMOUNT) {
		REFUND_AMOUNT = rEFUND_AMOUNT;
	}

	public String getREFUND_AMOUNT_EXPLAIN() {
		return REFUND_AMOUNT_EXPLAIN;
	}

	public void setREFUND_AMOUNT_EXPLAIN(String rEFUND_AMOUNT_EXPLAIN) {
		REFUND_AMOUNT_EXPLAIN = rEFUND_AMOUNT_EXPLAIN;
	}

	public long getSERVICE_ID() {
		return SERVICE_ID;
	}

	public void setSERVICE_ID(long sERVICE_ID) {
		SERVICE_ID = sERVICE_ID;
	}
	
	
}
