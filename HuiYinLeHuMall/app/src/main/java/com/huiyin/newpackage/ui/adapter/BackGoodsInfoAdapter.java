package com.huiyin.newpackage.ui.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.bean.ReturnDetailBean.GoodReturnBean;
import com.huiyin.newpackage.utils.ViewHolder;
import com.huiyin.utils.ImageManager;

/**
 * 换货流程-商品adapter
 * 
 * @author 刘晨旭
 * 
 */
public class BackGoodsInfoAdapter extends BaseAdapter {
	
	private Context context;
	
	/**上下文**/
	private List<GoodReturnBean> dataList;
	
	public BackGoodsInfoAdapter(Context context, List<GoodReturnBean> dataList) {
		this.context = context;
		this.dataList = dataList;
	}

	/**
	 * 刷新数据
	 * @param dataList
	 */
	public void refreshData(List<GoodReturnBean> dataList) {
		this.dataList = dataList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null != dataList ? dataList.size() : 0; 
	}

	@Override
	public Object getItem(int position) {
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			convertView = LayoutInflater.from(context).inflate(R.layout.adapter_back_goods_process, null);
		}
		initView(convertView,position);
		return convertView;
	}
	
	private void initView(View convertView,final int position){
		GoodReturnBean bean=dataList.get(position);
		ImageView iv_logo = ViewHolder.get(convertView, R.id.goods_pic_imageview);
		TextView tv_name = ViewHolder.get(convertView, R.id.goods_name_tv);
		TextView tv_norms = ViewHolder.get(convertView, R.id.norms_value_tv);
		TextView numberTv = ViewHolder.get(convertView, R.id.apply_product_count_textview_new);
		//名称，规格
		ImageManager.LoadWithServer(bean.GOODS_IMG, iv_logo);
		tv_name.setText(bean.GOODS_NAME);
		tv_norms.setText(bean.NORMS_VALUE);
		numberTv.setText(bean.QUANTITY);
	}
}
