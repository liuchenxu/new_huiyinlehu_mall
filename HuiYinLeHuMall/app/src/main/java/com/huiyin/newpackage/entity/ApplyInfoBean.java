/**
 * 
 */
package com.huiyin.newpackage.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 退款申请实体类
 * @author liuchenxu
 *
 */
public class ApplyInfoBean implements Serializable {
	
	private String STORE_LOGO;

	private String APPLY_PICTURES;

	private String APPLY_REASON;

	private String APPLY_REMARKS;

	private int APPLY_STATE;

	private String APPLY_STATE_NAME;

	private String CREATE_TIME;

	private int IS_COMPLETE;

	private int NUM;

	private String ORDER_CODE;

	private long ORDER_ID;

	private int REFUNDS_MODE;

	private String REFUNDS_MODE_NAME;

	private double REFUND_AMOUNT;

	private int REFUND_METHOD;

	private String REFUND_METHOD_NAME;

	private int SERVICE_TYPE;

	private long STORE_ID;

	private String STORE_NAME;

	private String UPDATE_TIME;

	private long USER_ID;

	private String USER_NAME;

	private String USER_PHONE;
	
	private String STATUS_NAME;
	
	private float TOTAL_PRICE;
	
	private String REMAINING_TIME;
	
	private String SERVICE_CODE;
	
	private long ORDER_SERVICE_ID;
	
	private ArrayList<GoodsDetail> orderServiceDetalList;

	public String getAPPLY_PICTURES() {
		return APPLY_PICTURES;
	}

	public void setAPPLY_PICTURES(String aPPLY_PICTURES) {
		APPLY_PICTURES = aPPLY_PICTURES;
	}

	public String getAPPLY_REASON() {
		return APPLY_REASON;
	}

	public void setAPPLY_REASON(String aPPLY_REASON) {
		APPLY_REASON = aPPLY_REASON;
	}

	public String getAPPLY_REMARKS() {
		return APPLY_REMARKS;
	}

	public void setAPPLY_REMARKS(String aPPLY_REMARKS) {
		APPLY_REMARKS = aPPLY_REMARKS;
	}

	public int getAPPLY_STATE() {
		return APPLY_STATE;
	}

	public void setAPPLY_STATE(int aPPLY_STATE) {
		APPLY_STATE = aPPLY_STATE;
	}

	public String getAPPLY_STATE_NAME() {
		return APPLY_STATE_NAME;
	}

	public void setAPPLY_STATE_NAME(String aPPLY_STATE_NAME) {
		APPLY_STATE_NAME = aPPLY_STATE_NAME;
	}

	public String getCREATE_TIME() {
		return CREATE_TIME;
	}

	public void setCREATE_TIME(String cREATE_TIME) {
		CREATE_TIME = cREATE_TIME;
	}

	public int getIS_COMPLETE() {
		return IS_COMPLETE;
	}

	public void setIS_COMPLETE(int iS_COMPLETE) {
		IS_COMPLETE = iS_COMPLETE;
	}

	public int getNUM() {
		return NUM;
	}

	public void setNUM(int nUM) {
		NUM = nUM;
	}

	public String getORDER_CODE() {
		return ORDER_CODE;
	}

	public void setORDER_CODE(String oRDER_CODE) {
		ORDER_CODE = oRDER_CODE;
	}

	public long getORDER_ID() {
		return ORDER_ID;
	}

	public void setORDER_ID(long oRDER_ID) {
		ORDER_ID = oRDER_ID;
	}

	public long getORDER_SERVICE_ID() {
		return ORDER_SERVICE_ID;
	}

	public void setORDER_SERVICE_ID(long oRDER_SERVICE_ID) {
		ORDER_SERVICE_ID = oRDER_SERVICE_ID;
	}

	public int getREFUNDS_MODE() {
		return REFUNDS_MODE;
	}

	public void setREFUNDS_MODE(int rEFUNDS_MODE) {
		REFUNDS_MODE = rEFUNDS_MODE;
	}

	public String getREFUNDS_MODE_NAME() {
		return REFUNDS_MODE_NAME;
	}

	public void setREFUNDS_MODE_NAME(String rEFUNDS_MODE_NAME) {
		REFUNDS_MODE_NAME = rEFUNDS_MODE_NAME;
	}

	public double getREFUND_AMOUNT() {
		return REFUND_AMOUNT;
	}

	public void setREFUND_AMOUNT(double rEFUND_AMOUNT) {
		REFUND_AMOUNT = rEFUND_AMOUNT;
	}

	public int getREFUND_METHOD() {
		return REFUND_METHOD;
	}

	public void setREFUND_METHOD(int rEFUND_METHOD) {
		REFUND_METHOD = rEFUND_METHOD;
	}

	public String getREFUND_METHOD_NAME() {
		return REFUND_METHOD_NAME;
	}

	public void setREFUND_METHOD_NAME(String rEFUND_METHOD_NAME) {
		REFUND_METHOD_NAME = rEFUND_METHOD_NAME;
	}

	public int getSERVICE_TYPE() {
		return SERVICE_TYPE;
	}

	public void setSERVICE_TYPE(int sERVICE_TYPE) {
		SERVICE_TYPE = sERVICE_TYPE;
	}

	public long getSTORE_ID() {
		return STORE_ID;
	}

	public void setSTORE_ID(long sTORE_ID) {
		STORE_ID = sTORE_ID;
	}

	public String getSTORE_NAME() {
		return STORE_NAME;
	}

	public void setSTORE_NAME(String sTORE_NAME) {
		STORE_NAME = sTORE_NAME;
	}

	public String getUPDATE_TIME() {
		return UPDATE_TIME;
	}

	public void setUPDATE_TIME(String uPDATE_TIME) {
		UPDATE_TIME = uPDATE_TIME;
	}

	public long getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(long uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getUSER_NAME() {
		return USER_NAME;
	}

	public void setUSER_NAME(String uSER_NAME) {
		USER_NAME = uSER_NAME;
	}

	public String getUSER_PHONE() {
		return USER_PHONE;
	}

	public void setUSER_PHONE(String uSER_PHONE) {
		USER_PHONE = uSER_PHONE;
	}

	public ArrayList<GoodsDetail> getOrderServiceDetalList() {
		return orderServiceDetalList;
	}

	public void setOrderServiceDetalList(ArrayList<GoodsDetail> orderServiceDetalList) {
		this.orderServiceDetalList = orderServiceDetalList;
	}
	
	
	public String getSTORE_LOGO() {
		return STORE_LOGO;
	}

	public void setSTORE_LOGO(String sTORE_LOGO) {
		STORE_LOGO = sTORE_LOGO;
	}

	public String getSTATUS_NAME() {
		return STATUS_NAME;
	}

	public void setSTATUS_NAME(String sTATUS_NAME) {
		STATUS_NAME = sTATUS_NAME;
	}

	public float getTOTAL_PRICE() {
		return TOTAL_PRICE;
	}

	public void setTOTAL_PRICE(float tOTAL_PRICE) {
		TOTAL_PRICE = tOTAL_PRICE;
	}

	public String getREMAINING_TIME() {
		return REMAINING_TIME;
	}

	public void setREMAINING_TIME(String rEMAINING_TIME) {
		REMAINING_TIME = rEMAINING_TIME;
	}

	public String getSERVICE_CODE() {
		return SERVICE_CODE;
	}

	public void setSERVICE_CODE(String sERVICE_CODE) {
		SERVICE_CODE = sERVICE_CODE;
	}
	
	
}
