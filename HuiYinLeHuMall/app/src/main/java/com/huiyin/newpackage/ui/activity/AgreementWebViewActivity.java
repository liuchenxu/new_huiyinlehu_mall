/**
 * 
 */
package com.huiyin.newpackage.ui.activity;

import com.huiyin.R;
import com.huiyin.newpackage.base.NewBaseActivity;
import com.huiyin.newpackage.consts.HTTPConsts;
import com.huiyin.newpackage.event.GetAggrementInfoEvent;
import com.huiyin.newpackage.utils.HtmlUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.TextView;

/**
 * @author liuchenxu 加载协议的Activity
 */
public class AgreementWebViewActivity extends NewBaseActivity {
	private TextView ab_title;
	private TextView ab_back;
	private WebView web_view;
	
	private String flag="";
	private String title="";
	private static final String KEY_TITLE="KEY_TITLE";
	private static final String KEY_HTML_FLAG="KEY_FLAG";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		register=true;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_agreement_webview);
		Intent intent=getIntent();
		if(intent==null){
			this.finish();
		}
		else{
			flag=intent.getStringExtra(KEY_HTML_FLAG);
			title=intent.getStringExtra(KEY_TITLE);
			initViews();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		showProgress(true);
		GetAggrementInfoEvent.GetAggrementInfo(client, flag);
	}
	
	private void initViews(){
		ab_title=(TextView)findViewById(R.id.ab_title);
		web_view=(WebView)findViewById(R.id.web_view);
		ab_back=(TextView)findViewById(R.id.ab_back);
		ab_title.setText(title);
		ab_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AgreementWebViewActivity.this.finish();
			}
		});
	}
	
	public void onEvent(GetAggrementInfoEvent event){
		finishProgress();
		if(event.type==HTTPConsts.HTTP_OK){
			String htmlContent=event.getContent();
			Log.i("onEvent", htmlContent);
			String html=HtmlUtils.FormatHtmlContent(htmlContent);
			web_view.getSettings().setDefaultTextEncodingName("UTF-8") ;
//			web_view.loadData(htmlContent,"text/html","UTF-8") ;
			web_view.loadData(htmlContent, "text/html; charset=UTF-8", null);
		}
		else{
			showToast(event.msg);
		}
	}
}
