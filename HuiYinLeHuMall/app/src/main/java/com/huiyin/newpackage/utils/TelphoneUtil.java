package com.huiyin.newpackage.utils;


import android.util.Log;

public class TelphoneUtil {
    /**
     * 手机号格式处理
     */

    public static String getHiddenResult(String telphone) {
        StringBuffer buffer = new StringBuffer();
        char[] arry = telphone.toCharArray();
        for (int i = 0; i < arry.length; i++) {
            if (i >= 3 && i < 7) {
                buffer.append("*");

            } else {
                buffer.append(arry[i]);
            }
        }

        return buffer.toString();

    }

    public static boolean checkTelphoneType(String telphone) {
        if (telphone == null || telphone.isEmpty() || telphone.length() != 11)

            return false;

        if (telphone.length() == 11 && telphone.startsWith("1", 0)) {
            Log.e("checkTelphoneType", "3" + telphone.startsWith("1", 0));
            return true;
        } else {
            return false;
        }

    }


}
