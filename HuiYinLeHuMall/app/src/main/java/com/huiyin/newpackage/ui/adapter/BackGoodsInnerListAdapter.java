package com.huiyin.newpackage.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.bean.BespeakDetalListEntity;
import com.huiyin.bean.GoodList;
import com.huiyin.bean.OrderDetail;
import com.huiyin.bean.OrderRecordBean;
import com.huiyin.newpackage.entity.GoodsDetail;
import com.huiyin.newpackage.entity.OrderInfo;
import com.huiyin.newpackage.utils.ViewHolder;
import com.huiyin.ui.user.order.BackGoodsActivity;
import com.huiyin.ui.user.order.ReplaceAddActivity;
import com.huiyin.utils.ImageManager;

/**
 * 售后申请内部嵌套的ListView
 * 
 * @author liuchenxu
 * 
 */
public class BackGoodsInnerListAdapter extends BaseAdapter {

	private Context context;
	
	private boolean canReturn;

	private List<GoodsDetail> items = new ArrayList<GoodsDetail>();
	
	private String ID;
	
	private OrderInfo order;

	public BackGoodsInnerListAdapter(List<GoodsDetail> mList, Context context,boolean canReturn,OrderInfo order) {
		this.context = context;
		if (mList != null) {
			this.items.clear();
			this.items.addAll(mList);
		}
		this.canReturn=canReturn;
		this.ID=order.getID()+"";
		this.order=order;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.adapter_back_goods_apply_service, null);
		}
		initViews(convertView, position);
		return convertView;
	}

	/**
	 * 
	 */
	private void initViews(View view, int position) {
		final GoodsDetail detail = items.get(position);
		ImageView goods_pic_imageview = ViewHolder.get(view, R.id.goods_pic_imageview);// 商品图片
		TextView goods_name_tv = ViewHolder.get(view, R.id.goods_name_tv);// 商品名称
		TextView norms_value_tv = ViewHolder.get(view, R.id.norms_value_tv);
		TextView apply_product_count_textview_new = ViewHolder.get(view, R.id.apply_product_count_textview_new);
		TextView back_goods_num = ViewHolder.get(view, R.id.back_goods_num);
		back_goods_num.setText("数量: x"+detail.getQUANTITY());
		TextView adapter_back_good = ViewHolder.get(view, R.id.adapter_back_good);// 退货按钮
		ImageManager.LoadWithServer(detail.getGOODS_IMG(), goods_pic_imageview);
		goods_name_tv.setText(detail.getGOODS_NAME());
		if(this.canReturn){
			adapter_back_good.setVisibility(View.VISIBLE);
		}
		else{
			adapter_back_good.setVisibility(View.INVISIBLE);
		}
		adapter_back_good.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, BackGoodsActivity.class);
				intent.putExtra("Order", order);
				intent.putExtra(BackGoodsActivity.EXTRA_ORDERID, order.getID()+"");
				intent.putExtra("ID", detail.getGOODS_ID()+"");
				context.startActivity(intent);
			}
		});
	}	
}
