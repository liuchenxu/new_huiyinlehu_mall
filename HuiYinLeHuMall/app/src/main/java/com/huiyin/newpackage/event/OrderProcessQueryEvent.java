package com.huiyin.newpackage.event;

import java.util.ArrayList;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.huiyin.AppContext;
import com.huiyin.api.URLs;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.android.http.AsyncHttpResponseHandler;
import com.huiyin.newpackage.android.http.RequestParams;
import com.huiyin.newpackage.entity.ApplyInfoBean;

import de.greenrobot.event.EventBus;

/**
 * 进度查询的Event
 * 
 * @author liuchenxu
 * 
 */
public class OrderProcessQueryEvent extends Event {
	
	private int pageIndex;
	private int totalPageNum;
	private ArrayList<ApplyInfoBean> list;
	
	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}



	public int getTotalPageNum() {
		return totalPageNum;
	}



	public void setTotalPageNum(int totalPageNum) {
		this.totalPageNum = totalPageNum;
	}

	public ArrayList<ApplyInfoBean> getList() {
		return list;
	}

	public void setList(ArrayList<ApplyInfoBean> list) {
		this.list = list;
	}

	public static void QueryOrderProcessList(AsyncHttpClient client, int pageIndex) {
		RequestParams params = new RequestParams();
		params.put("user_id", AppContext.userId);
		params.put("pageIndex", pageIndex+"");
		client.post(URLs.applied_servcie_order_list, params, new AsyncHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				String str = new String(responseBody);
				Gson gson = new Gson();
				OrderProcessQueryEvent event = gson.fromJson(str, OrderProcessQueryEvent.class);
				EventBus.getDefault().post(event);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				OrderProcessQueryEvent event = new OrderProcessQueryEvent();
				OrderProcessQueryEvent.onFailure(event);
			}
			
		} );
	}
}
