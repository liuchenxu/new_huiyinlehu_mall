package com.huiyin.newpackage.entity;

/**
 * 对应退货中的物流跟踪、进度查询对应的实体类
 * 
 * @author liuchenxu
 * 
 */
public class BackGoodsStepInfo {
	private String first_time;
	private String second_time;
	private String main_info;
	private String sub_info;

	public BackGoodsStepInfo(String firstTime, String mainInfo) {
		first_time = firstTime;
		main_info = mainInfo;
	}

	public String getFirst_time() {
		return first_time;
	}

	public void setFirst_time(String first_time) {
		this.first_time = first_time;
	}

	public String getSecond_time() {
		return second_time;
	}

	public void setSecond_time(String second_time) {
		this.second_time = second_time;
	}

	public String getMain_info() {
		return main_info;
	}

	public void setMain_info(String main_info) {
		this.main_info = main_info;
	}

	public String getSub_info() {
		return sub_info;
	}

	public void setSub_info(String sub_info) {
		this.sub_info = sub_info;
	}

}
