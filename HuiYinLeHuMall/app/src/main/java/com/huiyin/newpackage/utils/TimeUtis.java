package com.huiyin.newpackage.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


import android.content.Context;
import android.util.Log;

/**
 * @author 赵云霄
 * 
 * @time:2016-06-13
 * 
 * @function:时间工具类,处理时间的转换和获取
 * */
public class TimeUtis {

	/**
	 * 获取当前系统的月份月份从1开始
	 * */
	public static int getMonth() {
		Calendar calendar = Calendar.getInstance(Locale.CHINA);
		
		return calendar.get(Calendar.MONTH)+1;

	}

	/**
	 * 获取希系统当前时间的年份
	 * */

	public static int getYear() {
		Calendar calendar = Calendar.getInstance(Locale.CHINA);

		return calendar.get(Calendar.YEAR);

	}

	/**
	 * 获取系统当前时间的月份的多少号
	 * */

	public static int getDayOfMonth() {
		Calendar calendar = Calendar.getInstance(Locale.CHINA);

		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 获取星期几
	 * */

	public static int getDayOfWeek() {
		Calendar calendar = Calendar.getInstance(Locale.CHINA);

		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * 根据年月取出年
	 * */

	public static int getYearByDate(String time) {
		String type = "yyyy-MM";
		SimpleDateFormat dateFormat = new SimpleDateFormat(type);
		try {
			Date date = dateFormat.parse(time);
			Calendar calendar = Calendar.getInstance(Locale.CHINA);
			calendar.setTime(date);
			return calendar.get(Calendar.YEAR);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return getYear();

	}
	/**
	 * 根据年月取出月份
	 * */

	public static int getMonthByDate(String time) {
		String type = "yyyy-MM";
		SimpleDateFormat dateFormat = new SimpleDateFormat(type);
		try {
			Date date = dateFormat.parse(time);
			Calendar calendar = Calendar.getInstance(Locale.CHINA);
			calendar.setTime(date);
			return calendar.get(Calendar.MONTH)+1;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return getYear();

	}
	
	
	/**
	 * 获取月和日
	 * 
	 * */
	
	public static String getMonthAndDay(String data,Context context)
	{
		String result="";
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Calendar calendar=Calendar.getInstance(Locale.CHINA);
			calendar.setTime(format.parse(data));
			result=(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		return result;
		
	}

	/**
	 * 获取语音识别返回结果中的毫秒
	 *
	 * */

	public static String getVoicdeTime(long time)
	{
		Date date=new Date(time);
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}

	/**
	 * waitsendgoods
	 * 代发货时间转换
	 * */

	public static String getWaitSendGoods(String time)
	{

		if(time.isEmpty())
			return "";

		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			Date date=format.parse(time);
			SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
			return  dateFormat.format(date);
		}catch (Exception e)
		{
			e.printStackTrace();
		}

	return  getVoicdeTime(System.currentTimeMillis());
	}

	/**
	 *根据String时间获取毫秒
	 * */

	public static long getLongTime(String time)
	{
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Calendar calendar=Calendar.getInstance(Locale.CHINA);
		try {
			calendar.setTime(format.parse(time));
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return calendar.getTimeInMillis();
	}


}
