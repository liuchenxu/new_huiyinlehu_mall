/**
 * 
 */
package com.huiyin.newpackage.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 物流信息实体类
 * 
 * @author liuchenxu
 * 
 */
public class LogisticsInfoBean implements Serializable {
	
	private ArrayList<LogisticsInfoDetail> DATA;
	
	private String EXPRESS_CODE;
	
	private String EXPRESS_COMPANY;
	
	private String EXPRESS_NO;

	public ArrayList<LogisticsInfoDetail> getDATA() {
		return DATA;
	}

	public void setDATA(ArrayList<LogisticsInfoDetail> dATA) {
		DATA = dATA;
	}

	public String getEXPRESS_CODE() {
		return EXPRESS_CODE;
	}

	public void setEXPRESS_CODE(String eXPRESS_CODE) {
		EXPRESS_CODE = eXPRESS_CODE;
	}

	public String getEXPRESS_COMPANY() {
		return EXPRESS_COMPANY;
	}

	public void setEXPRESS_COMPANY(String eXPRESS_COMPANY) {
		EXPRESS_COMPANY = eXPRESS_COMPANY;
	}

	public String getEXPRESS_NO() {
		return EXPRESS_NO;
	}

	public void setEXPRESS_NO(String eXPRESS_NO) {
		EXPRESS_NO = eXPRESS_NO;
	}
	
	
}
