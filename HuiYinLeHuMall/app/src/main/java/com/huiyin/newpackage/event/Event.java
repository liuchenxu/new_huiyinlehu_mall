
package com.huiyin.newpackage.event;


import de.greenrobot.event.EventBus;


/**
 * @ClassName: Event
 * @Description:(Network request error message)
 * @author Watermelon
 * @date 2015-9-8 下午2:52:51
 * 
 */
public class Event {
	public int type;
	public String msg;

	/**
	 * @Title: onFailure
	 * @Description:(Network request error assignment method)
	 * @param @param event
	 * @return void
	 * @throws
	 */
	public static void onFailure(Event event) {
		event.type = -1;
		event.msg = "网络错误";
		EventBus.getDefault().post(event);
	}
}
