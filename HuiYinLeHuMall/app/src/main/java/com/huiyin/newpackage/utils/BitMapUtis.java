package com.huiyin.newpackage.utils;

import java.io.ByteArrayOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;

/**
 * @author赵云霄
 * @time:2016-06-12
 * */

public class BitMapUtis {
	public static String Bitmap2StrByBase64(Bitmap bit, int quality) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bit.compress(Bitmap.CompressFormat.JPEG, quality, bos);// 参数100表示不压缩
		byte[] bytes = bos.toByteArray();
		return Base64.encodeToString(bytes, Base64.DEFAULT);
	}

	public static Bitmap getBitmapFromUri(Uri uri, Context mContext) {
		try {
			// 读取uri所在的图片
			Bitmap bitmap = MediaStore.Images.Media.getBitmap(
					mContext.getContentResolver(), uri);
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
