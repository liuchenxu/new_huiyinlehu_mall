package com.huiyin.newpackage.ui.activity;

import android.R.bool;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.huiyin.R;
import com.huiyin.newpackage.base.NewBaseActivity;
import com.huiyin.ui.user.order.ReturnRecordFragment;
import com.huiyin.ui.user.order.SearchOrderFragment;

/**
 * 刘晨旭 新的售后申请和退换货进度查询Activity
 */
public class BackGoodsAndProcessActivity extends NewBaseActivity {
	private FragmentTransaction transaction;
	private TextView applyServcie;// 售后申请
	private TextView serviceProcess;// 进度查询
	private TextView btnback;
	private TextView tvtitle;
	private SearchOrderFragment applyServcieFragment;// 售后申请Fragment
	private ReturnRecordFragment serviceProcessFragment;// 进度查询Fragment
	private View first_line;
	private View second_line;
	OnClickListener listener;
	private boolean ShowApplyServiceFragment=true;

	/** 当前显示的fragment **/
	private Fragment currentFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bespeak_return_new);
		transaction = getSupportFragmentManager().beginTransaction();
		findView();
	}

	@Override
	protected void onResume() {
		super.onResume();
		initView();
	}
	
	private void findView() {
		this.tvtitle = (TextView) findViewById(R.id.tv_title);
		this.btnback = (TextView) findViewById(R.id.btn_back);
		this.applyServcie = (TextView) findViewById(R.id.ask_for_service);
		this.serviceProcess = (TextView) findViewById(R.id.service_process);
		first_line=(View)findViewById(R.id.first_line);
		second_line=(View)findViewById(R.id.second_line);
		tvtitle.setText("售后申请");
	}

	/**
	 * 初始化控件
	 */
	private void initView() {
		applyServcieFragment = new SearchOrderFragment();
		serviceProcessFragment = new ReturnRecordFragment();
		if(ShowApplyServiceFragment){
			showFragment(applyServcieFragment);
			first_line.setVisibility(View.VISIBLE);
			second_line.setVisibility(View.INVISIBLE);
			setSelectedColor(applyServcie,true);
			setSelectedColor(serviceProcess,false);
		}
		else{
			showFragment(serviceProcessFragment);
			first_line.setVisibility(View.INVISIBLE);
			second_line.setVisibility(View.VISIBLE);
			setSelectedColor(applyServcie,false);
			setSelectedColor(serviceProcess,true);
		}
		listener=new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.ask_for_service:
					doSelectFragment(v);
					tvtitle.setText("售后申请");
					setSelectedColor(applyServcie,true);
					setSelectedColor(serviceProcess,false);
					ShowApplyServiceFragment=true;
					break;
				case R.id.service_process:
					doSelectFragment(v);
					tvtitle.setText("进度查询");
					setSelectedColor(applyServcie,false);
					setSelectedColor(serviceProcess,true);
					ShowApplyServiceFragment=false;
					break;
				case R.id.btn_back:
					BackGoodsAndProcessActivity.this.finish();
					break;
				default:
					break;
				}
			}
		};
		btnback.setOnClickListener(listener);
		applyServcie.setOnClickListener(listener);
		serviceProcess.setOnClickListener(listener);
	}

	/**
	 * 左上角返回按钮
	 */
//	@OnClick(R.id.back)
	void doFinish() {
		this.finish();
	}

	/**
	 * 点击选择申请售后或者进度查询按钮
	 * 
	 * @param view
	 */
	void doSelectFragment(View view) {
		switch (view.getId()) {
		// 申请售后
		case R.id.ask_for_service:
			showFragment(applyServcieFragment);
			first_line.setVisibility(View.VISIBLE);
			second_line.setVisibility(View.INVISIBLE);
			break;
		// 进度查询
		case R.id.service_process:
			showFragment(serviceProcessFragment);
			first_line.setVisibility(View.INVISIBLE);
			second_line.setVisibility(View.VISIBLE);
			break;
		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		doFinish();
	}

	public void showFragment(Fragment currentShow) {
		transaction = getSupportFragmentManager().beginTransaction();
		if (applyServcieFragment.isAdded() && !applyServcieFragment.isHidden()) {
			transaction.hide(applyServcieFragment);
		}

		if (serviceProcessFragment.isAdded() && !serviceProcessFragment.isHidden()) {
			transaction.hide(serviceProcessFragment);
		}

		if (!currentShow.isAdded()) {
			transaction.add(R.id.fragment_content, currentShow);
		}
		transaction.show(currentShow);
		transaction.commit();
		// 记住当前显示的fragment
		currentFragment = currentShow;
	}
	
	/**
	 * 设置文字的选中效果
	 * @param view
	 * @param selected
	 */
	private void setSelectedColor(TextView view,boolean selected){
		if(selected){
			view.setTextColor(Color.parseColor("#f42256"));	
		}
		else{
			view.setTextColor(Color.parseColor("#484848"));
		}
	}
}
