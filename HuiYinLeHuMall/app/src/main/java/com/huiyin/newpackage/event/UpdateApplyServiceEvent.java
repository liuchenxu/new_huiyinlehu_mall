package com.huiyin.newpackage.event;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.huiyin.api.URLs;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.android.http.AsyncHttpResponseHandler;
import com.huiyin.newpackage.android.http.RequestParams;

import de.greenrobot.event.EventBus;

/**
 * 修改退货申请Event
 * 
 * @author liuchenxu
 * 
 */
public class UpdateApplyServiceEvent extends Event {
	public static void UpdateOrderApply(AsyncHttpClient client,RequestParams params) {
		client.post(URLs.update_apply_service_order_info, params, new AsyncHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				String str = new String(responseBody);
				Gson gson = new Gson();
				UpdateApplyServiceEvent event = gson.fromJson(str, UpdateApplyServiceEvent.class);
				EventBus.getDefault().post(event);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				UpdateApplyServiceEvent event = new UpdateApplyServiceEvent();
				UpdateApplyServiceEvent.onFailure(event);
			}
			
		} );
	}
}
