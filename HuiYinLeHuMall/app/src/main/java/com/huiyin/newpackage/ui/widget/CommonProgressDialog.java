package com.huiyin.newpackage.ui.widget;

import com.huiyin.R;
import com.huiyin.newpackage.android.http.AsyncHttpClient;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;

/**
 * @ClassName: CommonProgressDialog
 * @Description:(通用进度条)
 * @author Watermelon
 * @date 2015-9-7 下午3:44:53
 * 
 */
public class CommonProgressDialog extends Dialog {
    AsyncHttpClient client;
    TextView tv_tip;

    /**
     * @Title:CommonProgressDialog
     * @Description:(构造方法)
     * @param context
     * @param cancelable
     * @param cancelListener
     */
    public CommonProgressDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    /**
     * @Title:CommonProgressDialog
     * @Description:(构造方法)
     * @param context
     * @param theme
     */
    public CommonProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    /**
     * @Title:CommonProgressDialog
     * @Description:(构造方法)
     * @param context
     * @param client
     */
    public CommonProgressDialog(final Context context, final AsyncHttpClient client) {
        super(context, R.style.MyProgressDialog);
        this.client = client;
        setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (client != null) {
                    client.cancelRequests(context, true);
                }
            }
        });
    }

    /**
     * @Title: onCreate
     * @Description:(系统方法)
     * @param savedInstanceState
     * @see Dialog#onCreate(Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_progress);
        tv_tip = (TextView) findViewById(R.id.tv_tip);
    }

    /**
     * @Title: setTip
     * @Description:(设置提示)
     * @param @param text
     * @return void
     * @throws
     */
    public void setTip(String text) {
        tv_tip.setText(text);
    }
}
