/**
 * 
 */
package com.huiyin.newpackage.event;

import java.io.IOException;
import java.net.URI;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

import com.google.gson.Gson;
import com.huiyin.api.URLs;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.android.http.AsyncHttpResponseHandler;
import com.huiyin.newpackage.android.http.RequestParams;
import com.huiyin.newpackage.entity.AppliedServiceInfoBean;
import com.huiyin.newpackage.entity.ApplyInfoBean;

import de.greenrobot.event.EventBus;

/**
 * 获得协议内容的接口
 * 
 * @author liuchenxu
 * 
 */
public class GetAggrementInfoEvent extends Event {
	private String content;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public static void GetAggrementInfo(AsyncHttpClient client, String flag) {
		RequestParams params = new RequestParams();
		params.put("flag", flag);
		client.post(URLs.aggrement_info, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				String str = new String(responseBody);
				Gson gson = new Gson();
				GetAggrementInfoEvent event = gson.fromJson(str, GetAggrementInfoEvent.class);
				EventBus.getDefault().post(event);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				GetAggrementInfoEvent event = new GetAggrementInfoEvent();
				GetAggrementInfoEvent.onFailure(event);
			}

		});

	}
}
