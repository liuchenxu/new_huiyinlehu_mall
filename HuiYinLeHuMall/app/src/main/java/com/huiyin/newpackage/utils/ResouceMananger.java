package com.huiyin.newpackage.utils;

import android.content.Context;
import android.content.res.Resources;

import com.huiyin.AppContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Administrator on 2016/4/5.
 */
public class ResouceMananger {

    public static String readFile(int resid) {
        Resources res = AppContext.getInstance().getResources();
        InputStream in = null;
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        try {
            in = res.openRawResource(resid);
            String str;
            br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
