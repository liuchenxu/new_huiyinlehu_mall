/**
 * 
 */
package com.huiyin.newpackage.utils;

import android.text.TextUtils;

/**
 * @author liuchenxu
 *  用来处理接口返回的html body
 *  内容使其在webview中显示的工具类
 */
public class HtmlUtils {
	public static String FormatHtmlContent(String html){
		String HTML_START="<html>";
		String HTML_END="</html>";
		String HTML_HEAD_START="<head>";
		String HTML_HEAD_MIDDLE="<meta charset=\"UTF-8\">";
		String HTML_HEAD_END="</head>";
		if(TextUtils.isEmpty(html)){
			return "";
		}
		else{
			return HTML_START+HTML_HEAD_START+HTML_HEAD_MIDDLE+HTML_HEAD_END+html+HTML_HEAD_END;
		}
	}
}
