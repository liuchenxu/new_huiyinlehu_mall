/**
 * 
 */
package com.huiyin.newpackage.event;

import java.util.ArrayList;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.huiyin.api.URLs;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.android.http.AsyncHttpResponseHandler;
import com.huiyin.newpackage.android.http.RequestParams;
import com.huiyin.newpackage.entity.ExpressInfoEntity;

import de.greenrobot.event.EventBus;

/**
 * @author liuchenxu 获取快递公司列表
 */
public class GetExpressCompanyListEvent extends Event {
	private ArrayList<ExpressInfoEntity> companyList;
	
	public ArrayList<ExpressInfoEntity> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(ArrayList<ExpressInfoEntity> companyList) {
		this.companyList = companyList;
	}

	public static void GetExpressCompanies(AsyncHttpClient client) {
		RequestParams params = new RequestParams();
		client.post(URLs.appLogisticsInfo, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				String str = new String(responseBody);
				Gson gson = new Gson();
				GetExpressCompanyListEvent event = gson.fromJson(str, GetExpressCompanyListEvent.class);
				EventBus.getDefault().post(event);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				GetExpressCompanyListEvent event = new GetExpressCompanyListEvent();
				UpdateApplyServiceEvent.onFailure(event);
			}

		});
	}
}
