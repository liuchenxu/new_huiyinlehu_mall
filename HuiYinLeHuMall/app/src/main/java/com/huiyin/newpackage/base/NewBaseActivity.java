package com.huiyin.newpackage.base;

import com.huiyin.R;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.ui.widget.CommonProgressDialog;
import com.huiyin.wight.Tip;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import de.greenrobot.event.EventBus;


/**
 * @author Watermelon
 * @ClassName: BaseActivity
 * @Description:(基类配置)
 * @date 2015-9-7 下午3:27:50
 */
public class NewBaseActivity extends FragmentActivity {
    protected static final int RequestCode = 0x101;
    public Context mContext;
    long flag = -1;// 退出开关
    public boolean register = false; // 是否注册接收的监听
    public InputMethodManager im;
    public DisplayMetrics dm; // 图片位置
    public static int screenX, ScreenY;
    public AsyncHttpClient client = new AsyncHttpClient();
    public int pageSize = 10;
    public Boolean isshowProg = false;
    protected Dialog loading, dialog;
    public CommonProgressDialog pd = null;

    /**
     * @param savedInstanceState
     * @Title: onCreate
     * @Description:(系统方法)
     * @see android.app.Activity#onCreate(Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        screenX = dm.widthPixels;
        ScreenY = dm.heightPixels;
        if (register) {
            EventBus.getDefault().register(this);
        }
        im = ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE));
        pd = new CommonProgressDialog(this, client);
    }

    /**
     * @Title: onResume
     * @Description:(系统方法)
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();

        if (!EventBus.getDefault().isRegistered(this) && register) {
            EventBus.getDefault().register(this);
        }

    }

    /**
     * @Title: onDestroy
     * @Description:(系统方法)
     * @see android.app.Activity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        hideSoftInput();
        super.onDestroy();
    }

    /**
     * @Title: onStop
     * @Description:(系统方法)
     * @see android.app.Activity#onStop()
     */
    @Override
    protected void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    // 判断当前是否使用的是 WIFI或者移动网络
    public boolean netActive(Context icontext, int type) {
        Context context = icontext.getApplicationContext();
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] info;
        if (connectivity != null) {
            info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getType() == type && info[i].isConnected()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param
     * @return void
     * @throws
     * @Title: hideSoftInput
     * @Description:(关闭输入法)
     */
    protected void hideSoftInput() {
        View view = getCurrentFocus();
        if (view != null) {
            IBinder binder = view.getWindowToken();
            if (binder != null) {
                im.hideSoftInputFromWindow(binder, 0);
            }
        }
    }

    /**
     * @param
     * @return void
     * @throws
     * @Title: showSoftInput
     * @Description:(弹出输入法)
     */
    public void showSoftInput() {
        im.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * @param @param message
     * @return void
     * @throws
     * @Title: showToast
     * @Description:(弹出Toast)
     */
    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * @param @param message
     * @return void
     * @throws
     * @Title: showToast
     * @Description:(弹出Toast)
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * @param @param text
     * @return void
     * @throws
     * @Title: showProgress
     * @Description:(显示异步上传时的等待进度框)
     */
    public void showProgress(Boolean isfinish) {
//        pd.show();
//        pd.setTip("请稍后...");
        Tip.showLoadDialog(mContext,
                mContext.getString(R.string.loading));
    }

    /**
     * @param
     * @return void
     * @throws
     * @Title: finishProgress
     * @Description:(结束进度框)
     */
    public void finishProgress() {
//        if (pd != null) {
//            pd.dismiss();
//        }
    	Tip.colesLoadDialog();
    }

    // @Override
    // public boolean onKeyDown(int keyCode, KeyEvent event) {
    // if (keyCode == KeyEvent.KEYCODE_BACK) {
    // if (isshowProg) {
    // return isshowProg;
    // } else {
    // finish();
    // }
    // return isshowProg;
    // }
    // return super.onKeyDown(keyCode, event);
    // }

    /**
     * 刷新后更新
     *
     * @param listview
     */
    public void refreshComplete(final PullToRefreshBase<ListView> listview) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (listview != null) {
                    listview.onRefreshComplete();
                }
            }
        }, 100);
    }

    public void finish(View view) {
        hideSoftInput();
        finish();
    }

    // 获取整个屏幕的宽度(px)
    protected int getDispalWidth() {
        if (dm != null)
            return dm.widthPixels;

        return 0;
    }

    // 获取整个屏幕的高度(px)
    protected int getDispalHeight() {

        if (dm != null)
            return dm.heightPixels;

        return 0;

    }

    // 弹窗显示Dialog
    protected void showDialogView(String msg, boolean outsideCancel) {
        disShowDialogView();
        // , R.style.Theme_Light_FullScreenDialogAct
        if (loading == null)
            loading = new Dialog(this, R.style.Theme_Light_FullScreenDialogAct);
        View view = getLayoutInflater().inflate(R.layout.layout_progress_dialog_view, null);
        TextView textview = (TextView) view.findViewById(R.id.dialogtext);
        textview.setText(msg);

        loading.addContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        Window manager = loading.getWindow();
        loading.setCanceledOnTouchOutside(outsideCancel);
        manager.setGravity(Gravity.CENTER);
        loading.show();
    }

    // 消失

    protected void disShowDialogView() {
        if (loading != null) {
            loading.dismiss();
        }
    }

    // 显示dialog的view
    protected void showMyDialog(View view, boolean outsideCancel) {
        disMyDialog();
        if (dialog == null) {
            dialog = new Dialog(this, R.style.dialog2);
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.width = getDispalWidth() / 4 * 3;
        dialog.addContentView(view, params);
        dialog.setCanceledOnTouchOutside(outsideCancel);
        dialog.show();

    }

    protected void disMyDialog() {
        if (dialog != null)
            if (dialog.isShowing())
                dialog.dismiss();

    }

    /**
     * @author赵云霄
     * @time:2016-06-07
     * @function:链接超时或者请求出错给出提示
     */

    protected void getError() {
        showToast("链接超时...");

    }

    /**
     * @author赵云霄
     * @time:2016-06-07
     * @function:无网络给出提示
     */
    protected void getUnConnect() {
        showToast("网络错误...");

    }

    /**
     * @author 赵云霄
     * @time:2016-06-15
     * @function:隐藏键盘输入
     */

    protected void hiddlenSoftKeyBroad(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0); //强制隐藏键盘

    }

}
