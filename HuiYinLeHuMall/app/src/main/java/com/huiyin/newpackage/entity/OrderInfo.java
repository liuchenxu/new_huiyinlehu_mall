package com.huiyin.newpackage.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 订单信息实体类
 * @author Administrator
 *
 */
public class OrderInfo implements Serializable {
	private String CREATE_DATE;

	private long ID;

	private int NUM;

	private String ORDER_CODE;

	private int STATUS;

	private String STATUS_NAME;

	private long STORE_ID;

	private String STORE_LOGO;

	private String STORE_NAME;

	private double TOTAL_PRICE;

	private String TRADE_CODE;

	private String USER_ID;

	private String USER_NAME;

	private String USER_PHONE;
	
	private String APPLY_REASON;
	
	private String APPLY_REMARK;
	
	private String APPLY_IMGS;

	private ArrayList<GoodsDetail> orderDetalList ;

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public int getNUM() {
		return NUM;
	}

	public void setNUM(int nUM) {
		NUM = nUM;
	}

	public String getORDER_CODE() {
		return ORDER_CODE;
	}

	public void setORDER_CODE(String oRDER_CODE) {
		ORDER_CODE = oRDER_CODE;
	}

	public int getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(int sTATUS) {
		STATUS = sTATUS;
	}

	public String getSTATUS_NAME() {
		return STATUS_NAME;
	}

	public void setSTATUS_NAME(String sTATUS_NAME) {
		STATUS_NAME = sTATUS_NAME;
	}

	public long getSTORE_ID() {
		return STORE_ID;
	}

	public void setSTORE_ID(long sTORE_ID) {
		STORE_ID = sTORE_ID;
	}

	public String getSTORE_LOGO() {
		return STORE_LOGO;
	}

	public void setSTORE_LOGO(String sTORE_LOGO) {
		STORE_LOGO = sTORE_LOGO;
	}

	public String getSTORE_NAME() {
		return STORE_NAME;
	}

	public void setSTORE_NAME(String sTORE_NAME) {
		STORE_NAME = sTORE_NAME;
	}

	public double getTOTAL_PRICE() {
		return TOTAL_PRICE;
	}

	public void setTOTAL_PRICE(double tOTAL_PRICE) {
		TOTAL_PRICE = tOTAL_PRICE;
	}

	public String getTRADE_CODE() {
		return TRADE_CODE;
	}

	public void setTRADE_CODE(String tRADE_CODE) {
		TRADE_CODE = tRADE_CODE;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getUSER_NAME() {
		return USER_NAME;
	}

	public void setUSER_NAME(String uSER_NAME) {
		USER_NAME = uSER_NAME;
	}

	public String getUSER_PHONE() {
		return USER_PHONE;
	}

	public void setUSER_PHONE(String uSER_PHONE) {
		USER_PHONE = uSER_PHONE;
	}

	public ArrayList<GoodsDetail> getOrderDetalList() {
		return orderDetalList;
	}

	public void setOrderDetalList(ArrayList<GoodsDetail> orderDetalList) {
		this.orderDetalList = orderDetalList;
	}

	public String getAPPLY_REASON() {
		return APPLY_REASON;
	}

	public void setAPPLY_REASON(String aPPLY_REASON) {
		APPLY_REASON = aPPLY_REASON;
	}

	public String getAPPLY_REMARK() {
		return APPLY_REMARK;
	}

	public void setAPPLY_REMARK(String aPPLY_REMARK) {
		APPLY_REMARK = aPPLY_REMARK;
	}

	public String getAPPLY_IMGS() {
		return APPLY_IMGS;
	}

	public void setAPPLY_IMGS(String aPPLY_IMGS) {
		APPLY_IMGS = aPPLY_IMGS;
	}
	
	
	
}
