package com.huiyin.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.api.URLs;
import com.huiyin.bean.GalleryAd;
import com.huiyin.ui.classic.ProductsDetailActivity;
import com.huiyin.ui.club.ClubActivity;
import com.huiyin.ui.home.HuodongWebPageActivity;
import com.huiyin.ui.home.LotteryActivity;
import com.huiyin.ui.home.NewsTodayActivity;
import com.huiyin.ui.home.PhonePayActivity;
import com.huiyin.ui.home.TryApplyActivity;
import com.huiyin.ui.home.VoteActivity;
import com.huiyin.ui.home.WaterCoalElectricActivity;
import com.huiyin.ui.home.Logistics.LogisticsQueryActivity;
import com.huiyin.ui.home.prefecture.ZhuanQuActivity;
import com.huiyin.ui.housekeeper.HouseKeeperActivity;
import com.huiyin.ui.lehuvoucher.LehuVoucherActivity;
import com.huiyin.ui.servicecard.BindServiceCard;
import com.huiyin.ui.servicecard.ServiceCardActivity;
import com.huiyin.ui.shark.ShakeActivity;
import com.huiyin.ui.user.KefuCenterActivity;
import com.huiyin.ui.user.LoginActivity;
import com.huiyin.ui.user.order.ApplyBespeakActivity;
import com.huiyin.ui.videoplayer.VideoPlayerActivity;
import com.huiyin.utils.DensityUtil;
import com.huiyin.utils.ImageManager;
import com.huiyin.utils.StringUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class GalleryViewflowAdapter extends BaseAdapter {

	private DisplayImageOptions options;
	private ImageLoader imageLoader;
	private Context mContext;

	private List<GalleryAd> listDatas;

	public GalleryViewflowAdapter(Context context, List<GalleryAd> listDatas) {
		mContext = context;
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true)
				.showStubImage(R.drawable.image_default_gallery)
				.showImageForEmptyUri(R.drawable.image_default_gallery)
				.showImageOnFail(R.drawable.image_default_gallery)
				.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new RoundedBitmapDisplayer(0)).build();

		imageLoader = ImageLoader.getInstance();
		this.listDatas = listDatas;
	}

	@Override
	public int getCount() {
		return Integer.MAX_VALUE;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			//布局
			FrameLayout layout = new FrameLayout(mContext);
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			params.gravity = Gravity.CENTER;
			layout.setLayoutParams(params);
			
			//banner图片
			ImageView imageView = new ImageView(mContext);
			imageView.setScaleType(ScaleType.FIT_XY);
			imageView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			layout.addView(imageView);
			
			//视频播放按钮
			ImageView playImg = new ImageView(mContext);
			LayoutParams params2 = new LayoutParams(DensityUtil.dip2px(mContext, 80), DensityUtil.dip2px(mContext, 80));
			params2.gravity = Gravity.CENTER;
			playImg.setLayoutParams(params2);
			playImg.setImageResource(R.drawable.ionic_player);
			playImg.setVisibility(View.GONE);
			
			layout.addView(playImg);
			convertView = layout;
			
			layout.invalidate();
			
			viewHolder.frameLayout = (FrameLayout) convertView;
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		GalleryAd bean = listDatas.get(position % listDatas.size());
        //统一图片加载方式用于省流量处理
        ImageManager.LoadWithServer(bean.getImageUrl(), (ImageView)viewHolder.frameLayout.getChildAt(0), options);
        
        if(bean.getFlag() == 6) {
        	viewHolder.frameLayout.getChildAt(1).setVisibility(View.VISIBLE);
        }
        else {
        	viewHolder.frameLayout.getChildAt(1).setVisibility(View.GONE);
        }

		viewHolder.frameLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				GalleryAd bean = listDatas.get(position % listDatas.size());
				if (bean.getFlag() == 2) {
					Intent intent = new Intent(mContext,
                            ProductsDetailActivity.class);
					intent.putExtra(ProductsDetailActivity.BUNDLE_KEY_GOODS_ID,
							bean.getRowId() + "");
					mContext.startActivity(intent);
				} else if (bean.getFlag() == 3) {
					Intent intent = new Intent(mContext, ZhuanQuActivity.class);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_ID,
							bean.getRowId() + "");
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_TYPE, 3);
					mContext.startActivity(intent);
				} else if (bean.getFlag() == 4) {

					switch (bean.getRowId()) {
					case 1:
						// 今日头条
						mContext.startActivity(new Intent(mContext,
								NewsTodayActivity.class));
						break;
					case 2:
						// 乐虎彩票
						mContext.startActivity(new Intent(mContext,
								LotteryActivity.class));
						break;
					case 3:
						if (StringUtils.isBlank(AppContext.getInstance().userId)) {
							Toast.makeText(mContext, "请先登录", Toast.LENGTH_SHORT)
									.show();
							mContext.startActivity(new Intent(mContext,
									LoginActivity.class));
						} else {
							// 预约服务
							mContext.startActivity(new Intent(mContext,
									ApplyBespeakActivity.class));
						}
						break;
					case 4:
						// 物流查询
						if (StringUtils.isBlank(AppContext.getInstance().userId)) {
							Toast.makeText(mContext, "请先登录", Toast.LENGTH_SHORT)
									.show();
							mContext.startActivity(new Intent(mContext,
									LoginActivity.class));
						} else {
							mContext.startActivity(new Intent(mContext,
									LogisticsQueryActivity.class));
						}
						break;
					case 5:
						// 智慧管家
//						AppContext.MAIN_TASK = AppContext.HOUSEKEEPER;
//						Intent i = new Intent();
//						i.setClass(mContext, MainActivity.class);
//						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//						mContext.startActivity(i);
						Intent i = new Intent();
						i.setClass(mContext, HouseKeeperActivity.class);
						mContext.startActivity(i);
						break;
					/*case 6:
						// 秀场
						mContext.startActivity(new Intent(mContext,
								TheShowActivity.class));
						break;*/
					case 7:
						// 积分club
						Intent intent = new Intent(mContext, ClubActivity.class);
						intent.putExtra(ClubActivity.INTENT_KEY_TITLE, "积分club");
						mContext.startActivity(intent);
						break;
					case 8:
						// 客户中心
						mContext.startActivity(new Intent(mContext,
								KefuCenterActivity.class));
						break;
//					case 9:
//						mContext.startActivity(new Intent(mContext,
//								NearTheShopActivityNew.class));
//						break;
					case 10:
						// 服务卡
						if (StringUtils.isBlank(AppContext.getInstance().userId)) {
							Toast.makeText(mContext, "请先登录", Toast.LENGTH_SHORT)
									.show();
							mContext.startActivity(new Intent(mContext,
									LoginActivity.class));
						} else {
							Intent fuwu_intent = new Intent();
							if (AppContext.getInstance().mUserInfo.getBDStatus() == 1) {
								fuwu_intent.setClass(mContext,
										ServiceCardActivity.class);
							} else {
								fuwu_intent.setClass(mContext,
										BindServiceCard.class);
							}
							mContext.startActivity(fuwu_intent);
						}
						break;
					case 11://话费
						
						intent = new Intent(mContext, PhonePayActivity.class);
						mContext.startActivity(intent);
						
						break;
			        case 12: //水费
			        case 13: //燃气
			        case 14: //电费
			        	intent = new Intent(new Intent(mContext, WaterCoalElectricActivity.class));
			        	intent.putExtra(WaterCoalElectricActivity.ID,bean.getRowId());
			        	mContext.startActivity(intent);
			        	break;
					case 15:
						// 红包申领
						if (StringUtils.isBlank(AppContext.getInstance().userId)) {
							Toast.makeText(mContext, "请先登录", Toast.LENGTH_SHORT).show();
							intent = new Intent(new Intent(mContext, LoginActivity.class));
							mContext.startActivity(intent);
						} else {
							intent = new Intent(new Intent(mContext, LehuVoucherActivity.class));
							mContext.startActivity(intent);
						}
						break;
					case 16:
						// 文体俱乐部
						intent = new Intent(new Intent(mContext, LotteryActivity.class));
						intent.putExtra("type",1);
						mContext.startActivity(intent);
						break;
					// add by zhyao @2015/5/8  添加摇一摇入口
					case 17:
						// 摇一摇
						intent = new Intent(new Intent(mContext, ShakeActivity.class));
						intent.putExtra(ClubActivity.INTENT_KEY_TITLE, "摇一摇");
						mContext.startActivity(intent);
						break;
						// add by zhyao @2015/8/31 添加免费试用快捷方式
					case 22:
						//免费试用
						if (StringUtils.isBlank(AppContext.getInstance().userId)) {
							Toast.makeText(mContext, "请先登录", Toast.LENGTH_SHORT).show();
							intent = new Intent(new Intent(mContext, LoginActivity.class));
							mContext.startActivity(intent);
						}
						else {
							intent = new Intent(mContext, TryApplyActivity.class);
							intent.putExtra(TryApplyActivity.TRY_TYPE, TryApplyActivity.TRY_ACTITY);
							mContext.startActivity(intent);
						}
						
						break;
					case 23:
						//投票活动
						if (StringUtils.isBlank(AppContext.getInstance().userId)) {
							Toast.makeText(mContext, "请先登录", Toast.LENGTH_SHORT).show();
							intent = new Intent(new Intent(mContext, LoginActivity.class));
							mContext.startActivity(intent);
						}
						else {
							intent = new Intent(mContext, VoteActivity.class);
							mContext.startActivity(intent);
						}
						break;
					default:
						break;
					}

				} else if (bean.getFlag() == 1 || bean.getFlag() == 5) {
					Intent intent = new Intent(mContext,
							HuodongWebPageActivity.class);
					intent.putExtra("flag", bean.getHuodongId() + "");
					mContext.startActivity(intent);
				}
				//视频播放
				else if(bean.getFlag() == 6) {
					Intent intent = new Intent(mContext,
							VideoPlayerActivity.class);
					intent.putExtra(VideoPlayerActivity.VIDEO_SRC, URLs.IMAGE_URL + bean.getVideoUrl());
					mContext.startActivity(intent);
				}
				// else if (bean.getFlag() == 2) {
				// Intent intent = new Intent(mContext,
				// CategorySearchActivity.class);
				// intent.putExtra(CategorySearchActivity.BUNDLE_KEY_CATEGORY_ID,
				// bean.getRowId() + "");
				// mContext.startActivity(intent);
				// }
			}
		});

		return convertView;
	}

	private class ViewHolder {
		//ImageView imageView;
		FrameLayout frameLayout;
	}

}