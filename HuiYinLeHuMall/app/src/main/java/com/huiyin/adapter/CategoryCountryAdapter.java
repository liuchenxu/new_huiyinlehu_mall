package com.huiyin.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.huiyin.R;
import com.huiyin.bean.CategoryAndBrandBean.NationalPavilion;
import com.huiyin.ui.web.GoodsListWebActivity;
import com.huiyin.utils.DensityUtil;
import com.huiyin.utils.ImageManager;
/**
 * 分类-国家馆适配器
 * @author zhyao
 *
 */
public class CategoryCountryAdapter extends BaseAdapter{
	
	private Context mContext;
	
	private ArrayList<NationalPavilion> mCountrys;
	
	public CategoryCountryAdapter(Context context, ArrayList<NationalPavilion> countrys) {
		mContext = context;
		
		mCountrys = countrys;
		
	}

	@Override
	public int getCount() {
		return mCountrys != null ? mCountrys.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return mCountrys != null ? mCountrys.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			holder.mCountryImg = new ImageView(mContext);
			int width = DensityUtil.getScreenWidth((Activity)mContext);
			int height = (int)(width * 0.4);
			LayoutParams params = new LayoutParams(width, height);
			holder.mCountryImg.setLayoutParams(params);
			holder.mCountryImg.setScaleType(ImageView.ScaleType.FIT_XY);
			
			convertView = holder.mCountryImg;
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		final NationalPavilion nationalPavilion = mCountrys.get(position);
		
		ImageManager.Load(nationalPavilion.getIMG(), holder.mCountryImg);
		
		holder.mCountryImg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				NationalPavilion bean = mCountrys.get(position);
				Intent intent = new Intent(mContext, GoodsListWebActivity.class);
                intent.putExtra(GoodsListWebActivity.BUNDLE_MARK, "8");
				intent.putExtra(GoodsListWebActivity.BUNDLE_ORGIN_ID, bean.getORIGIN_IDS());
				mContext.startActivity(intent);
			}
		});
	
		return convertView;
	}

	static class ViewHolder {
		ImageView mCountryImg;
	}
	
}
