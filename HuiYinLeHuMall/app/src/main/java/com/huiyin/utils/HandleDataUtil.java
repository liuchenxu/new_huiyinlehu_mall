package com.huiyin.utils;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

import com.grong.hyin.sdk.entity.AccountKeeper;
import com.huiyin.AppContext;
import com.huiyin.UIHelper;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.api.URLs;
import com.huiyin.bean.BannerBean;
import com.huiyin.constants.Constants;
import com.huiyin.ui.classic.ProductsDetailActivity;
import com.huiyin.ui.club.ClubActivity;
import com.huiyin.ui.home.HuodongWebPageActivity;
import com.huiyin.ui.home.LotteryActivity;
import com.huiyin.ui.home.NewsTodayActivity;
import com.huiyin.ui.home.PhonePayActivity;
import com.huiyin.ui.home.TryApplyActivity;
import com.huiyin.ui.home.VoteActivity;
import com.huiyin.ui.home.WaterCoalElectricActivity;
import com.huiyin.ui.home.prefecture.ZhuanQuActivity;
import com.huiyin.ui.housekeeper.HouseKeeperActivity;
import com.huiyin.ui.lehuvoucher.LehuVoucherActivity;
import com.huiyin.ui.servicecard.BindServiceCard;
import com.huiyin.ui.servicecard.ServiceCardActivity;
import com.huiyin.ui.shark.ShakeActivity;
import com.huiyin.ui.user.KefuCenterActivity;
import com.huiyin.ui.user.LoginActivity;
import com.huiyin.ui.user.order.AllOrderActivity;
import com.huiyin.ui.user.order.ApplyBespeakActivity;
import com.huiyin.ui.videoplayer.VideoPlayerActivity;

public class HandleDataUtil {

	public static int getRespStatus(String url) {
		int status = -1;
		try {
			HttpHead head = new HttpHead(url);
			HttpClient client = new DefaultHttpClient();
			HttpResponse resp = client.execute(head);
			status = resp.getStatusLine().getStatusCode();
		} catch (IOException e) {
		}
		return status;
	}

	/**
	 * 处理首页Banner事件
	 * 
	 * @param context
	 * @param bannerBean
	 */
	public static void handleBannerEvent(Context context, BannerBean bannerBean) {
		Intent intent;
		// banner跳转标志 1活动介绍,2商品详细页,3专区,4快捷服务,5活动H5网页,6视频
		switch (bannerBean.BANNER_JUMP_FLAG) {
		// 活动介绍
		case 1:
			intent = new Intent(context, HuodongWebPageActivity.class);
			intent.putExtra("flag", String.valueOf(bannerBean.ID));
			context.startActivity(intent);
			break;
		// 商品详细页
		case 2:
			intent = new Intent(context, ProductsDetailActivity.class);
			intent.putExtra(ProductsDetailActivity.BUNDLE_KEY_GOODS_ID, String.valueOf(bannerBean.BANNER_JUMP_ID));
			context.startActivity(intent);
			break;
		// 专区
		case 3:
			intent = new Intent(context, ZhuanQuActivity.class);
			intent.putExtra(ZhuanQuActivity.INTENT_KEY_ID, String.valueOf(bannerBean.BANNER_JUMP_ID));
			intent.putExtra(ZhuanQuActivity.INTENT_KEY_TYPE, 3);
			context.startActivity(intent);
			break;
		// 快捷服务
		case 4:
			HandleDataUtil.handShortcut(context, bannerBean.BANNER_JUMP_ID);
			break;
		// 活动H5网页
		case 5:
			intent = new Intent(context, HuodongWebPageActivity.class);
			intent.putExtra("flag", String.valueOf(bannerBean.ID));
			context.startActivity(intent);
			break;
		// 视频
		case 6:
			intent = new Intent(context, VideoPlayerActivity.class);
			intent.putExtra(VideoPlayerActivity.VIDEO_SRC, URLs.IMAGE_URL + bannerBean.BANNER_CONTENT);
			context.startActivity(intent);
			break;

		default:
			break;
		}

	}

	/**
	 * 处理首页快捷方式
	 * 
	 * @param context
	 * @param channelId
	 */
	public static void handShortcut(final Context context, int channelId) {

		Intent intent;
		switch (channelId) {
		case -1:
			 // 播播直播
			 new Handler().post(new Runnable() {
					
					@Override
					public void run() {

						if (AppContext.mUserInfo != null && AppContext.mUserInfo.userId != null) {
							RequstClient.saveUserInfoForGongRong(AppContext.mUserInfo.phone, AppContext.mUserInfo.userName, AppContext.mUserInfo.userName, AppContext.mUserInfo.img, new CustomResponseHandler(context) {
								@Override
								public void onSuccess(String content) {
									super.onSuccess(content);
									if(JSONParseUtils.getInt(content, "statusCode") == 200) {
										AccountKeeper aKeeper = new AccountKeeper(context);
										aKeeper.setAccount(AppContext.mUserInfo.phone, AppContext.mUserInfo.userName, URLs.IMAGE_URL + AppContext.mUserInfo.img);
									
										Intent intent=new Intent(context, com.grong.hyin.sdk.MainActivity.class);
										context.startActivity(intent);
									}
								}
							});
						}
						else {
							Intent intent=new Intent(context, com.grong.hyin.sdk.MainActivity.class);
							context.startActivity(intent);
						}
					}
				});
				break;
		case 1:
			// 今日头条
			intent = new Intent(new Intent(context, NewsTodayActivity.class));
			context.startActivity(intent);
			break;
		case 2:
			// 乐虎彩票
			intent = new Intent(new Intent(context, LotteryActivity.class));
			context.startActivity(intent);
			break;
		case 3:
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(context, "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(context, LoginActivity.class));
				context.startActivity(intent);
			} else {
				// 预约服务
				intent = new Intent(new Intent(context, ApplyBespeakActivity.class));
				context.startActivity(intent);
			}
			break;
		case 4:
			// 物流查询
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(context, "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(context, LoginActivity.class));
				context.startActivity(intent);
			} else {
				// 跳转到待收货订单
				Intent wr_i = new Intent();
				wr_i.setClass(context, AllOrderActivity.class);
				wr_i.putExtra(AllOrderActivity.ORDER_TYPE, 3);
				context.startActivity(wr_i);
			}
			break;
		case 5:
			// 智慧管家
			intent = new Intent();
			intent.setClass(context, HouseKeeperActivity.class);
			context.startActivity(intent);
			break;
			//以前是秀场，1.5.8版本
		 case 6:
			 //红包专区
			intent = new Intent(context, ZhuanQuActivity.class);
			intent.putExtra(ZhuanQuActivity.INTENT_KEY_TYPE, Constants.ZhuangQu_Home_Promote);
			intent.putExtra(ZhuanQuActivity.INTENT_KEY_ID, "642");//由于快捷方式中缺少专区ID参数，暂时传固定642
			intent.putExtra(ZhuanQuActivity.INTENT_KEY_FLAG, 1);
			context.startActivity(intent);
		 break;
		case 7:
			// 积分club 天天有奖
			intent = new Intent(new Intent(context, ClubActivity.class));
			intent.putExtra(ClubActivity.INTENT_KEY_TITLE, "积分club");
			context.startActivity(intent);
			break;
		case 8:
			// 客户中心
			intent = new Intent(new Intent(context, KefuCenterActivity.class));
			context.startActivity(intent);
			break;
		// case 9:
		// //身边汇银
		// intent = new Intent(new Intent(context,
		// NearTheShopActivityNew.class));
		// context.startActivity(intent);
		// break;
		case 10:
			// 服务卡
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(context, "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(context, LoginActivity.class));
				context.startActivity(intent);
			} else {
				intent = new Intent(new Intent(context, LotteryActivity.class));
				if (AppContext.getInstance().mUserInfo.getBDStatus() == 1) {
					intent.setClass(context, ServiceCardActivity.class);
				} else {
					intent.setClass(context, BindServiceCard.class);
				}
				context.startActivity(intent);
			}
			break;
		case 11:// 话费

			intent = new Intent(context, PhonePayActivity.class);
			context.startActivity(intent);

			break;
		case 12: // 水费
		case 13: // 燃气
		case 14: // 电费
			intent = new Intent(new Intent(context, WaterCoalElectricActivity.class));
			intent.putExtra(WaterCoalElectricActivity.ID, channelId);
			context.startActivity(intent);
			break;
		case 15:
			// 红包申领
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(context, "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(context, LoginActivity.class));
				context.startActivity(intent);
			} else {
				intent = new Intent(new Intent(context, LehuVoucherActivity.class));
				context.startActivity(intent);
			}
			break;
		case 16:
			// 文体俱乐部
			intent = new Intent(new Intent(context, LotteryActivity.class));
			intent.putExtra("type", 1);
			context.startActivity(intent);
			break;
		// add by zhyao @2015/5/8 添加摇一摇入口
		case 17:
			// 摇一摇
			intent = new Intent(new Intent(context, ShakeActivity.class));
			intent.putExtra(ClubActivity.INTENT_KEY_TITLE, "摇一摇");
			context.startActivity(intent);
			break;
		// add by zhyao @2015/8/31 添加免费试用快捷方式
		case 22:
			// 免费试用
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(context, "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(context, LoginActivity.class));
				context.startActivity(intent);
			} else {
				intent = new Intent(context, TryApplyActivity.class);
				intent.putExtra(TryApplyActivity.TRY_TYPE, TryApplyActivity.TRY_ACTITY);
				context.startActivity(intent);
			}

			break;
		case 23:
			// 投票活动
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(context, "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(context, LoginActivity.class));
				context.startActivity(intent);
			} else {
				intent = new Intent(context, VoteActivity.class);
				context.startActivity(intent);
			}
			break;
		case 25:
			// 每日签到
			if (StringUtils.isBlank(AppContext.userId)) {
				Toast.makeText(context, "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(context, LoginActivity.class));
				context.startActivity(intent);
			} else {
				solveComplain(context);
			}
			break;
		default:
			
			break;
		}

	}

	/**
	 * 签到
	 */
	public static void solveComplain(Context context) {

		// 未签到
		if (0 == AppContext.mUserInfo.getFlagSign()) {
			RequstClient.sign(new CustomResponseHandler(context, true) {
				@Override
				public void onSuccess(int statusCode, Header[] headers, String content) {
					super.onSuccess(statusCode, headers, content);

					// 异常消息显示
					if (JSONParseUtils.isErrorJSONResult(content)) {
						String msg = JSONParseUtils.getString(content, "msg");
						UIHelper.showToast(msg);
						return;
					}

					// 签到时间,最新积分
					String curTime = JSONParseUtils.getString(content, "curTime");
					int integral = JSONParseUtils.getInt(content, "INTEGRAL");

					try {
						String newTime = DateUtil.convertBirthday(curTime);
						curTime = newTime;
					} catch (Exception e) {
						e.printStackTrace();
					}

					// 显示已签到
					UIHelper.showToast("签到成功！");
					AppContext.mUserInfo.FLAG_SIGN = "1";
					AppContext.mUserInfo.addSignDay(1);
					AppContext.mUserInfo.SIGNDAY_TIME = curTime;

					// 刷新积分
					if (integral > 0) {
						AppContext.mUserInfo.integral = integral + "";
					}
				}

			});
		} else {
			UIHelper.showToast("您今天已签到，请不要重复签到呦！请明天再来吧！");
		}
	}

}
