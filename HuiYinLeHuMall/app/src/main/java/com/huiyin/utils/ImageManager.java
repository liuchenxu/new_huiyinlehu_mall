package com.huiyin.utils;

import java.io.File;
import java.util.List;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.api.URLs;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

/**
 * 图片获取统一管理类
 * <p><b>内部封装ImageLoader实现</b></p>
 */
public class ImageManager {
	
	/**
	 * 圆角头像
	 */
	public static DisplayImageOptions headOptions = new DisplayImageOptions.Builder()
			.cacheInMemory(true)
			.cacheOnDisk(true)
			.showImageForEmptyUri(R.drawable.default_head)
			.showImageOnFail(R.drawable.default_head)
			.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.displayer(new RoundedBitmapDisplayer(200))
			.build();
	
	/**
	 * 矩形默认背景
	 */
	public static DisplayImageOptions galleryOptions = new DisplayImageOptions.Builder()
			.cacheInMemory(true)
			.cacheOnDisk(true)
			.showImageForEmptyUri(R.drawable.image_default_gallery)
			.showImageOnFail(R.drawable.image_default_gallery)
			.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.build();
	
	/**
	 * 矩形默认背景
	 */
	public static DisplayImageOptions rectangleOptions = new DisplayImageOptions.Builder()
			.cacheInMemory(true)
			.cacheOnDisk(true)
			.showImageForEmptyUri(R.drawable.image_default_rectangle)
			.showImageOnFail(R.drawable.image_default_rectangle)
			.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.build();
	
	
	/**
	 * 正方形默认背景
	 */
	public static DisplayImageOptions squareOptions = new DisplayImageOptions.Builder()
			.cacheInMemory(true)
			.cacheOnDisk(true)
			.showImageForEmptyUri(R.drawable.image_default_square)
			.showImageOnFail(R.drawable.image_default_square)
			.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.build();

	public static void Load(String imgUrl, ImageView imageView) {
		LoadWithServer(imgUrl, "", imageView);
	}

	public static void Load(String imgUrl, ImageView imageView, DisplayImageOptions o) {
		LoadWithServer(imgUrl, "", imageView, o);
	}

	
	/**
     * 从服务器获取图片并显示
     * <p><b>统一获取方式，用来处理省流量的模式</b></p>
     * @param imgUrl 图片地址
     * @param imageView 展示的View
     */
	public static void LoadWithServer(String imgUrl, ImageView imageView) {
		LoadWithServer(imgUrl, "", imageView, null);
	}
	
    /**
     * 从服务器获取图片并显示
     * <p><b>统一获取方式，用来处理省流量的模式</b></p>
     * @param imgUrl 图片地址
     * @param imageView 展示的View
     */
	public static void LoadWithServer(String imgUrl, String picSize, ImageView imageView) {
		LoadWithServer(imgUrl, picSize, imageView, null);
	}
    
	
	/**
     * 从服务器获取图片并显示
     * <p><b>统一获取方式，用来处理省流量的模式</b></p>
     * @param imgUrl 图片地址
     * @param imageView 展示的View
     * @param o DisplayImageOptions
     */
	public static void LoadWithServer(String imgUrl, ImageView imageView, DisplayImageOptions o) {
		LoadWithServer(imgUrl, "", imageView, o, null);
	}
	
	/**
     * 从服务器获取图片并显示
     * <p><b>统一获取方式，用来处理省流量的模式</b></p>
     * @param imgUrl 图片地址
     * @param imageView 展示的View
     * @param o DisplayImageOptions
     */
	public static void LoadWithServer(String imgUrl, String picSize, ImageView imageView, DisplayImageOptions o) {
		LoadWithServer(imgUrl, picSize, imageView, o, null);
	}
	
    /**
     * 从服务器获取图片并显示
     * <p><b>统一获取方式，用来处理省流量的模式</b></p>
     * @param imgUrl 图片地址
     * @param imageView 展示的View
     * @param o DisplayImageOptions
     * @param loadingListener ImageLoadingListener  加载监听
     */
	public static void LoadWithServer(String imgUrl, String picSize, final ImageView imageView, final DisplayImageOptions o, final ImageLoadingListener loadingListener) {
		
		try{
			final String mWrapImagUrl = wrapImagUrl(imgUrl, picSize);
			if (shouldShowPic() || isLocalFile(mWrapImagUrl)) {
				ImageLoader.getInstance().displayImage(mWrapImagUrl, imageView, o,
						loadingListener);
				LogUtil.i("ImageManager", "加载图片:"+mWrapImagUrl);
			} else {
				if (hasCache(mWrapImagUrl)) {
					ImageLoader.getInstance().displayImage(mWrapImagUrl, imageView,
							o, loadingListener);
					LogUtil.i("ImageManager", "加载缓存图片:"+mWrapImagUrl);
				} else {
					imageView.setImageResource(R.drawable.image_loader_noimage);
					imageView
							.setOnLongClickListener(new View.OnLongClickListener() {
	                            @Override
	                            public boolean onLongClick(View view) {
	                                ImageLoader.getInstance().displayImage(
	                                        mWrapImagUrl, imageView, o,
	                                        loadingListener);
	                                return true;
	                            }
	                        });
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
    /**
     * 从服务器获取图片
     * <p><b>用于下载之类的不需要展示控件</b></p>
     * @param imgUrl 图片地址
     * @param loadingListener ImageLoadingListener  加载监听
     */
    public static void LoadWithoutImageView(String imgUrl,ImageLoadingListener loadingListener){
        ImageLoader.getInstance().loadImage(wrapImagUrl(imgUrl, ""),loadingListener);
    }

	private static boolean shouldShowPic() {
		SettingPreferenceUtil.SettingItem mSettingItem = AppContext
				.getInstance().getSettingSharePreference();
		if (mSettingItem == null)
			return true;
		switch (mSettingItem.interType) {
		case 1:
			// normal
			return false;
		case 2:
			// high_quality
			return true;
		case 3:
			// auto
			return AppContext.getInstance().isWifi();
		}
		return true;
	}

	/**
	 * 该图片是否已被缓存
	 *
	 * @param wrapImagUrl
	 *            被包装过的url
	 * @return boolean
	 */
	private static boolean hasCache(String wrapImagUrl) {
		List<String> keys = MemoryCacheUtils.findCacheKeysForImageUri(
				wrapImagUrl, ImageLoader.getInstance().getMemoryCache());
		if (keys == null || keys.size() < 1) {
			File cache = DiskCacheUtils.findInCache(wrapImagUrl, ImageLoader
					.getInstance().getDiskCache());
			if (cache == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 包装图片url,添加域名。 <b>如果url本身包含主域名就不用额外添加域名.<b/>
	 * <p/>
	 * "http://site.com/image.png" // from Web<br/>
	 * "file:///mnt/sdcard/image.png" // from SD card<br/>
	 * "file:///mnt/sdcard/video.mp4" // from SD card (video thumbnail)<br/>
	 * "content://media/external/images/media/13" // from content provider<br/>
	 * "content://media/external/video/media/13" // from content provider (video
	 * thumbnail)<br/>
	 * "assets://image.png" // from assets<br/>
	 * "drawable://" + R.drawable.img // from drawables (non-9patch images)<br/>
	 * <p/>
	 *
	 * @param originUrl 原始url
	 * @param picSize 图片尺寸  只允许 numXnum如200X200
	 * @return String
	 */
	private static String wrapImagUrl(String originUrl, String picSize) {

        if(TextUtils.isEmpty(originUrl)){
            return "";
        }

        //拼接图片，按照尺寸拼接(只处理网络图片)
        if(!TextUtils.isEmpty(picSize) && !isLocalFile(originUrl)){
        	originUrl = StringUtils.addPicSizeInImgUrl(originUrl, picSize);
        }
        
		String wrappedUrl;
		if (originUrl.startsWith("http") || originUrl.startsWith("https")
				|| originUrl.startsWith("file://")
				|| originUrl.startsWith("content://")
				|| originUrl.startsWith("assets://")
				|| originUrl.startsWith("drawable://")) {
			wrappedUrl = originUrl;
		} else {
			wrappedUrl = URLs.IMAGE_URL + originUrl;
		}
		return wrappedUrl;
	}
	
	private static boolean isLocalFile(String originUrl){
		if (originUrl.startsWith("file://")
				|| originUrl.startsWith("content://")
				|| originUrl.startsWith("assets://")
				|| originUrl.startsWith("drawable://")) {
			return true;
		}
		return false;
	}
	
	
}
