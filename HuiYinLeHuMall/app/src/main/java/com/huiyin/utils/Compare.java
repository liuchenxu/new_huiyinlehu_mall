package com.huiyin.utils;

import java.util.Comparator;

import com.huiyin.db.SQLOpearteImpl.Area;

public class Compare implements Comparator<Area> {

	public int compare(Area a1, Area a2) {
		if ("扬州".equals(a1.areaName)) {
			return -1;
		} else if (!"扬州".equals(a1.areaName)) {
			return 1;
		}
		return 0;
	}
}
