package com.huiyin.ui.fillorder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.adapter.OrderGoodsListAdapter;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.base.BaseActivity;
import com.huiyin.bean.ShoppingCarDataBaseBeanNew;
import com.huiyin.bean.ShoppingCarStoreBean;
import com.huiyin.db.SQLOpearteImpl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by justme on 15/5/31. 商品清单
 */
public class FillOrderGoodsListActivityNew extends BaseActivity {

	public final static String SHOPIDS = "shopIds"; // 购物车ID
	public final static String REMARK = "remark"; // 商品列表数据

	private TextView ab_back;
	private TextView ab_title;
	private TextView goodsInfo;
	private ListView mListview;

	private Context mContext;
	private ArrayList<ShoppingCarStoreBean> cart;
	private ArrayList<ShoppingCarDataBaseBeanNew.storeFreight> storefreight;
	private OrderGoodsListAdapter mAdapter;
	
	
	private String remark;
	private String shoppingIds;		//购物车ID集合
	private String addressDetail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.goods_list_layout);
		mContext = this;

		findView();
		setListener();
		initData();
	}

	/**
	 * 初始化数据
	 */
	private void initData() {
		Intent intent = getIntent();
		
		//ShoppingCarDataReturnMap objectses = (ShoppingCarDataReturnMap)intent.getSerializableExtra(GOODS_LIST);
		//cart =objectses.getCart();
		
		
		remark = intent.getStringExtra(REMARK);
		shoppingIds = intent.getStringExtra(SHOPIDS);
		addressDetail = intent.getStringExtra("addressDetail");
	
		//购物车初始化
		getTheShopCarList();
	}
	
	/**
	 * 显示商品清单
	 */
	private void showData(){
		List<OrderGoodsListAdapter.RemarksJson> mRemarksJsons = null;
		if (!TextUtils.isEmpty(remark)) {
			mRemarksJsons = jsonToList(remark, OrderGoodsListAdapter.RemarksJson.class);
		}
		mAdapter = new OrderGoodsListAdapter(mContext, cart, mRemarksJsons,storefreight);
		mListview.setAdapter(mAdapter);

		goodsInfo.setVisibility(cart.size() > 1 ? View.VISIBLE : View.GONE);
	}
	
	private void findView() {
		ab_back = (TextView) findViewById(R.id.ab_back);
		ab_title = (TextView) findViewById(R.id.ab_title);
		ab_title.setText("商品清单");

		goodsInfo = (TextView) findViewById(R.id.goods_info);
		mListview = (ListView) findViewById(R.id.m_listview);

	}

	private void setListener() {
		ab_back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	
	/**
     * 初始化查询购物车数据
     */
    private void getTheShopCarList() {
        CustomResponseHandler handler = new CustomResponseHandler(this, true) {

            @Override
            public void onRefreshData(String content) {
                super.onRefreshData(content);
                handlerData(content);
            }
        };

		try {
			SQLOpearteImpl temp = new SQLOpearteImpl(mContext);
			String Ids = temp.checkIdsByAddress(addressDetail);
			temp.CloseDB();
			String[] ida = Ids.split(",",3);
			RequstClient.shoppingCatData(handler, null, shoppingIds, ida[0], ida[1], ida[2], AppContext.getInstance().getRegionId());
		} catch (Exception e) {
			e.printStackTrace();
			RequstClient.shoppingCatData(handler, null, shoppingIds, AppContext.getInstance().getRegionId());
		}
	}
	
    /**
     * 处理购物车清单数据
     * @param content
     */
    public void handlerData(String content) {
    	if(!isSuccessResponse(content)){
    		return;
    	}
    	
    	//解析数据
    	ShoppingCarDataBaseBeanNew data = ShoppingCarDataBaseBeanNew.explainJson(content, mContext);
        if (data != null && data.type > 0) {

        	//显示数据
        	cart = data.getCart().getCart();
			storefreight = data.getStorefreight();
        	if(null != cart && cart.size() > 0){
        		showData();
        	}
        } 
    }
    

	@Override
	public void finish() {
		if(null != mAdapter){
			Intent intent = new Intent();
			intent.putExtra("remarksJson", mAdapter.getTheResult());
			setResult(RESULT_OK, intent);
		}
		super.finish();
	}

	static <T> ArrayList<T> jsonToList(String json, Class<T> classOfT) {
		Type type = new TypeToken<ArrayList<JsonObject>>() {
		}.getType();
		ArrayList<JsonObject> jsonObjs = new Gson().fromJson(json, type);
		ArrayList<T> listOfT = new ArrayList<T>();
		for (JsonObject jsonObj : jsonObjs) {
			listOfT.add(new Gson().fromJson(jsonObj, classOfT));
		}

		return listOfT;
	}
}
