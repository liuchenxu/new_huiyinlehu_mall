package com.huiyin.ui.videoplayer;

import java.io.IOException;
import java.net.URL;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.ui.videoplayer.player.PlayerManager;

public class VideoPlayerActivityNew extends Activity implements OnClickListener, Callback{
	
	public static final String TAG = "VideoPlayerActivityNew";
	
	public static final String VIDEO_SRC = "videoSrc";
	
	public static final String VIDEO_CURRENTT_POSITION = "currentPosition";
	
	private SurfaceView mSurfaceView;
	
	private PlayerManager mPlayerManager;
	
	private Button  playBtn;
	
	private String videoSrc;
	
	private int curPosition;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.videoview_item_new);

	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		init();
	}

	private void init() {
		
		videoSrc = getIntent().getStringExtra(VIDEO_SRC);
		curPosition = getIntent().getIntExtra(VIDEO_CURRENTT_POSITION, 0);
		
		playBtn = (Button) findViewById(R.id.play_btn);
		playBtn.setOnClickListener(this);
		
		initVideo();
		mPlayerManager = PlayerManager.getInstance();
	}
	
	private void initVideo() {
		mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
		mSurfaceView.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);      
		mSurfaceView.getHolder().addCallback(this);
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.play_btn:
			
			if(mPlayerManager.isPlaying()) {
				mPlayerManager.pause();
			}
			else {
				mPlayerManager.start();
			}
			
			break;

		default:
			break;
		}
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "surfaceCreated");
		
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		Log.d(TAG, "surfaceChanged");
		mPlayerManager.initMediaPlayer(mSurfaceView, videoSrc);
		mPlayerManager.seekTo(curPosition);
		mPlayerManager.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "surfaceDestroyed");
	}
	
	

}
