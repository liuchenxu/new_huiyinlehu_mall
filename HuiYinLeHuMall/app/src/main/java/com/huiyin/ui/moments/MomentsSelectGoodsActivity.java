package com.huiyin.ui.moments;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.base.BaseActivity;
import com.huiyin.bean.MomentsGoodsItem;
import com.huiyin.bean.MomentsGoodsListBean;
import com.huiyin.ui.moments.adapter.MomentsGoodsAdapter;
import com.huiyin.utils.AppManager;
/**
 * 秀好物-选择商品
 * @author zhyao
 *
 */
public class MomentsSelectGoodsActivity extends BaseActivity implements OnClickListener, OnItemClickListener{
	
	private static final String TAG = "MomentsSelectGoodsActivity";

	/**返回**/
	private TextView mBackTv;
	
	/**发布**/
	private TextView mNextTv;
	
	/**无数据**/
	private TextView mNoDataTv;
	
	/**商品列表**/
	private ListView mGoodsList;
	
	/**商品列表adapter**/
	private MomentsGoodsAdapter mGoodsAdapter;
	
	/**商品列表**/
	private ArrayList<MomentsGoodsItem> goodsList;
	
	/** 分页： 当前页 **/
	private int pageIndex = 1;

	/** 分页： 每页大小 **/
	private int pageSize = 1000;

	/** 分页： 总页数 **/
	private int totalPageNum;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_moments_select_goods);
		
		initView();
		initData();
	}
	
	private void initView() {
		
		mBackTv = (TextView) findViewById(R.id.tv_back);
		mNextTv = (TextView) findViewById(R.id.tv_next);
		mNoDataTv = (TextView) findViewById(R.id.no_data_view);
		mGoodsList = (ListView) findViewById(R.id.lv_goods);
		
		mBackTv.setOnClickListener(this);
		mNextTv.setOnClickListener(this);
		mGoodsList.setOnItemClickListener(this);
	}
	
	private void initData() {
		String userId = AppContext.userId;
		
		goodsList = new ArrayList<MomentsGoodsItem>();
		pageIndex = 1;

		requestGoodsList(userId, pageIndex, pageSize);
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		//返回
		case R.id.tv_back:
			AppManager.getAppManager().finishActivity();
			break;
		//发布
		case R.id.tv_next:
			try {
				doNext();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			break;

		default:
			break;
		}
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		mGoodsAdapter.setSelectPosition(position);
		mGoodsAdapter.notifyDataSetChanged();
	}

	/**
	 * 请求圈子详情，当前圈子下的秀场列表
	 * 
	 * @param circleId
	 * @param pageIndex
	 * @param pageSize
	 */
	private void requestGoodsList(String userId, int pageIndex, int pageSize) {
		RequstClient.showGoodsList(userId, pageIndex, pageSize, new CustomResponseHandler(this) {
			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);

				MomentsGoodsListBean bean = MomentsGoodsListBean.explainJson(content, MomentsSelectGoodsActivity.this);

				// 成功
				if (bean.type == 1) {
					totalPageNum = bean.getTotalPageNum();
					
					try {
						refreshGoodsListData(bean);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				// 失败
				else {
					UIHelper.showToast(bean.msg);
				}
			}
		});
	}
	
	/**
	 * 设置秀好物信息
	 * @param bean
	 */
	private void refreshGoodsListData(MomentsGoodsListBean bean) {
		goodsList = bean.getGoodsList();
		setNoDataUI(goodsList.isEmpty());
		
		setAdapter();
	}
	
	/**
	 * 没有商品列表
	 * @param flag
	 */
	private void setNoDataUI(boolean flag) {
		if(flag) {
			mNoDataTv.setVisibility(View.VISIBLE);
			mNextTv.setVisibility(View.GONE);
			mGoodsList.setVisibility(View.GONE);
		}
		else {
			mNoDataTv.setVisibility(View.GONE);
			mNextTv.setVisibility(View.VISIBLE);
			mGoodsList.setVisibility(View.VISIBLE);
		}
	}
	
	private void setAdapter() {
		mGoodsAdapter = new MomentsGoodsAdapter(this, goodsList);
		mGoodsList.setAdapter(mGoodsAdapter);
	}
	
	/**
	 * 下一步，发布秀好物
	 */
	private void doNext() {
		if (!goodsList.isEmpty()) {
			Intent intent = new Intent(MomentsSelectGoodsActivity.this, MomentsShowGoodsActivity.class);
			intent.putExtra(MomentsShowGoodsActivity.INTENT_KEY_GOODS, goodsList.get(mGoodsAdapter.getSelectPosition()));
			startActivity(intent);
		}
		else {
			UIHelper.showToast("您还没有可晒的商品");
		}
		
	}
}
