package com.huiyin.ui.web;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.api.URLs;
import com.huiyin.base.BaseActivity;
import com.huiyin.db.SQLOpearteImpl;
import com.huiyin.utils.LocationUtil;
import com.huiyin.utils.StringUtils;

/**
 * 商品详情Html5版本
 * 
 * @author zhyao
 * 
 */
public class GoodsDetailWebActivity extends BaseActivity {
	
	private static final String TAG = GoodsDetailWebActivity.class.getName();
	
	/**
	 * 商品详情的key
	 */
	public static final String BUNDLE_KEY_GOODS_ID = "goods_detail_id";
	
	/**
	 * 商品店铺id
	 */
	public static final String BUNDLE_KEY_STORE_ID = "STORE_ID";
	/**
	 * 商品编号
	 */
	public static final String BUNDLE_KEY_GOODS_NO = "GOODS_NO";


	
	private CustomWebView mCustomWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.fragment_home_web);

		mCustomWebView = (CustomWebView) findViewById(R.id.webview);
		mCustomWebView.setOnRefreshEnable(false);
		try {
			loadData(getIntent());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		try {
			loadData(intent);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		mCustomWebView.onResume();
		
	}

	@Override
	public void onStop() {
		super.onStop();

		mCustomWebView.onStop();
	}

	private void loadData(Intent intent) throws UnsupportedEncodingException {
	
		String shoppingIds = AppContext.getShopcardId();
		String goodsId = intent.getStringExtra(BUNDLE_KEY_GOODS_ID);
		String storeId = intent.getStringExtra(BUNDLE_KEY_STORE_ID);
		String goodsNo = intent.getStringExtra(BUNDLE_KEY_GOODS_NO);
		String provinceName = "";
        String cityName = ""; 
        int provinceId = -1;
        int cityId = -1;
        
        HashMap<String, String> locationMap = LocationUtil.getCurrentLocation(mContext);
        provinceName = locationMap.get("provinceName");
        cityName = locationMap.get("cityName");
        
        SQLOpearteImpl temp = new SQLOpearteImpl(this);
        provinceId = temp.checkIdByName(provinceName);
        cityId = temp.checkIdByName(cityName);
        temp.CloseDB();
        
		StringBuffer params = new StringBuffer();
		params.append("?");
		if (!StringUtils.isEmpty(shoppingIds)) {
			params.append("shoppingIds=").append(shoppingIds).append("&");
		}
		if (!StringUtils.isEmpty(goodsId)) {
			params.append("goodsId=").append(goodsId).append("&");
		}
		if (!StringUtils.isEmpty(storeId)) {
			params.append("storeId=").append(storeId).append("&");
		}
		if (!StringUtils.isEmpty(goodsNo)) {
			params.append("goodsNo=").append(goodsNo).append("&");
		}
		if (!StringUtils.isEmpty(provinceName)) {
			params.append("provinceName=").append(provinceName).append("&");
		}
		if (provinceId != -1) {
			params.append("provinceId=").append(provinceId).append("&");
		}
		if (!StringUtils.isEmpty(cityName)) {
			params.append("cityName=").append(cityName).append("&");
		}
		if (cityId != -1) {
			params.append("cityId=").append(cityId).append("&");
		}
		try {
			mCustomWebView.loadUrl(URLs.goods_detail_web + params.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
