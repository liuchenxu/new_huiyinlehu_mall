package com.huiyin.ui.user.order;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.huiyin.R;
import com.huiyin.bean.MyOrderListBean;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase.Mode;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshListView;
import com.huiyin.newpackage.base.NewBaseFragment;
import com.huiyin.newpackage.consts.HTTPConsts;
import com.huiyin.newpackage.entity.OrderInfo;
import com.huiyin.newpackage.event.ApplyServicesGoodsListEvent;
import com.huiyin.newpackage.ui.adapter.ApplyForBackGoodsAdapter;

import java.util.ArrayList;

/**
 * 预约退换货-搜索界面
 * 
 * @author 刘远祺
 * 
 * @todo TODO
 * 
 * @date 2015-7-3
 */
public class SearchOrderFragment extends NewBaseFragment {
	private static final String TAG = "ReturnRecordFragment";
	private ImageView searchorderiv;
	private EditText searchcontented;
	private PullToRefreshListView lvorder;
	private ApplyForBackGoodsAdapter adapter;
	private MyOrderListBean orderListBean;
	private String keyWord;// 关键字
	private int pageIndex = 1;// 当前页
	private int pageSize = 10;// 每页数据
	private LinearLayout layout_empty;// 提示数据为空布局
	private View layoutView;
	boolean refresh = true;
	private int page = 1;
	private ArrayList<OrderInfo> lists = new ArrayList<OrderInfo>();
	private ImageView search_order_clear;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		register = true;
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layoutView = inflater.inflate(R.layout.layout_search_order_fragment, null);
		findView();
		return layoutView;
	}

	@Override
	public void onResume() {
		super.onResume();
		setListener();
		keyWord = "";
		showProgress(true);
		getApplyGoodsList();
	}

	public void findView() {
		this.lvorder = (PullToRefreshListView) layoutView.findViewById(R.id.lv_order);
		lvorder.setMode(Mode.BOTH);
		lvorder.getLoadingLayoutProxy(false, true).setPullLabel("上拉加载更多");
		lvorder.getLoadingLayoutProxy(false, true).setReleaseLabel("松开加载");
		lvorder.getLoadingLayoutProxy(false, true).setRefreshingLabel("正在加载。。");

		lvorder.getLoadingLayoutProxy(true, false).setPullLabel("下拉刷新数据");
		lvorder.getLoadingLayoutProxy(true, false).setReleaseLabel("松开刷新");
		lvorder.getLoadingLayoutProxy(true, false).setRefreshingLabel("正在刷新。。");
		this.searchcontented = (EditText) layoutView.findViewById(R.id.search_content_ed);
		this.searchorderiv = (ImageView) layoutView.findViewById(R.id.search_order_iv);
		this.layout_empty = (LinearLayout) layoutView.findViewById(R.id.layout_empty);
		this.search_order_clear = (ImageView) layoutView.findViewById(R.id.search_order_clear);
		adapter = new ApplyForBackGoodsAdapter(getActivity(), lists);
		lvorder.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase refreshView) {
				refresh = true;
				keyWord = searchcontented.getText().toString();
				lists.clear();
				getApplyGoodsList();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase refreshView) {
				refresh = false;
				getApplyGoodsList();
			}
		});
		searchcontented.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEND
						|| (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
					keyWord = v.getText().toString();
					refresh = true;
					hideSoftInput();
					getApplyGoodsList();
				}
				return false;
			}
		});

		// 搜索框清空的时候做一遍搜索数据
		searchcontented.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				String contentString = s.toString();
				if (TextUtils.isEmpty(contentString)) {
					search_order_clear.setVisibility(View.INVISIBLE);
					SearchOrderFragment.this.hideSoftInput();
					keyWord = "";
					refresh = true;
					getApplyGoodsList();
				} else {
					search_order_clear.setVisibility(View.VISIBLE);
				}
			}
		});

		// 清除搜索关键字数据
		search_order_clear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				keyWord = "";
				searchcontented.setText("");
			}
		});
		lvorder.setAdapter(adapter);
	}

	private void setListener() {
		searchorderiv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			}
		});
	}

	private void getApplyGoodsList() {
		int next = page;
		if (refresh)
			next = 1;
		ApplyServicesGoodsListEvent.GetApplyServcieGoodsList(client, next, keyWord);
	}

	public void onEvent(ApplyServicesGoodsListEvent event) {
		finishProgress();
		if (refresh) { // 刷新，则清空，页码置为1
			lists.clear();
			page = 1;
		}
		if (event.type == HTTPConsts.HTTP_OK) {
			if (event.orderList != null && event.orderList.size() >= 0) {
				if (event.getTotalPageNum() >= event.getPageIndex()) {
					page++;
					ArrayList<OrderInfo> values = event.getOrderList();
					lists.addAll(values);
					adapter.setOrderList(lists);
					adapter.notifyDataSetChanged();
				}
			} else {
				layout_empty.setVisibility(View.VISIBLE);
			}
		} else {
			String message = event.msg;
			if (message != null) {
				showToast(message);
			}
		}
		refreshComplete(lvorder);
	}
}
