package com.huiyin.ui.classic;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.bean.CategoryAndBrandBean;
import com.huiyin.bean.CategoryAndBrandBean.Category;
import com.huiyin.bean.CategoryAndBrandBean.NationalPavilion;
import com.huiyin.bean.CategoryAndBrandBean.Navigator;
import com.huiyin.ui.home.SiteSearchActivity;
import com.huiyin.utils.DensityUtil;
import com.huiyin.utils.NetworkUtils;
import com.zxing.scan.activity.ZxingCodeActivity;

/**
 * 分类-新分类（南京）
 * @author zhyao
 *
 */
public class CategoryFragmentNew extends Fragment implements OnClickListener, OnPageChangeListener {

	private static final String TAG = "CategoryFragmentNew";

	private View v;
	
	private ImageView mScanCodeImg;
	
	private View mSearchView;;
	
	private LinearLayout mNoNetworkLayout;
	
	private LinearLayout mCategoryLayout;
	
	private Button mReloadBtn;
	
	private TextView mCategoryTv;
	
	private TextView mCountryTv;
	
	private TextView mBrandTv;
	
	private ImageView mViewpagerIndex;
	
	private ViewPager mViewpager;
	
	private MyAdapter mAdapter;
	
	private List<Fragment> mFragmentList;
	
	private CategoryClassFragment mCategoryClassFragment;
	
	private CategoryCountryFragment mCategoryCountryFragment;
	
	private CategoryBrandFragment mCategoryBrandFragment;
	
	private CategoryAndBrandBean bean;
	
	/**
	 * 下划线滑动宽度
	 */
	private int indexWidth;
	
	/**
	 * 当前页卡编号  
	 */
	private int currIndex;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.fragment_classic_new, null);
		initView();
		//initFragment();
		initData();
		return v;
	}

	private void initView() {
		indexWidth = DensityUtil.dp2px(getActivity(), 53);
		
		mScanCodeImg = (ImageView) v.findViewById(R.id.class_sort_code_scan);
		mSearchView = v.findViewById(R.id.ab_search);
		
		mNoNetworkLayout = (LinearLayout) v.findViewById(R.id.layout_no_network);
		mCategoryLayout = (LinearLayout) v.findViewById(R.id.layout_category);
		mReloadBtn = (Button) v.findViewById(R.id.btn_reload);
		mCategoryTv = (TextView) v.findViewById(R.id.tv_category);
		mCountryTv = (TextView) v.findViewById(R.id.tv_country);
		mBrandTv = (TextView) v.findViewById(R.id.tv_brand);
		mViewpagerIndex = (ImageView) v.findViewById(R.id.viewpager_index);
		mViewpager = (ViewPager) v.findViewById(R.id.viewpager);
		
		mScanCodeImg.setOnClickListener(this);
		mSearchView.setOnClickListener(this);
		mReloadBtn.setOnClickListener(this);
		mCategoryTv.setOnClickListener(this);
		mCountryTv.setOnClickListener(this);
		mBrandTv.setOnClickListener(this);
		mViewpager.setOnPageChangeListener(this);
	}

	 private void initFragment(ArrayList<Category> categories, ArrayList<NationalPavilion> countrys, ArrayList<Navigator> navigators) {
		mCategoryClassFragment = CategoryClassFragment.newInstance(categories);
		mCategoryCountryFragment = CategoryCountryFragment.newInstance(countrys);
		mCategoryBrandFragment = CategoryBrandFragment.newInstance(navigators);
		 
    	mFragmentList = new ArrayList<Fragment>();
    	mFragmentList.add(mCategoryClassFragment);
    	mFragmentList.add(mCategoryCountryFragment);
    	mFragmentList.add(mCategoryBrandFragment);
    	
    	mAdapter = new MyAdapter(getChildFragmentManager());
    	mViewpager.setAdapter(mAdapter);
    	mViewpager.setCurrentItem(0);
	}
	 
	 private void initData() {
		// 检测网络
		if (!NetworkUtils.isNetworkAvailable(getActivity())) {
			Toast.makeText(getActivity(), "网络不可用，请先检查网络！", Toast.LENGTH_SHORT)
					.show();
			mCategoryLayout.setVisibility(View.GONE);
			mNoNetworkLayout.setVisibility(View.VISIBLE);
			return;
		} else {
			getData();
		}
	 }
	 
	 private void getData() {
		 RequstClient.listCategoryAndBrand(new CustomResponseHandler(getActivity()) {
			 
			@Override
			public void onSuccess(int statusCode, String content) {
				super.onSuccess(statusCode, content);
				
				bean = CategoryAndBrandBean.explainJson(content, getActivity());
				if(bean.getType() == 1) {
					
					try {
						initFragment(bean.getCategories(), bean.getNationalPavilionList(), bean.getNavigators());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				else {
					UIHelper.showToast(bean.getMsg());
				}
			}
			 
			@Override
			public void onFailure(String error, String errorMessage) {
				super.onFailure(error, errorMessage);
				mCategoryLayout.setVisibility(View.GONE);
				mNoNetworkLayout.setVisibility(View.VISIBLE);
			}
		 });
	 }
	 
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		//分类
		case R.id.tv_category:
			if(!mCategoryTv.isSelected()) {
				mCategoryTv.setSelected(true);
				mCountryTv.setSelected(false);
				mBrandTv.setSelected(false);
				mViewpager.setCurrentItem(0);
			}
			break;
		//国家
		case R.id.tv_country:
			if(!mCountryTv.isSelected()) {
				mCategoryTv.setSelected(false);
				mCountryTv.setSelected(true);
				mBrandTv.setSelected(false);
				mViewpager.setCurrentItem(1);
			}
			break;
		//品牌
		case R.id.tv_brand:
			if(!mBrandTv.isSelected()) {
				mCategoryTv.setSelected(false);
				mCountryTv.setSelected(false);
				mBrandTv.setSelected(true);
				mViewpager.setCurrentItem(2);
			}
			break;
		case R.id.class_sort_code_scan:
			startActivity(new Intent(getActivity(), ZxingCodeActivity.class));
			break;
		case R.id.ab_search:
			startActivity(new Intent(getActivity(), SiteSearchActivity.class));
			break;
		case R.id.btn_reload:
			if (!NetworkUtils.isNetworkAvailable(getActivity())) {
				Toast.makeText(getActivity(), "网络没连接吧，亲！请先检查一下网络吧！", Toast.LENGTH_SHORT).show();
				return;
			} else {
				mCategoryLayout.setVisibility(View.VISIBLE);
				mNoNetworkLayout.setVisibility(View.GONE);
				getData();
			}
			break;
		default:
			break;
		}
	}
	
	@Override
	public void onPageScrollStateChanged(int arg0) {
		
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		
	}

	@Override
	public void onPageSelected(int arg0) {
		
		if (arg0 == 0) {
			mCategoryTv.setSelected(true);
			mCountryTv.setSelected(false);
			mBrandTv.setSelected(false);
		} else if (arg0 == 1) {
			mCategoryTv.setSelected(false);
			mCountryTv.setSelected(true);
			mBrandTv.setSelected(false);
		} else if (arg0 == 2) {
			mCategoryTv.setSelected(false);
			mCountryTv.setSelected(false);
			mBrandTv.setSelected(true);
		}
		
		//移动动画
		Animation animation = new TranslateAnimation(currIndex * indexWidth, arg0 * indexWidth, 0, 0);//平移动画  
        currIndex = arg0;  
        animation.setFillAfter(true);
        animation.setDuration(200);
        mViewpagerIndex.startAnimation(animation);
		
	}
	
	class MyAdapter extends FragmentStatePagerAdapter {
		

		public MyAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int arg0) {
			return mFragmentList.get(arg0);
		}

		@Override
		public int getCount() {
			return mFragmentList == null ? 0 : mFragmentList.size();
		}
		
	}

}
