package com.huiyin.ui.home;

import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.adapter.HotRecommendViewflowAdapter;
import com.huiyin.bean.HotRecommendationBean.RecommendationBean;
import com.huiyin.wight.viewflow.CircleFlowIndicator;
import com.huiyin.wight.viewflow.ViewFlow;
import com.huiyin.wight.viewflow.ViewFlow.ViewSwitchListener;
/**
 * 热点推荐
 * @author zhyao
 *
 */
public class HotRecommendView extends LinearLayout{
	
	private Context mContext;
	
	private List<RecommendationBean> mRecommendList;
	
	private String mTile;
	
	private String mDesc;
	
	private TextView hot_recommend_tilte_name;
	
	private TextView hot_recommend_tilte_desc;
	
	private ViewFlow hotRecommendViewFlow;
	
	private CircleFlowIndicator hotRecommendIndic;
	
	private HotRecommendViewflowAdapter hotRecommendViewflowAdapter;


	public HotRecommendView(Context context) {
		super(context);
		initLayout(context);
	}

	public HotRecommendView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initLayout(context);
	}

	public HotRecommendView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initLayout(context);
	}
	

	public HotRecommendView(Context context, String title, String desc, List<RecommendationBean> recommendList) {
		super(context);
		mTile = title;
		mDesc = desc;
		mRecommendList = recommendList;
		initLayout(context);
	}
	
	private void initLayout(Context context) {
		mContext = context;
		View view = LayoutInflater.from(mContext).inflate(R.layout.hot_recomeend_view, null, false);
		
		hot_recommend_tilte_name = (TextView) view.findViewById(R.id.hot_recommend_tilte_name);
		hot_recommend_tilte_desc = (TextView) view.findViewById(R.id.hot_recommeed_tilte_desc);
		
		hotRecommendViewFlow = (ViewFlow) view.findViewById(R.id.mHotRecommendViewflow);// 获得viewFlow对象
		hotRecommendIndic = (CircleFlowIndicator) view.findViewById(R.id.mHotRecommendViewflowindic); // viewFlow下的indic
		
		hot_recommend_tilte_name.setText(mTile);
		hot_recommend_tilte_desc.setText(mDesc);
		
		hotRecommendViewflowAdapter = new HotRecommendViewflowAdapter(mContext, mRecommendList);
		
		hotRecommendViewFlow.setAdapter(hotRecommendViewflowAdapter); // 对viewFlow添加图片
		hotRecommendViewFlow.setmSideBuffer(mRecommendList.size());
		hotRecommendViewFlow.setFlowIndicator(hotRecommendIndic);
		hotRecommendViewFlow.setTimeSpan(5000);
		hotRecommendViewFlow.setSelection(mRecommendList.size() * 10); // 设置初始位置
		if(mRecommendList.size() > 1) {
			hotRecommendViewFlow.startAutoFlowTimer();
		}
		else {
			hotRecommendViewFlow.stopAutoFlowTimer();
		}
		hotRecommendViewFlow.setOnViewSwitchListener(new ViewSwitchListener() {
			
			public void onSwitched(View view, int position) {
				
			}
		});
		
		addView(view);
	}

}
