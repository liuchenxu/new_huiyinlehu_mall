package com.huiyin.ui.classic;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.huiyin.R;
import com.huiyin.adapter.CategoryBrandAdapter;
import com.huiyin.bean.CategoryAndBrandBean.NationalPavilion;
import com.huiyin.bean.CategoryAndBrandBean.Navigator;
/**
 * 分类-品牌fragment
 * @author zhyao
 *
 */
public class CategoryBrandFragment extends Fragment {

private static final String TAG = "CategoryBrandFragment";
	
	private View rootView;
	
	private ListView mBrandList;
	
	private CategoryBrandAdapter mAdapter;
	
	private ArrayList<Navigator> mNavigators;
	
	public static CategoryBrandFragment newInstance(ArrayList<Navigator> navigators) {
		CategoryBrandFragment instance = new CategoryBrandFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable("data", navigators);
		instance.setArguments(bundle);
		return instance;
	}
	
	@Override
	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate");
		
		mNavigators = (ArrayList<Navigator>) getArguments().getSerializable("data");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		rootView = inflater.inflate(R.layout.fragment_category_brand, null);
		initView();
		setData(mNavigators);
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.i(TAG, "onResume");

	}

	private void initView() {
		mBrandList = (ListView) rootView.findViewById(R.id.lv_category_brand);
	}
	
	public void setData(ArrayList<Navigator> navigators) {
		//if(mAdapter == null) {
			mAdapter = new CategoryBrandAdapter(getActivity(), navigators);
			mBrandList.setAdapter(mAdapter);
		//}
	}
}
