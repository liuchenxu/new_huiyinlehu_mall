package com.huiyin.ui.moments.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.api.URLs;
import com.huiyin.bean.MomentsShowItem;
import com.huiyin.ui.moments.MomentsDetailActivity;
import com.huiyin.utils.ImageManager;
import com.jcvideoplayer.JCVideoPlayerStandard;

/**
 * 秀场列表
 * 
 * @author zhyao
 * 
 */
public class MomentsShowAdapter extends BaseAdapter {

	private static final String TAG = "MomentsShowAdapter";

	private Context mContext;

	private ArrayList<MomentsShowItem> listDatas;

	public MomentsShowAdapter(Context mContext, ArrayList<MomentsShowItem> listDatas) {
		super();
		this.mContext = mContext;
		this.listDatas = listDatas;
		
	}

	@Override
	public int getCount() {
		return listDatas == null ? 0 : listDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return listDatas == null ? null : listDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_moments_show, null);

			viewHolder.mShowInfoLayout = (LinearLayout) convertView.findViewById(R.id.ll_show_info);
			viewHolder.mShowImg = (ImageView) convertView.findViewById(R.id.img_show);
			viewHolder.mVideoLayout = (RelativeLayout) convertView.findViewById(R.id.rl_video);
			viewHolder.mJcVideoPlayer = (JCVideoPlayerStandard) convertView.findViewById(R.id.videoplayer);
			viewHolder.mTitleTv = (TextView) convertView.findViewById(R.id.tv_title);
			viewHolder.mHeadImg = (ImageView) convertView.findViewById(R.id.img_head);
			viewHolder.mNameTv = (TextView) convertView.findViewById(R.id.tv_name);
			viewHolder.mTimeTv = (TextView) convertView.findViewById(R.id.tv_time);
			viewHolder.mFavoriteTv = (TextView) convertView.findViewById(R.id.tv_favorite);
			viewHolder.mMessageTv = (TextView) convertView.findViewById(R.id.tv_message);
			viewHolder.mCircleTv = (TextView) convertView.findViewById(R.id.tv_circle);
			viewHolder.mDividerView = (View) convertView.findViewById(R.id.view_divider);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		//最后item隐藏分割线
		if(position == listDatas.size() - 1) {
			viewHolder.mDividerView.setVisibility(View.GONE);
		}
		else {
			viewHolder.mDividerView.setVisibility(View.VISIBLE);
		}

		final MomentsShowItem showItem = listDatas.get(position);
	
		// 标题
		viewHolder.mTitleTv.setText(showItem.getTITLE());
		// 头像 
		ImageManager.Load(showItem.getFACE_IMAGE_PATH(), viewHolder.mHeadImg, ImageManager.headOptions);
		// 姓名 
		viewHolder.mNameTv.setText(showItem.getUSER_NAME());;
		// 发布时间 
		viewHolder.mTimeTv.setText(showItem.getCREATE_TIME());;
		// 搜藏 
		viewHolder.mFavoriteTv.setText(String.valueOf(showItem.getLIKENUM()));
		// 评论消息
		viewHolder.mMessageTv.setText(String.valueOf(showItem.getAPPRAISENUM()));
		// 发布的圈子 
		viewHolder.mCircleTv.setText(showItem.getCIRCLE_NAME());

		//秀场类型   1.秀图 2.秀歌 3秀视频 4.秀好物
		// 秀视频   
		if (showItem.getTYPE() == 3) {
			viewHolder.mJcVideoPlayer.setVisibility(View.VISIBLE);
			viewHolder.mShowImg.setVisibility(View.GONE);
			viewHolder.mTitleTv.setVisibility(View.GONE);
			//设置视频路径和标题
			viewHolder.mJcVideoPlayer.setUp(URLs.IMAGE_URL + showItem.getSHOW_FILE(), "", true);
			//设置视频略所图
			ImageManager.Load(showItem.getVIDEO_IMG(), viewHolder.mJcVideoPlayer.ivThumb, ImageManager.rectangleOptions);
		} 
		// 秀图或者秀好物
		else if(showItem.getTYPE() == 1 || showItem.getTYPE() == 4){
			viewHolder.mJcVideoPlayer.setVisibility(View.GONE);
			viewHolder.mShowImg.setVisibility(View.VISIBLE);
			viewHolder.mTitleTv.setVisibility(View.VISIBLE);
			String showImgUrls = showItem.getSHOW_IMG();
			if(!TextUtils.isEmpty(showImgUrls)) {
				String showImgUrl = showImgUrls.split(",")[0];
				ImageManager.Load(showImgUrl, viewHolder.mShowImg, ImageManager.rectangleOptions);
			}
		}
		
		viewHolder.mShowImg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(showItem.getID());
			}
		});
		viewHolder.mShowInfoLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(showItem.getID());
			}
		});

		return convertView;
	}

	private void startActivity(String showId) {
		Intent intent = new Intent(mContext, MomentsDetailActivity.class);
		intent.putExtra(MomentsDetailActivity.INTENT_KEY_SHOW_ID, showId);
		mContext.startActivity(intent);
	}
	
	static class ViewHolder {
		/** list item info 布局 **/
		LinearLayout mShowInfoLayout;
		
		/** 秀场图片 **/
		ImageView mShowImg;

		/** 视频布局 **/
		RelativeLayout mVideoLayout;

		/** 视频 **/
		JCVideoPlayerStandard mJcVideoPlayer;

		/** 标题 **/
		TextView mTitleTv;

		/** 头像 **/
		ImageView mHeadImg;

		/** 姓名 **/
		TextView mNameTv;

		/** 发布时间 **/
		TextView mTimeTv;

		/** 搜藏 **/
		TextView mFavoriteTv;

		/** 评论消息 **/
		TextView mMessageTv;

		/** 发布的圈子 **/
		TextView mCircleTv;
		
		/** 分割线 **/
		View mDividerView;

	}
	
}
