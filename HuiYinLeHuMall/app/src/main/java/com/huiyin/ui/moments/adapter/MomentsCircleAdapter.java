package com.huiyin.ui.moments.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.bean.MomentsCircleItem;
import com.huiyin.utils.ImageManager;
import com.huiyin.wight.RoundImageView;

/**
 * 圈子列表adapter
 * @author zhyao
 *
 */
public class MomentsCircleAdapter extends BaseAdapter{
	
	private Context mContext;
	
	private ArrayList<MomentsCircleItem> listDatas;
	
	/**选择的商品**/
	private String selectCircleId = "";
	
	public MomentsCircleAdapter(Context context, ArrayList<MomentsCircleItem> listDatas, String lastSelectCircleId) {
		mContext = context;
		this.listDatas = listDatas;
		selectCircleId = lastSelectCircleId;
	}

	@Override
	public int getCount() {
		return listDatas == null ? 0 : listDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return listDatas == null ? null : listDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder viewHolder;
		if(convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_moments_circle, null);
			
			viewHolder.mCircleImg = (RoundImageView) convertView.findViewById(R.id.img_circle);
			viewHolder.mCircleTv = (TextView) convertView.findViewById(R.id.tv_circle);
			viewHolder.mTitleTv = (TextView) convertView.findViewById(R.id.tv_title);
			viewHolder.mDescTv = (TextView) convertView.findViewById(R.id.tv_desc);
			viewHolder.mCountTv = (TextView) convertView.findViewById(R.id.tv_count);
			viewHolder.mSelectImg = (ImageView) convertView.findViewById(R.id.img_select);
			
			convertView.setTag(viewHolder);
		}
		else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		MomentsCircleItem mCircleItem = listDatas.get(position);
		
		ImageManager.Load(mCircleItem.getIMG(), viewHolder.mCircleImg, ImageManager.squareOptions);
		viewHolder.mCircleTv.setText(mCircleItem.getNAME());
		viewHolder.mTitleTv.setText(mCircleItem.getNAME());
		viewHolder.mDescTv.setText(mCircleItem.getDESCRIPTION());
		viewHolder.mCountTv.setText(mCircleItem.getCOUNT());
		
		if(selectCircleId.equals(mCircleItem.getID())) {
			viewHolder.mSelectImg.setVisibility(View.VISIBLE);
		}
		else {
			viewHolder.mSelectImg.setVisibility(View.GONE);
		}
		
		return convertView;
	}
	
	static class ViewHolder {
		/**圈子图片**/
		RoundImageView mCircleImg;
		/**圈子名称**/
		TextView mCircleTv;
		/**圈子标题**/
		TextView mTitleTv;
		/**圈子描述**/
		TextView mDescTv;
		/**圈子今日数量**/
		TextView mCountTv;
		/**当前选折的圈子**/
		ImageView mSelectImg;
	}

	public String getSelectCircleId() {
		return selectCircleId;
	}

	public void setSelectCircleId(String selectCircleId) {
		this.selectCircleId = selectCircleId;
		notifyDataSetChanged();
	}

	public ArrayList<MomentsCircleItem> getListDatas() {
		return listDatas;
	}

	public void setListDatas(ArrayList<MomentsCircleItem> listDatas) {
		this.listDatas = listDatas;
		notifyDataSetChanged();
	}

	
}
