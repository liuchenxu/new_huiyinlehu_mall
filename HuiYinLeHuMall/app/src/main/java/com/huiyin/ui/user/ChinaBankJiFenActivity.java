package com.huiyin.ui.user;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.base.BaseActivity;
import com.huiyin.utils.JSONParseUtils;
import com.huiyin.utils.StringUtils;
import com.orhanobut.logger.Logger;

/**
 * 中行积分绑定/解绑
 * 
 * @author zhyao
 *
 */
public class ChinaBankJiFenActivity extends BaseActivity implements
		OnClickListener {
	/**
	 * 返回
	 */
	private TextView left_ib;
	/**
	 * 标题
	 */
	private TextView middle_title_tv;

	// //////绑定布局///////////
	/**
	 * 绑定中行银行卡布局
	 */
	private LinearLayout layout_bind;
	/**
	 * 银行卡卡号
	 */
	private EditText china_bank_num_ed;
	/**
	 * 真实姓名
	 */
	private EditText name_ed;
	/**
	 * 预留电话
	 */
	private EditText phone_ed;
	/**
	 * 开卡证件号
	 */
	private EditText certificate_ed;
	/**
	 * 短信校验码
	 */
	private EditText verification_code_ed;
	/**
	 * 获取校验码
	 */
	private Button verification_code_btn;
	/**
	 * 绑定中行卡
	 */
	private Button bind_btn;
	// //////解绑布局///////////
	/**
	 * 解绑中行银行卡布局
	 */
	private LinearLayout layout_unbind;
	/**
	 * 中行积分
	 */
	private TextView jifen_totalnum_tv;
	/**
	 * 银行卡卡号
	 */
	private TextView china_bank_num_tv;
	/**
	 * 真实姓名
	 */
	private TextView name_tv;
	/**
	 * 预留电话
	 */
	private TextView phone_tv;
	/**
	 * 开卡证件号
	 */
	private TextView certificate_tv;
	/**
	 * 解绑中行卡
	 */
	private Button unbind_btn;
	//////////////////////////
	/**
	 * 银行卡卡号
	 */
	private String chinaBankNum;
	/**
	 * 真实姓名
	 */
	private String name;
	/**
	 * 预留电话
	 */
	private String phone;
	/**
	 * 开卡证件号
	 */
	private String certificate;
	/**
	 * 短信验证码
	 */
	private String verification_code;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 倒计时
	 */
	private TimeCount time;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_chianbank_jifen);

		initViews();
		intiData();
	}

	private void initViews() {
		left_ib = (TextView) findViewById(R.id.left_ib);
		middle_title_tv = (TextView) findViewById(R.id.middle_title_tv);
		
		layout_bind = (LinearLayout) findViewById(R.id.layout_bind);
		china_bank_num_ed = (EditText) findViewById(R.id.china_bank_num_ed);
		name_ed = (EditText) findViewById(R.id.name_ed);
		phone_ed = (EditText) findViewById(R.id.phone_ed);
		certificate_ed = (EditText) findViewById(R.id.certificate_ed);
		verification_code_ed = (EditText) findViewById(R.id.verification_code_ed);
		verification_code_btn = (Button) findViewById(R.id.verification_code_btn);
		bind_btn = (Button) findViewById(R.id.bind_btn);

		layout_unbind = (LinearLayout) findViewById(R.id.layout_unbind);
		jifen_totalnum_tv = (TextView) findViewById(R.id.jifen_totalnum_tv);
		china_bank_num_tv = (TextView) findViewById(R.id.china_bank_num_tv);
		name_tv = (TextView) findViewById(R.id.name_tv);
		phone_tv = (TextView) findViewById(R.id.phone_tv);
		certificate_tv = (TextView) findViewById(R.id.certificate_tv);
		unbind_btn = (Button) findViewById(R.id.unbind_btn);

		left_ib.setOnClickListener(this);
		verification_code_btn.setOnClickListener(this);
		bind_btn.setOnClickListener(this);
		unbind_btn.setOnClickListener(this);
		
		middle_title_tv.setText("我的积分");
		
		time = new TimeCount(60000, 1000);//构造CountDownTimer对象
		
//		china_bank_num_ed.setText("6332600160001673823");
//		name_ed.setText("蔡远");
//		phone_ed.setText("13040221753");
//		certificate_ed.setText("321002198904104955");
	}

	private void intiData() {
		userId = AppContext.userId;
		//查询是否已绑定中行账户
		queryBocAccount();
	}
	
	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		//返回
		case R.id.left_ib:
			finish();
			break;
		//获取校验码
		case R.id.verification_code_btn:
			if(checkInfo(false)) {
				obtainVerificationCode();
			}
			break;
		//绑定
		case R.id.bind_btn:
			if(checkInfo(true)) {
				sendBindRequest();
			}
			break;
		//解绑
		case R.id.unbind_btn:
			sendUnBindRequest();
			break;
		default:
			break;
		}

	}
	
	/**
	 * 查询中行积分账户
	 */
	private void queryBocAccount() {
		RequstClient.queryBocAccount(userId, new CustomResponseHandler(this, true) {
			@Override
			public void onRefreshData(String content) {
				super.onRefreshData(content);
				//Logger.json(content);
				int type = JSONParseUtils.getInt(content, "type");
				//已绑定
				if(type == 1) {
					layout_bind.setVisibility(View.GONE);
					layout_unbind.setVisibility(View.VISIBLE);
					//设置中行积分账户信息
					setBocAccountInfo(content);
					//查询积分
					queryBocIntegral();
				}
				//未绑定
				else {
					layout_bind.setVisibility(View.VISIBLE);
					layout_unbind.setVisibility(View.GONE);
				}
				
			}
		});
	}
	
	/**
	 * 查询中行积分
	 */
	private void queryBocIntegral() {
		RequstClient.queryBocIntegral(userId, new CustomResponseHandler(this, true) {
			@Override
			public void onRefreshData(String content) {
				super.onRefreshData(content);
				//Logger.json(content);
				int type = JSONParseUtils.getInt(content, "type");
				if(type == 1) {
					setIntegralInfo(content);
				}
				else {
					UIHelper.showToast(JSONParseUtils.getString(content, "msg"));
				}
			}
		});
	}
	
	/**
	 * 获取验证码
	 */
	private void obtainVerificationCode() {
		RequstClient.validateBocIntegralAccount(userId, chinaBankNum, name, phone, certificate, new CustomResponseHandler(this, true){
			@Override
			public void onRefreshData(String content) {
				super.onRefreshData(content);
				//Logger.json(content);
				int type = JSONParseUtils.getInt(content, "type");
				if(type == 1) {
					time.start();
				}
				UIHelper.showToast(JSONParseUtils.getString(content, "msg"));
			}
		});
	}
	
	/**
	 * 绑定
	 */
	private void sendBindRequest() {
		RequstClient.openBocAccount(userId, chinaBankNum, name, phone, certificate, verification_code, new CustomResponseHandler(this, true) {
			@Override
			public void onRefreshData(String content) {
				super.onRefreshData(content);
				//Logger.json(content);
				int type = JSONParseUtils.getInt(content, "type");
				//绑定成功
				if(type == 1) {
					queryBocAccount();
				}
				UIHelper.showToast(JSONParseUtils.getString(content, "msg"));
			}
		});
	}
	
	/**
	 * 解绑
	 */
	private void sendUnBindRequest() {
		RequstClient.unBindBocAccount(userId, new CustomResponseHandler(this, true) {
			@Override
			public void onRefreshData(String content) {
				super.onRefreshData(content);
				Logger.json(content);
				int type = JSONParseUtils.getInt(content, "type");
				//解绑成功
				if(type == 1) {
					layout_bind.setVisibility(View.VISIBLE);
					layout_unbind.setVisibility(View.GONE);
				}
				UIHelper.showToast(JSONParseUtils.getString(content, "msg"));
			}
		});
	}
	
	/**
	 * 设置中行积分账户信息
	 * @param content
	 */
	private void setBocAccountInfo(String content) {
		String ACCOUNT_ID = JSONParseUtils.getString(content, "ACCOUNT_ID");
		String REAL_NAME = JSONParseUtils.getString(content, "REAL_NAME");
		String MOBILE = JSONParseUtils.getString(content, "MOBILE");
		String ID_CARD = JSONParseUtils.getString(content, "ID_CARD");
		
		china_bank_num_tv.setText(ACCOUNT_ID);
		name_tv.setText(REAL_NAME);
		phone_tv.setText(MOBILE);
		certificate_tv.setText(ID_CARD);
	}
	
	/**
	 * 设置中行积分信息
	 * @param content
	 */
	private void setIntegralInfo(String content) {
		String integral = JSONParseUtils.getString(content, "integral");
		jifen_totalnum_tv.setText(integral);
	}
	
	/**
	 * 检验输入是否合法
	 * @return
	 */
	private boolean checkInfo(boolean isBind) {
		chinaBankNum = china_bank_num_ed.getText().toString().trim();
		name = name_ed.getText().toString().trim();
		phone = phone_ed.getText().toString().trim();
		certificate = certificate_ed.getText().toString().trim();
		verification_code = verification_code_ed.getText().toString().trim();
		
		if(TextUtils.isEmpty(chinaBankNum)) {
			UIHelper.showToast("银行卡号不能为空");
			return false;
		}
		
		if(TextUtils.isEmpty(name)) {
			UIHelper.showToast("真实姓名不能为空");
			return false;
		}
		
		if(TextUtils.isEmpty(phone)) {
			UIHelper.showToast("银行预留手机号不能为空");
			return false;
		}
		
		if(!StringUtils.isPhoneNumber(phone)) {
			UIHelper.showToast("请输入正确的银行预留手机号码");
			return false;
		}
		
		if(TextUtils.isEmpty(certificate)) {
			UIHelper.showToast("银行卡开卡证件号码不能为空");
			return false;
		}
		
		if(isBind && TextUtils.isEmpty(verification_code)) {
			UIHelper.showToast("短信验证码不能为空");
			return false;
		}
		
		return true;
	}

	/**
	 *  定义一个倒计时的内部类 
	 */
	class TimeCount extends CountDownTimer {
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
		}
		
		@Override
		public void onFinish() {
			//计时完毕时触发
			verification_code_btn.setText("立即获取");
			verification_code_btn.setClickable(true);
		}
	
		@Override
		public void onTick(long millisUntilFinished){
			//计时过程显示
			verification_code_btn.setClickable(false);
			verification_code_btn.setText(millisUntilFinished / 1000 + "秒");
		}
	}
}
