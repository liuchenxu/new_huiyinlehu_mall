package com.huiyin.ui.moments.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.huiyin.R;
import com.huiyin.utils.ImageManager;

public class MomentsPictureGridViewAdapter extends BaseAdapter {
	private ArrayList<String> mImagePathList;
	private Context mContext;

	public MomentsPictureGridViewAdapter(Context context) {
		this.mContext = context;
		mImagePathList = new ArrayList<String>();
	}

	@Override
	public int getCount() {
		return null != mImagePathList ? mImagePathList.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return null != mImagePathList.get(position) ? mImagePathList.get(position) : null; 
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if(convertView == null) {
		   viewHolder = new ViewHolder();
		   convertView = View.inflate(mContext, R.layout.add_iamge_item, null);
		   viewHolder.mContentImg = (ImageView) convertView.findViewById(R.id.iv_add_image_item);
		   viewHolder.mDeleteImg =  (ImageView) convertView.findViewById(R.id.iv_delete_jianhao);
		   convertView.setTag(viewHolder);
		}
		else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		if (mImagePathList != null & mImagePathList.size() > 0) {
			
			ImageManager.Load("file://"+mImagePathList.get(position), viewHolder.mContentImg);
			
			// 点击删除上传的图片
			viewHolder.mDeleteImg.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {

					mImagePathList.remove(position);
					notifyDataSetChanged();
				}
			});

		}

		return convertView;
	}
	
	static class ViewHolder {
		ImageView mContentImg;
		ImageView mDeleteImg;
	}


	/**
	 * 添加上传图片的路径
	 * 
	 */
	public void addImagePath(String path) {
		mImagePathList.add(path);
		notifyDataSetChanged();
	}
	
	public ArrayList<String> getImagePathList() {
		return mImagePathList;
	}

	/**
	 * 获取上传图片的路径集合
	 * 
	 */
	public String getImagePath() {
		if (mImagePathList.size() == 0) {

			return "";
		}
		String imagePath = "";
		for (int i = 0; i < mImagePathList.size(); i++) {

			if (i == mImagePathList.size() - 1) {

				imagePath += mImagePathList.get(i);
				break;
			}
			imagePath += mImagePathList.get(i) + ",";
		}

		return imagePath;
	}

}
