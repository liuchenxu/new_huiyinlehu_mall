package com.huiyin.ui.user.order;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.adapter.ReturnRecordAdapter;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.base.BaseFragment;
import com.huiyin.bean.BespeakReturnBean;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshListView;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase.Mode;
import com.huiyin.newpackage.base.NewBaseFragment;
import com.huiyin.newpackage.consts.HTTPConsts;
import com.huiyin.newpackage.entity.ApplyInfoBean;
import com.huiyin.newpackage.entity.OrderInfo;
import com.huiyin.newpackage.event.OrderProcessQueryEvent;
import com.huiyin.newpackage.ui.adapter.ProcessGoodsItemAdapter;
import com.huiyin.utils.LogUtil;
import com.huiyin.wight.XListView;

/**
 * 查看退换纪录
 * 
 */
public class ReturnRecordFragment extends NewBaseFragment {

	private static final String TAG = "BespeakRecordFragment";
	public static final String DELETE_ORDERID = "deleteId";// 删除订单id
	public static final int ORDER_DELETE = 1;// 删除订单

	/** 退货，查看详情 **/
	public static final int REQUEST_CODE_RETURN = 2;

	private PullToRefreshListView xListView;

	private LinearLayout layout_empty;// 提示数据为空布局
	private LinearLayout layout_search;

	private ProcessGoodsItemAdapter adapter;

	private BespeakReturnBean orderListBean;

	private int pageIndex = 1;// 当前页

	private View layoutView;

	private boolean refresh = true;

	private int page = 1;

	private ArrayList<ApplyInfoBean> appliedOrders = new ArrayList<ApplyInfoBean>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layoutView = inflater.inflate(R.layout.layout_return_record_fragment, null);
		findView();
		return layoutView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		register = true;
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		showProgress(true);
		getAppliedServiceOrders();
	}

	public void findView() {
		this.xListView = (PullToRefreshListView) layoutView.findViewById(R.id.lv_order);
		xListView.setMode(Mode.BOTH);
		xListView.getLoadingLayoutProxy(false, true).setPullLabel("上拉加载更多");
		xListView.getLoadingLayoutProxy(false, true).setReleaseLabel("松开加载");
		xListView.getLoadingLayoutProxy(false, true).setRefreshingLabel(
				"正在加载。。");
		
		xListView.getLoadingLayoutProxy(true, false).setPullLabel("下拉刷新数据");
		xListView.getLoadingLayoutProxy(true, false).setReleaseLabel("松开刷新");
		xListView.getLoadingLayoutProxy(true, false).setRefreshingLabel(
				"正在刷新。。");
		this.layout_search = (LinearLayout) layoutView.findViewById(R.id.layout_search);
		this.layout_empty = (LinearLayout) layoutView.findViewById(R.id.layout_empty);
		layout_search.setVisibility(View.GONE);
		xListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase refreshView) {
				refresh = true;
				appliedOrders.clear();
				getAppliedServiceOrders();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase refreshView) {
				refresh = false;
				getAppliedServiceOrders();
			}
		});
		adapter = new ProcessGoodsItemAdapter(mContext, appliedOrders);
		xListView.setAdapter(adapter);
	}

	private void getAppliedServiceOrders() {
		int next = page;
		if (refresh)
			next = 1;
		OrderProcessQueryEvent.QueryOrderProcessList(client, next);
	}

	/**
	 * 获取进度查询列表数据
	 * 
	 * @param event
	 */
	public void onEvent(OrderProcessQueryEvent event) {
		finishProgress();
		if (refresh) { // 刷新，则清空，页码置为1
			appliedOrders.clear();
			page = 1;
		}
		if (event.type == HTTPConsts.HTTP_OK) {
			if (event.getList() != null && event.getList().size() >= 0) {
				if((event.getTotalPageNum()>=event.getPageIndex())){
					page++;
					ArrayList<ApplyInfoBean> values = event.getList();
					appliedOrders.addAll(values);
					adapter.refreshData(appliedOrders);
				}
			} else {
				layout_empty.setVisibility(View.VISIBLE);
			}
		} else {
			String message = event.msg;
			if (message != null) {
				showToast(message);
			}
		}
		refreshComplete(xListView);
	}
}