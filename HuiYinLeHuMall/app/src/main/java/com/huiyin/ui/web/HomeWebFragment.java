package com.huiyin.ui.web;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.huiyin.R;
import com.huiyin.api.URLs;
import com.huiyin.base.BaseFragment;

/**
 * 首页Html5版本
 * 
 * @author zhyao
 * 
 */
public class HomeWebFragment extends BaseFragment {

	private CustomWebView mCustomWebView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layoutView = inflater.inflate(R.layout.fragment_home_web, null);
		return layoutView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		loaddata();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
	}
	
	@Override
	public void onStop() {
		super.onStop();
		
	}
	
	@Override
	public void findView() {
		super.findView();
		
		mCustomWebView = (CustomWebView) findViewById(R.id.webview);
		mCustomWebView.set404Page(false);
		
	}

	private void loaddata() {
		try {
			mCustomWebView.loadUrl(URLs.home_web);
//			mCustomWebView.loadUrl("http://192.168.19.22:8082/html5/app/zhongqiu/index.html");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
