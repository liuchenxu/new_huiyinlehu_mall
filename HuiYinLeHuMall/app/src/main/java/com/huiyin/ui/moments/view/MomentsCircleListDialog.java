package com.huiyin.ui.moments.view;


import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

import com.huiyin.R;
import com.huiyin.bean.MomentsCircleItem;
import com.huiyin.ui.moments.adapter.MomentsCircleAdapter;
import com.huiyin.utils.DensityUtil;

/**
 * 选择圈子列表框
 * 
 * @author Administrator
 *
 */
public class MomentsCircleListDialog extends Dialog implements OnItemClickListener, TextWatcher{

	private Context mContext;
	
	/**搜索框**/
	private EditText mSearchEdt;
	
	/**圈子列表**/
	private ListView mCircleList;
	
	/**圈子列表adapter**/
	private MomentsCircleAdapter mCircleAdapter;
	
	/**选择监听器**/
	private SelectCircleListener mSelectCircleListener;
	
	private ArrayList<MomentsCircleItem> listDatas;
	
	/**过滤之后的list**/
	private ArrayList<MomentsCircleItem> filterListDatas;
	
    /** 上次选择的圈子ID **/
	private String selectCircleId;
	
	public MomentsCircleListDialog(Context context, ArrayList<MomentsCircleItem> listDatas, String selectCircleId, SelectCircleListener selectCircleListener) {
		super(context, R.style.dialog);
		
		mContext = context;
		this.listDatas = listDatas;
		this.selectCircleId = selectCircleId;
		mSelectCircleListener = selectCircleListener;
		
		filterListDatas = new ArrayList<MomentsCircleItem>();
		filterListDatas.addAll(listDatas);
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_circle_list, null);
		setContentView(view);
		
		initView(view);
		
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.width = DensityUtil.getScreenWidth(((Activity) mContext)); //设置宽度
		lp.height = DensityUtil.getScreenHeight(((Activity) mContext)) - DensityUtil.dip2px(mContext, 46) - DensityUtil.getStatusBarHeight(((Activity) mContext)); //设置宽度
		lp.gravity = Gravity.BOTTOM;
		getWindow().setAttributes(lp);
	}

	private void initView(View view) {
		
		mSearchEdt = (EditText) view.findViewById(R.id.edt_search);
		mCircleList = (ListView) view.findViewById(R.id.lv_circle);
		 
		mSearchEdt.addTextChangedListener(this);
		mCircleList.setOnItemClickListener(this);
		
		setAdapter();
	}
	
	private void setAdapter() {
		getPosition(listDatas, selectCircleId);
		mCircleAdapter = new MomentsCircleAdapter(mContext, listDatas, selectCircleId);
		mCircleList.setAdapter(mCircleAdapter);
	}
	
	/**
	 * 根据id查找对应的位置
	 * @param listDatas
	 * @param selectCircleId
	 * @return
	 */
	private int getPosition(ArrayList<MomentsCircleItem> listDatas, String selectCircleId) {
		for (int i = 0; i < listDatas.size(); i++) {
			if(selectCircleId.equals(listDatas.get(i).getID())) {
				return i;
			}
		}
		return 0;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		dismiss();
		mCircleAdapter.setSelectCircleId(filterListDatas.get(position).getID());
		if(mSelectCircleListener != null) {
			mSelectCircleListener.onSelect(filterListDatas.get(position));
		}
	}
	
	public interface SelectCircleListener {
		void onSelect(MomentsCircleItem circleItem);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		filterListDatas.clear();
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if(!TextUtils.isEmpty(s)) {
			for (MomentsCircleItem circleItem : listDatas) {
				if (!TextUtils.isEmpty(circleItem.getNAME()) && circleItem.getNAME().contains(s)) {
					filterListDatas.add(circleItem);
				}
			}
			mCircleAdapter.setListDatas(filterListDatas);
		}
		else {
			filterListDatas.addAll(listDatas);
			mCircleAdapter.setListDatas(filterListDatas);
		}
	}

	@Override
	public void afterTextChanged(Editable s) {
		
	}
}
