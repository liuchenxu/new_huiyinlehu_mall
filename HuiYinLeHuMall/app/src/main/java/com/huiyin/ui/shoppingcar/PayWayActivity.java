package com.huiyin.ui.shoppingcar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.adapter.NearBoxListAdapter;
import com.huiyin.adapter.NearShopListAdapter;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.base.BaseActivity;
import com.huiyin.bean.NearBoxBean;
import com.huiyin.bean.NearBoxBean.DevicesItem;
import com.huiyin.bean.NearBoxBean.NearByItem;
import com.huiyin.bean.NearShopBean;
import com.huiyin.bean.NearShopBean.ShopMention;
import com.huiyin.db.SQLOpearteImpl;
import com.huiyin.ui.fillorder.FillOrderActivity;
import com.huiyin.ui.shoppingcar.DatePickPopMenu.ConfirmClick;
import com.huiyin.utils.LocationUtil;
import com.huiyin.utils.LogUtil;
import com.huiyin.utils.MyCustomResponseHandler;
import com.huiyin.utils.StringUtils;
import com.orhanobut.logger.Logger;

/**
 * 支付及配送方式
 *
 * @author xiaojianfeng
 * @todo TODO
 * @date 2015-7-30
 */
public class PayWayActivity extends BaseActivity {
	private final static String TAG = "PayWayActivity";
    public static String DEFAULT_PAY = "在线支付";             //默认支付方式
    public static String DEFAULT_DELIVER = "送货/服务上门";     //默认快递方式
    public static String DOOR_DELIVER = "上门自提";     //上门自提
    public static String BOX_DELIVER = "储物柜自提";     //箱子自提
    public static final int DELIVER_DOOR = 0;//上门自提
    public static final int DELIVER_BOX = 1;//箱子自提
    TextView left_rb, middle_title_tv, right_rb, payway_shop;
    GridView spin_pay_way, spin_send_way;
    EditText spin_member_num;
    TextView spin_send_time;
    PayWayBean mPayWayBean;
    Button payway_btn, payway_select_time;
    List<String> listPay;
    List<String> listSend;
    String payWayId, sendWayId, sendtime, member_num, pay, send; // sendWayId: 1、上门自提  2、送货/服务上门   4、箱子自提
    int shopId;
    private Context mContext;
    /**
     * 经度
     */
    private double lng = 0.0;
    /**
     * 纬度
     */
    private double lat = 0.0;
    private String cityName;
    private int cityId;
    private String addressDetail;
    private NearShopBean bean;
	//add by zhyao @2015/7/25 add by zhyao 附件箱子bean
	private NearBoxBean mNearBoxBean;
    private MyAdapter mMyAdapter, mMyAdapterSend;
    private String deliveryTime;
    private AlertDialog mDialog;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.payway_layout1);
        mContext = this;
        Intent i = getIntent();
        sendWayId = i.getStringExtra("sendWayId");
        // add by zhyao @2015/7/25 添加配送方式
		send = i.getStringExtra("send");
        shopId = i.getIntExtra("shopId", -1);
        addressDetail = i.getStringExtra("addressDetail");
        deliveryTime = i.getStringExtra("deliveryTime");
        // add by zhyao @2015/7/25 send为空默认为送货/服务上门
        if(TextUtils.isEmpty(send)) {
        	send = DEFAULT_DELIVER;
        }
        initView();
        refleshUI();
    }

    private void initView() {
        left_rb = (TextView) findViewById(R.id.left_ib);
        right_rb = (TextView) findViewById(R.id.right_ib);
        right_rb.setVisibility(View.GONE);
        left_rb.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
        middle_title_tv = (TextView) findViewById(R.id.middle_title_tv);
        middle_title_tv.setText("支付及配送方式");
        payway_shop = (TextView) findViewById(R.id.payway_shop);
        // add by zhyao @2015/7/25 显示配送方式
        if(DEFAULT_DELIVER.equals(send)) {
        	payway_shop.setText("");
        }
        else {
        	payway_shop.setText(send);
        }
        spin_pay_way = (GridView) findViewById(R.id.spin_pay_way);
        spin_send_way = (GridView) findViewById(R.id.spin_send_way);
        spin_send_time = (TextView) findViewById(R.id.spin_send_time);
        payway_select_time = (Button) findViewById(R.id.payway_select_time);
        payway_select_time.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DatePickPopMenu menu = new DatePickPopMenu(mContext);
                menu.setmClickListener(new ConfirmClick() {

                    @Override
                    public void OnConfirmClick(String temp) {
                        spin_send_time.setText(temp);
                    }
                });
                menu.showAtLocation(spin_send_time, Gravity.BOTTOM, 0, 0);
            }
        });
        spin_send_time.setText(deliveryTime);
        spin_member_num = (EditText) findViewById(R.id.spin_member_num);

        payway_btn = (Button) findViewById(R.id.payway_btn);
        payway_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                sendtime = spin_send_time.getText().toString();
                if (sendtime.equals("")) {
                    Toast.makeText(PayWayActivity.this, "送货时间不能为空",
                            Toast.LENGTH_LONG).show();
                    return;
                }
                Intent i = new Intent(PayWayActivity.this, FillOrderActivity.class);
                i.putExtra("payWayId", payWayId);
                i.putExtra("pay", pay);
                i.putExtra("sendWayId", sendWayId);
                i.putExtra("send", send);
                i.putExtra("sendtime", sendtime);
                i.putExtra("shopId", shopId);
                if (spin_member_num != null && spin_member_num.getText() != null) {
                    i.putExtra("staffId", spin_member_num.getText().toString().trim());
                }
                setResult(RESULT_OK, i);
                finish();
            }
        });
    }

    private void initData() {
        RequstClient.doPayWay(new CustomResponseHandler(this) {
            @Override
            public void onSuccess(int statusCode, Header[] headers,
                                  String content) {
                super.onSuccess(statusCode, headers, content);
                LogUtil.i(TAG, "doPayWay:" + content);
                try {
                    JSONObject obj = new JSONObject(content);
                    if (!obj.getString("type").equals("1")) {
                        String errorMsg = obj.getString("msg");
                        Toast.makeText(getBaseContext(), errorMsg,
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mPayWayBean = new Gson()
                            .fromJson(content, PayWayBean.class);
                    refleshUI();

                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 获得数据后刷新UI
     */
    private void refleshUI() {
//		for (int i = 0; i < mPayWayBean.payMethodList.size(); i++) {
//			listPay.add(mPayWayBean.payMethodList.get(i).METHOD_NAME);
//		}
        listPay = new ArrayList<String>();
        listPay.add(DEFAULT_PAY);
        mMyAdapter = new MyAdapter(this, listPay, DEFAULT_PAY);
        spin_pay_way.setAdapter(mMyAdapter);
        spin_pay_way.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,
                                    int i, long l) {
                mMyAdapter.setSelect(listPay.get(i));
                pay = listPay.get(i);
                mMyAdapter.notifyDataSetChanged();
            }
        });

		// add by zhyao @2015/7/25 添加箱子自提
        listSend = new ArrayList<String>();
        listSend.add(DEFAULT_DELIVER);
        listSend.add(DOOR_DELIVER);
        //listSend.add(BOX_DELIVER);//暂时隐藏速易递箱子自提
        
        String selectSendWay;
        //上门自提
        if("1".equals(sendWayId)){
        	selectSendWay = DOOR_DELIVER;
        }
        //送货/服务上门
        else if("2".equals(sendWayId)){
        	selectSendWay = DEFAULT_DELIVER;
        }
        //箱子自提
        else if("4".equals(sendWayId)){
        	selectSendWay = BOX_DELIVER;
        }
        else {
        	selectSendWay = DEFAULT_DELIVER;
        }
        
		mMyAdapterSend = new MyAdapter(this, listSend, selectSendWay);
		spin_send_way.setAdapter(mMyAdapterSend);
		spin_send_way.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int arg2, long l) {
				mMyAdapterSend.setSelect(listSend.get(arg2));
				mMyAdapterSend.notifyDataSetChanged();
				// add by zhyao @2015/7/25 添加箱子自提判断
				// 服务上门
				//if (arg2 != listSend.size() - 1) {
				if (arg2 == 0) {
					sendWayId = "2";
					send = listSend.get(arg2);
                    payway_shop.setText("");
				}
				// 上门自提
				else if (arg2 == 1){
					getNearShopByAddress(DELIVER_DOOR);
					//startLoacation(DELIVER_DOOR);
				}
				// 箱子自提
				else {
					getNearShopByAddress(DELIVER_BOX);
					//startLoacation(DELIVER_BOX);
				}
			}
		});
	}

    private void getNearShopByAddress(int way) {
        SQLOpearteImpl temp = new SQLOpearteImpl(mContext);
        cityId = temp.checkIdByAddress(addressDetail);
        temp.CloseDB();

        if (cityId == -1) {
            Toast.makeText(mContext, "通过地址解析失败，尝试定位!", Toast.LENGTH_SHORT).show();
            startLoacation(way);
        } else {
        	if(way == DELIVER_DOOR){
				getNearShop();
			}
			else if((way == DELIVER_BOX)){
				getNearBox();
			}
        }
    }

    /**
     * 获取定位信息
     */
    private void startLoacation(final int way) {
    	
    	HashMap<String, String> locationMap = LocationUtil.getCurrentLocation(mContext);
        cityName = locationMap.get("cityName");
        SQLOpearteImpl temp = new SQLOpearteImpl(mContext);
        cityId = temp.checkIdByName(cityName);
        if(way == DELIVER_DOOR){
			getNearShop();
		}
		else if((way == DELIVER_BOX)){
			getNearBox();
		}
        temp.CloseDB();
        
    }

    private void getNearShop() {
        if (bean != null && bean.type > 0) {
            showListDialog();
            return;
        }
        if (cityId == -1)
            return;
        MyCustomResponseHandler handler = new MyCustomResponseHandler(mContext, true) {

            @Override
            public void onRefreshData(String content) {
                super.onRefreshData(content);
                if (isSuccessResponse(content)) {
                    bean = NearShopBean.explainJson(content, mContext);
                    if (bean.type > 0) {
                        showListDialog();
                    }
                }
            }
        };
        RequstClient.appNearShop(cityId, lng, lat, AppContext.getInstance().getRegionId(), handler);
    }
    
    // add by zhyao @2015/7/25 添加获取附件箱子接口
 	private void getNearBox() {
 		if (mNearBoxBean != null && mNearBoxBean.type > 0) {
 			showNearBoxListDialog();
 			return;
 		}
 		if (cityId == -1)
 			return;
 		MyCustomResponseHandler handler = new MyCustomResponseHandler(mContext,
 				true) {

 			@Override
 			public void onRefreshData(String content) {
 				super.onRefreshData(content);
 				Logger.json(content);
 				mNearBoxBean = NearBoxBean.explainJson(content, mContext);
 				if (mNearBoxBean.type > 0) {
 					showNearBoxListDialog();
 				}
 				else {
 					UIHelper.showToast(mNearBoxBean.msg);
 				}
 			}
 		};
 		RequstClient.appNearBox(cityId, lng, lat, handler);
// 		RequstClient.appNearBox(2724, 114.070908, 22.546162, handler);
 	}
    
 // modify by zhyao @2015/67/6 添加自提地址筛选功能
    private void showListDialog() {
        final ArrayList<ShopMention> nearbyList = bean.nearbyList;

        if (nearbyList == null || nearbyList.size() <= 0) {
            Toast.makeText(mContext, "该区域没有门店", Toast.LENGTH_SHORT).show();
            mMyAdapterSend.setSelect(listSend.get(0));
            mMyAdapterSend.notifyDataSetChanged();
            send = listSend.get(0);
            payway_shop.setText("");
            return;
        }
        final ArrayList<ShopMention> searchNearByList = new ArrayList<ShopMention>();

        View mView = LayoutInflater.from(mContext).inflate(
                R.layout.dialog_listview, null);
        EditText mSearchEdit = (EditText) mView.findViewById(R.id.ed_search);

        ListView mListView = (ListView) mView.findViewById(R.id.listView);
        final NearShopListAdapter mAdapter = new NearShopListAdapter(mContext);
        mAdapter.setNeedSort(lng != 0.0 && lat != 0.0 && !StringUtils.isBlank(cityName));
        mAdapter.addItem(nearbyList);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
            	// modify by zhyao @2015/7/25 修改上门自提 
				sendWayId = "1";
                send = "上门自提("
                        + ((NearShopListAdapter) parent.getAdapter())
                        .getShopName(position) + ")";
                shopId = ((NearShopListAdapter) parent.getAdapter())
                        .getShopId(position);
                payway_shop.setText(send);
                mDialog.dismiss();
            }
        });
        mSearchEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    for (ShopMention shopMention : nearbyList) {
                        if (!TextUtils.isEmpty(shopMention.NAME) && shopMention.NAME.contains(s)) {
                            searchNearByList.add(shopMention);
                        }
                    }
                    mAdapter.deleteItem();
                    mAdapter.addItem(searchNearByList);
                } else {
                    mAdapter.deleteItem();
                    mAdapter.addItem(nearbyList);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                searchNearByList.clear();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mDialog = new AlertDialog.Builder(mContext, 3).setTitle("选择门店")
                .setView(mView)
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mDialog.dismiss();
                        mMyAdapterSend.setSelect(listSend.get(0));
                        mMyAdapterSend.notifyDataSetChanged();
                        //add by zhyao @2015/8/27 设置送货方式
                        sendWayId = "2";
                        send = listSend.get(0);
                        payway_shop.setText("");
                    }
                }).create();

        //这种方式不适用，具体原因不明
//        Window dialogWindow = mDialog.getWindow();
//		WindowManager m = getWindowManager();
//		Display d = m.getDefaultDisplay(); // 获取屏幕宽、高用
//		DisplayMetrics displayMetrics = new DisplayMetrics();
//		d.getMetrics(displayMetrics);
//		WindowManager.LayoutParams lp = dialogWindow.getAttributes(); // 获取对话框当前的参数值
//		lp.height = (int) (displayMetrics.heightPixels * 0.6); // 高度设置为屏幕的0.4
//		lp.width = (int) (displayMetrics.widthPixels * 0.1); // 宽度设置为屏幕的0.7
//		dialogWindow.setAttributes(lp);

        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
    }

 // add by zhyao @2015/7/25 添加附件箱子自提对话框列表
 	private void showNearBoxListDialog() {
 		final ArrayList<NearByItem> nearbyList = mNearBoxBean.nearbyList;
 		
 		final ArrayList<DevicesItem> devicesList = new ArrayList<DevicesItem>();
 		for (NearByItem nearbyItem : nearbyList) {
 			devicesList.addAll(nearbyItem.devices);
 		}
 		
 		final ArrayList<DevicesItem> searchDevicesList = new ArrayList<DevicesItem>();
 		
 		View mView = LayoutInflater.from(mContext).inflate(
 				R.layout.dialog_listview, null);
 		EditText mSearchEdit = (EditText) mView.findViewById(R.id.ed_search);
 		mSearchEdit.setHint("请输入自提柜信息");
 		
 		ListView mListView = (ListView) mView.findViewById(R.id.listView);
 		final NearBoxListAdapter mAdapter = new NearBoxListAdapter(mContext);
 		mAdapter.addItem(devicesList);
 		mListView.setAdapter(mAdapter);
 		mListView.setOnItemClickListener(new OnItemClickListener() {

 			@Override
 			public void onItemClick(AdapterView<?> parent, View view,
 					int position, long id) {
 				sendWayId = "4";
 				send = "储物柜自提("
 						+ ((NearBoxListAdapter) parent.getAdapter())
 								.getBoxName(position) + ")";
 				shopId = ((NearBoxListAdapter) parent.getAdapter())
 						.getBoxId(position);
                 payway_shop.setText(send);
 				mDialog.dismiss();
 			}
 		});
 		mSearchEdit.addTextChangedListener(new TextWatcher() {
 					
 			@Override
 			public void onTextChanged(CharSequence s, int start, int before, int count) {
 				if(!TextUtils.isEmpty(s)) {
 					for (DevicesItem devicesItem : devicesList) {
 						if ((!TextUtils.isEmpty(devicesItem.name) && devicesItem.name.contains(s)) 
 								|| !TextUtils.isEmpty(devicesItem.address) && devicesItem.address.contains(s)) {
 							searchDevicesList.add(devicesItem);
 						}
 					}
 					mAdapter.deleteItem();
 					mAdapter.addItem(searchDevicesList);
 				}
 				else {
 					mAdapter.deleteItem();
 					mAdapter.addItem(devicesList);
 				}
 			}
 			
 			@Override
 			public void beforeTextChanged(CharSequence s, int start, int count,
 					int after) {
 				searchDevicesList.clear();
 			}
 			
 			@Override
 			public void afterTextChanged(Editable s) {
 				
 			}
 		});
 		mDialog = new AlertDialog.Builder(mContext, 3).setTitle("储物柜自提地点")
 				.setView(mView)
 				.setNegativeButton("取消", new DialogInterface.OnClickListener() {

 					@Override
 					public void onClick(DialogInterface dialog, int which) {
 						mDialog.dismiss();
 						mMyAdapterSend.setSelect(listSend.get(0));
 						mMyAdapterSend.notifyDataSetChanged();
 						sendWayId = "2";
 						send = listSend.get(0);
                         payway_shop.setText("");
 					}
 				}).create();
 		mDialog.setCancelable(false);
 		mDialog.setCanceledOnTouchOutside(false);
 		mDialog.show();
 	}

 	public class PayWayBean {

        ArrayList<PayItem> payMethodList = new ArrayList<PayItem>();
        ArrayList<SendItem> distributionManagementList = new ArrayList<SendItem>();

        public class PayItem {
            String PAYMETHODID;
            String METHOD_NAME;
        }

        public class SendItem {
            String DISMANNAME;
            String DISMANID;
        }

    }
 	
 	private class MyAdapter extends BaseAdapter {
        Context context;
        List<String> data;
        String select;

        private MyAdapter(Context context, List<String> data, String select) {
            this.context = context;
            this.data = data;
            setSelect(select);
        }

        public void setData(List<String> data) {
            if (this.data == null) {
                this.data = new ArrayList<String>();
            }
            if (data != null) {
                this.data.clear();
                for (String s : data) {
                    this.data.add(s);
                }
            }
        }

        public void setSelect(String select) {
            this.select = select;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder mHolder;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(
                        R.layout.payway_grid_item, null);
                mHolder = new ViewHolder();
                mHolder.mImageView = (ImageView) view
                        .findViewById(R.id.payway_item_image);
                mHolder.mTextView = (TextView) view
                        .findViewById(R.id.payway_item_text);
                mHolder.mLinearLayout = (RelativeLayout) view
                        .findViewById(R.id.payway_item_bg);
                view.setTag(mHolder);
            } else {
                mHolder = (ViewHolder) view.getTag();
            }
            if (data.get(i).equals(select)) {
                mHolder.mLinearLayout
                        .setBackgroundResource(R.drawable.show_red_bg);
                mHolder.mImageView.setVisibility(View.VISIBLE);
                mHolder.mTextView.setTextColor(getResources().getColor(
                        R.color.red));
            } else {
                mHolder.mLinearLayout
                        .setBackgroundResource(R.drawable.show_search_bg);
                mHolder.mImageView.setVisibility(View.GONE);
                mHolder.mTextView.setTextColor(getResources().getColor(
                        R.color.payway_text_color));
            }
            mHolder.mTextView.setText(data.get(i));
            return view;
        }

        private class ViewHolder {
            public ImageView mImageView;
            public TextView mTextView;
            private RelativeLayout mLinearLayout;
        }
    }
}
