package com.huiyin.ui.LexiangVipCard;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.base.BaseActivity;
import com.huiyin.ui.servicecard.ApplyServiceCard;
import com.huiyin.utils.JSONParseUtils;
import com.huiyin.utils.PreferenceUtil;
import com.huiyin.utils.StringUtils;

/**
 * 绑定乐享VIP会员卡
 * @author zhyao
 *
 */
public class BindLeXiangVipCardActivity extends BaseActivity{
	

	
	private TextView left_ib,middle_title_tv ;
	private EditText card_number;
	private EditText code;
	private TextView get_code_tv;
	private TextView phone;
	
	
	private Button btn_bind;
	private Button notcard_btn;
	
	private Timer mTimer = null;
	private int sec = 60;
	private Toast mToast;
	private Handler mHandler ;
	private final int SHOW_TOSAT = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bind_lexiangvip_card_layout);
		setRightText(View.GONE);
		initView();
		setListener();
		
	}
	
	private void initView(){
		middle_title_tv = (TextView)findViewById(R.id.ab_title);
		phone = (TextView)findViewById(R.id.phone);
		phone.setText(Html.fromHtml("提示:乐享VIP会员卡热线 <u>400-6901-822</u>"));
		phone.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent d_intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:400-6901-822"));
				d_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(d_intent);
				
			}
		});
		middle_title_tv.setText("绑定乐享VIP会员卡");
		left_ib = (TextView)findViewById(R.id.ab_back);
		left_ib.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
					finish();
			}
		});
		
		card_number = (EditText) findViewById(R.id.card_number);
		code = (EditText) findViewById(R.id.code);
		get_code_tv = (TextView) findViewById(R.id.get_code_tv); 
		
		btn_bind= (Button)findViewById(R.id.btn_bind);
		notcard_btn= (Button)findViewById(R.id.notcard_btn);
	}
	
	/**
	 * 获取短信验证码
	 */
	private void getSmsCode() {
		String cardNo = card_number.getText().toString().trim();
		if(!"".equals(cardNo)) {
			RequstClient.getHappyEnjoyCardSMSCode(cardNo, new CustomResponseHandler(this) {
				@Override
				public void onSuccess(int statusCode, String content) {
					super.onSuccess(statusCode, content);
					
					int type = JSONParseUtils.getInt(content, "type");
				    String msg = JSONParseUtils.getString(content, "msg");
				    showMyToast(msg);
					if(type == 1) {
						showResendTip();
					}
				}
			});
		}
		else {
			showToast("请输入卡号");
		}
		
	}
	
	private void initData(){
		String cardText = card_number.getText().toString();
		String codeText = code.getText().toString();
		RequstClient.bindHappyEnjoyCard(AppContext.getInstance().userId, cardText, codeText, new CustomResponseHandler(this) {
			@Override
			public void onSuccess(int statusCode, String content) {
				super.onSuccess(statusCode, content);
				
			    int type = JSONParseUtils.getInt(content, "type");
			    String msg = JSONParseUtils.getString(content, "msg");
			    showMyToast(msg);
				if(type == 1) {
					BindLeXiangVipCardActivity.this.finish();
				}
				
			}
		});
	}
	
	private void setListener(){
		
		get_code_tv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getSmsCode();
			}
		});
		
		btn_bind.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(checkInfo()){
					initData();
				}
			}
		});
		
		notcard_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(BindLeXiangVipCardActivity.this,ApplyServiceCard.class));
			}
		});
		 mHandler = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					super.handleMessage(msg);
					switch (msg.what) {
					case SHOW_TOSAT: {
						String message = (String) msg.obj;
						showToast(message);
					}
						break;
					}
				}
			};
	}
	
	/**
	 * 验证信息
	 * @return
	 */
	private boolean checkInfo() {
		Message msg = mHandler.obtainMessage();
		msg.what = SHOW_TOSAT;
		
		String cardText = card_number.getText().toString();
		String codeText = code.getText().toString();
		
		if (TextUtils.isEmpty(cardText)) {
			msg.obj = getString(R.string.card_is_null);
			mHandler.sendMessage(msg);
			return false;
		} else if (StringUtils.isEmpty(codeText)) {
			msg.obj = getString(R.string.ver_code_is_null);
			mHandler.sendMessage(msg);
			return false;
		}
		return true;
	}
	
	
	
	private void showToast(String msg) {
		if (mToast == null) {
			mToast = Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT) ;
		}
		mToast.setText(msg);
		mToast.show();
	}
	
	private void showResendTip() {
		mTimer = new Timer();
		get_code_tv.setEnabled(false);
		mTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				sec--;
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						if (sec < 1) {
							get_code_tv.setEnabled(true);
							get_code_tv.setText("重新获取");
							sec = 60;
							mTimer.cancel();
						} else {
							String tipMsg = String.format(getString(R.string.time_tip), sec);
							get_code_tv.setText(tipMsg);
						}
					}
				});
			}
		}, 0, 1000);
	}
	


	
}
