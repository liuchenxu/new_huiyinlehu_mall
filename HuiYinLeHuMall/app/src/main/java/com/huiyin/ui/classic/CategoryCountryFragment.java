package com.huiyin.ui.classic;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.huiyin.R;
import com.huiyin.adapter.CategoryCountryAdapter;
import com.huiyin.bean.CategoryAndBrandBean.Category;
import com.huiyin.bean.CategoryAndBrandBean.NationalPavilion;
/**
 * 分类-分类fragment
 * @author zhyao
 *
 */
public class CategoryCountryFragment extends Fragment {
	
	private static final String TAG = "CategoryCountryFragment";
	
	private View rootView;
	
	private ListView mCountryList;
	
	private CategoryCountryAdapter mAdapter;
	
	private ArrayList<NationalPavilion> mCountrys;
	
	public static CategoryCountryFragment newInstance(ArrayList<NationalPavilion> countrys) {
		CategoryCountryFragment instance = new CategoryCountryFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable("data", countrys);
		instance.setArguments(bundle);
		return instance;
	}
	
	@Override
	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate");
		
		mCountrys = (ArrayList<NationalPavilion>) getArguments().getSerializable("data");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		rootView = inflater.inflate(R.layout.fragment_category_country, null);
		initView();
		setData(mCountrys);
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.i(TAG, "onResume");

	}

	private void initView() {
		mCountryList = (ListView) rootView.findViewById(R.id.lv_category_country);
	}
	
	public void setData(ArrayList<NationalPavilion> countrys) {
		//if(mAdapter == null) {
			mAdapter = new CategoryCountryAdapter(getActivity(), countrys);
			mCountryList.setAdapter(mAdapter);
		//}
		
	}

}
