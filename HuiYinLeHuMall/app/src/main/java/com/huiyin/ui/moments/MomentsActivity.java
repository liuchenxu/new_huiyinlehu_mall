package com.huiyin.ui.moments;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.base.BaseActivity;
import com.huiyin.utils.AppManager;
import com.huiyin.utils.DialogUtil;
import com.huiyin.wight.MyViewPager;
import com.vCamera.ui.record.MediaRecorderActivity;

/**
 * 新秀场（圈子）
 * 
 * @author zhyao
 * 
 */
public class MomentsActivity extends BaseActivity implements OnClickListener, OnPageChangeListener,
		OnCheckedChangeListener {

	private static final String TAG = "MomentsActivity";

	private View v;

	/** 返回 **/
	private TextView mBackTv;

	/** 秀一秀 **/
	private TextView mShowTv;

	/** 我的和收藏 **/
	private RadioGroup mShowGroup;

	/** viewpager **/
	private MyViewPager mViewpager;

	/** 设配器 **/
	private MyAdapter mAdapter;

	/** fragment列表 **/
	private List<Fragment> mFragmentList;

	/** 我的fragment **/
	private MomentsMyFragment mMomentsMyFragment;

	/** 秀场fragment **/
	private MomentsFavoriteFragment mMomentsFavoriteFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.fragment_moments);
		initView();
		initFragment();
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if(mMomentsMyFragment != null) {
			mMomentsMyFragment.onRefreshData();
		}
		if(mMomentsFavoriteFragment != null) {
			mMomentsFavoriteFragment.onRefreshData();
		}
	}
	
	/**
	 * 初始化控件
	 */
	private void initView() {

		mBackTv = (TextView) findViewById(R.id.tv_back);
		mBackTv.setOnClickListener(this);
		
		mShowTv = (TextView) findViewById(R.id.tv_show);
		mShowTv.setOnClickListener(this);

		mShowGroup = (RadioGroup) findViewById(R.id.rg_show);
		mShowGroup.setOnCheckedChangeListener(this);

		mViewpager = (MyViewPager) findViewById(R.id.viewpager);
		mViewpager.setOnPageChangeListener(this);
	}

	/**
	 * 初始化fragment
	 */
	private void initFragment() {
		mMomentsMyFragment = new MomentsMyFragment();
		mMomentsFavoriteFragment = new MomentsFavoriteFragment();

		mFragmentList = new ArrayList<Fragment>();
		mFragmentList.add(mMomentsMyFragment);
		mFragmentList.add(mMomentsFavoriteFragment);

		mAdapter = new MyAdapter(getSupportFragmentManager());
		mViewpager.setAdapter(mAdapter);

	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		Log.d(TAG, "onCheckedChanged : checkedId = " + checkedId);
		switch (checkedId) {
		// 我的
		case R.id.rd_my:
			mViewpager.setCurrentItem(0);
			break;
		// 收藏
		case R.id.rd_favorite:
			mViewpager.setCurrentItem(1);
			break;
		// 秀一秀
		case R.id.tv_show:
			DialogUtil.showSelectMomentsMethodDialog(this, new OnClickListener() {

				@Override
				public void onClick(View v) {

					switch (v.getId()) {
					case R.id.btn_show_goods:
						startActivity(new Intent(MomentsActivity.this, MomentsSelectGoodsActivity.class));
						break;
					case R.id.btn_show_camera:
						startActivity(new Intent(MomentsActivity.this, MomentsShowPictureActivity.class));
						break;
					case R.id.btn_show_video:
						startActivity(new Intent(MomentsActivity.this, MediaRecorderActivity.class));
						break;

					default:
						break;
					}

				}
			});
			break;

		default:
			break;
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		// 返回
		case R.id.tv_back:
			AppManager.getAppManager().finishActivity();
			break;

		// 秀一秀
		case R.id.tv_show:
			DialogUtil.showSelectMomentsMethodDialog(this, new OnClickListener() {

				@Override
				public void onClick(View v) {

					switch (v.getId()) {
					case R.id.btn_show_goods:
						startActivity(new Intent(MomentsActivity.this, MomentsSelectGoodsActivity.class));
						break;
					case R.id.btn_show_camera:
						startActivity(new Intent(MomentsActivity.this, MomentsShowPictureActivity.class));
						break;
					case R.id.btn_show_video:
						break;

					default:
						break;
					}

				}
			});
			break;
		default:
			break;
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int arg0) {
		// 我的
		if (arg0 == 0) {
			mShowGroup.check(R.id.rd_my);
			;
		}
		//
		else if (arg0 == 1) {
			mShowGroup.check(R.id.rd_favorite);
			;
		}
	}

	public ViewPager getViewPager() {
		return mViewpager;
	}

	class MyAdapter extends FragmentStatePagerAdapter {

		public MyAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int arg0) {
			return mFragmentList == null ? null : mFragmentList.get(arg0);
		}

		@Override
		public int getCount() {
			return mFragmentList == null ? 0 : mFragmentList.size();
		}

	}

}
