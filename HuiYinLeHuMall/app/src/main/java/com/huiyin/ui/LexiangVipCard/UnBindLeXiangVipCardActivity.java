package com.huiyin.ui.LexiangVipCard;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.utils.JSONParseUtils;

/**
 * 解绑/续费 乐享VIP会员卡 
 * @author Administrator
 *
 */
public class UnBindLeXiangVipCardActivity extends Activity implements OnClickListener{
	
	private static final String TAG = UnBindLeXiangVipCardActivity.class.getName();
	
	/**
	 * 返回按钮
	 */
	private TextView mBackTv;
	
	/**
	 * 标题
	 */
	private TextView mTitleTv;
	
	/**
	 * 乐享VIP会员卡号
	 */
	private TextView mVipCardNoTv;
	
	/**
	 * 乐享VIP会员卡有效期
	 */
	private TextView mVipCardValidityTv;
	
	/**
	 * 积分
	 */
	private TextView mIntegralTv;
	
	/**
	 * 充值
	 */
	private Button mRechargeBtn;
	
	/**
	 * 解除绑定
	 */
	private Button mUnbindBtn;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.unbind_lexiangvip_card_layout);
		
		initView();
		initData();
	}

	private void initView() {
		mBackTv = (TextView) findViewById(R.id.left_ib);
		mTitleTv = (TextView) findViewById(R.id.middle_title_tv);
		mVipCardNoTv = (TextView) findViewById(R.id.vip_card_no_tv);
		mVipCardValidityTv = (TextView) findViewById(R.id.vip_card_validity_tv);
		mIntegralTv = (TextView) findViewById(R.id.integral_tv);
		mRechargeBtn = (Button) findViewById(R.id.btn_recharge);
		mUnbindBtn = (Button) findViewById(R.id.btn_unbind);
		
		mTitleTv.setText("乐享VIP会员卡");
		
		mBackTv.setOnClickListener(this);
		mRechargeBtn.setOnClickListener(this);
		mUnbindBtn.setOnClickListener(this);
	}

	private void initData() {
		CustomResponseHandler mHandler = new CustomResponseHandler(this, true) {
			@Override
			public void onRefreshData(String content) {
				super.onRefreshData(content);
				
				int type = JSONParseUtils.getInt(content, "type");
				if(type == 1) {
					String integral = JSONParseUtils.getString(content, "integral");
					String vipInfoObj = JSONParseUtils.getJSONObject(content, "vipInfo");
					String cardNo = JSONParseUtils.getString(vipInfoObj, "CARD_NO");
					String validDate = JSONParseUtils.getString(vipInfoObj, "VALID_DATE");
					refreshCardInfo(integral, cardNo, validDate);
				}
				
			}
		};
		RequstClient.getHappyEnjoyCardDetail(AppContext.getInstance().userId, mHandler);
	}
	
	/**
	 * 刷新VIP卡信息
	 * @param integral
	 * @param cardNo
	 * @param validDate
	 */
	private void refreshCardInfo(String integral, String cardNo, String validDate) {
		mIntegralTv.setText("积分: " + integral);
		mVipCardNoTv.setText(cardNo);
		mVipCardValidityTv.setText(validDate);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		//返回
		case R.id.left_ib:
			finish();
			break;
		//充值
		case R.id.btn_recharge:
			UIHelper.showToast("此功能正在升级中，尽情期待...");
			break;
		//解除绑定
		case R.id.btn_unbind:
			showUnBindConfirmDialog();
			break;

		default:
			break;
		}
	}

	/**
	 * 解除绑定确认对话框
	 */
	private void showUnBindConfirmDialog() {

		View mView = LayoutInflater.from(this).inflate(R.layout.service_card_dialog, null);
		final Dialog mDialog = new Dialog(this, R.style.dialog);
		EditText mPwdEdt = (EditText) mView.findViewById(R.id.com_message_tv);
		mPwdEdt.setVisibility(View.GONE);
		Button mYesBtn = (Button) mView.findViewById(R.id.com_ok_btn);
		Button mCancelBtn = (Button) mView.findViewById(R.id.com_cancel_btn);
		mCancelBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});
		mYesBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
				unbindLeXiangVipCard();
			}
		});
		mDialog.setContentView(mView);
		mDialog.setCanceledOnTouchOutside(true);
		mDialog.setCancelable(true);
		mDialog.show();
	}
	
	/**
	 * 解除绑定
	 */
	private void unbindLeXiangVipCard() {
		CustomResponseHandler mHandler = new CustomResponseHandler(this, true) {
			@Override
			public void onRefreshData(String content) {
				super.onRefreshData(content);
				
				int type = JSONParseUtils.getInt(content, "type");
				String msg = JSONParseUtils.getString(content, "msg");
				UIHelper.showToast(msg);
				if(type == 1) {
					finish();
				}
				
			}
		};
		RequstClient.unBindHappyEnjoyCard(AppContext.getInstance().userId, mHandler);
	}
	

}
