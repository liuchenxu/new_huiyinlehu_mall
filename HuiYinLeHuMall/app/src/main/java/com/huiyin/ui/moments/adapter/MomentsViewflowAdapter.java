package com.huiyin.ui.moments.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.huiyin.bean.MomentsShowListBean.MomentsBannerItem;
import com.huiyin.utils.ImageManager;

/**
 * 秀场广告轮换图Adpater
 * @author zhyao
 *
 */
public class MomentsViewflowAdapter extends BaseAdapter {

	private Context mContext;

	private ArrayList<MomentsBannerItem> listDatas;

	public MomentsViewflowAdapter(Context context, ArrayList<MomentsBannerItem> listDatas) {
		mContext = context;

		this.listDatas = listDatas;
	}

	@Override
	public int getCount() {
		return Integer.MAX_VALUE;
	}

	@Override
	public Object getItem(int position) {
		
		if(listDatas == null || listDatas.isEmpty())  {
			return null;
		}
		else {
			return listDatas.get(position % listDatas.size());
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if(convertView == null) {
			viewHolder = new ViewHolder();
			viewHolder.imageView = new ImageView(mContext);
			viewHolder.imageView.setScaleType(ScaleType.FIT_XY);
			viewHolder.imageView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			convertView = viewHolder.imageView;
			convertView.setTag(viewHolder);
		}
		else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		if(listDatas.size() > 0) {
			ImageManager.Load(listDatas.get(position % listDatas.size()).getIMG(), viewHolder.imageView, ImageManager.galleryOptions);
		}
		
		return convertView;
	}

	
	private class ViewHolder {
		ImageView imageView;
	}

}