package com.huiyin.ui.moments.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.bean.MomentsAppraiseListBean.AppraiseItem;
import com.huiyin.utils.ImageManager;
import com.huiyin.utils.StringUtils;

/**
 * 秀场评论adapter
 * @author zhyao
 *
 */
public class MomentsCommentAdapter extends BaseAdapter{
	
	private Context mContext;
	
	private ArrayList<AppraiseItem> listDatas;
	
	public MomentsCommentAdapter(Context context, ArrayList<AppraiseItem> listDatas) {
		mContext = context;
		this.listDatas = listDatas;
	}

	@Override
	public int getCount() {
		return listDatas == null ? 0 : listDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return listDatas == null ? null : listDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder viewHolder;
		if(convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_moments_comment, null);
			viewHolder.mHeadImg = (ImageView) convertView.findViewById(R.id.img_head);
			viewHolder.mNameTv = (TextView) convertView.findViewById(R.id.tv_name);
			viewHolder.mReplyTv = (TextView) convertView.findViewById(R.id.tv_reply);
			viewHolder.mReplyNameTv = (TextView) convertView.findViewById(R.id.tv_reply_name);
			viewHolder.mTimeTv = (TextView) convertView.findViewById(R.id.tv_time);
			viewHolder.mContentTv = (TextView) convertView.findViewById(R.id.tv_comment_content);
			
			convertView.setTag(viewHolder);
		}
		else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		AppraiseItem appraiseItem = listDatas.get(position);
		
		//头像
		ImageManager.Load(appraiseItem.getFACE_IMAGE_PATH(), viewHolder.mHeadImg, ImageManager.headOptions);
		//姓名
		viewHolder.mNameTv.setText(appraiseItem.getUSER_NAME());
		//是否回复
		if(!StringUtils.isEmpty(appraiseItem.getREPLY_NAME())) {
			viewHolder.mReplyNameTv.setVisibility(View.VISIBLE);
			viewHolder.mReplyTv.setVisibility(View.VISIBLE);
			viewHolder.mReplyNameTv.setText("@" + appraiseItem.getREPLY_NAME());
		}
		else {
			viewHolder.mReplyNameTv.setVisibility(View.GONE);
			viewHolder.mReplyTv.setVisibility(View.GONE);
		}
		viewHolder.mTimeTv.setText(appraiseItem.getCREATE_TIME());
		viewHolder.mContentTv.setText(appraiseItem.getCONTENT());
		
		return convertView;
	}
	
	static class ViewHolder {
		
		/**头像**/
		ImageView mHeadImg;
		
		/**姓名**/
		TextView mNameTv;
		
		/**回复**/
		TextView mReplyTv;
		
		/**回复人姓名**/
		TextView mReplyNameTv;
		
		/**创建时间**/
		TextView mTimeTv;
		
		/**评论内容**/
		TextView mContentTv;
		
		
		
	}

}
