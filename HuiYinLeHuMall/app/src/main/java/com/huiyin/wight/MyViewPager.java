package com.huiyin.wight;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.huiyin.ui.classic.HorizontialListView;
import com.huiyin.wight.viewflow.ViewFlow;

public class MyViewPager extends ViewPager {
	public MyViewPager(Context context) {
		super(context);
	}

	public MyViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {

		if (v instanceof HorizontialListView || v instanceof ViewFlow) {
			return true;
		}
		return super.canScroll(v, checkV, dx, x, y);
	}

}
